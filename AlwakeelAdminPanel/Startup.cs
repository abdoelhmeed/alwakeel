﻿using AlwakeelAdminPanel.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System;
using System.Linq;
using System.Text;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System.Collections.Generic;

[assembly: OwinStartupAttribute(typeof(AlwakeelAdminPanel.Startup))]
namespace AlwakeelAdminPanel
{
    public partial class Startup
    {
        ApplicationDbContext db = new ApplicationDbContext();
        BusinessUsers businessUsers = new BusinessUsers();
        Business business = new Business();
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
            AdminAccount();
        }

        public void AdminAccount()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var User = new ApplicationUser();
            User.Email = "Admin@gmail.com";
            User.UserName = "Admin";
         
            var check = UserAdmin.Create(User, "Admin@123");
            if (check.Succeeded)
            {
                if (roleManager.RoleExists("Admin"))
                {
                    var result1 = UserAdmin.AddToRole(User.Id, "Admin");
                }
                CreateCategory();
                Owinr(User.Id);
            }
        }

        private void createRolesandUsers()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Admin";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Company"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Company";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Agent"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Agent";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Insurance "))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Insurance ";
                roleManager.Create(role);
            }

            //if (!roleManager.RoleExists("Seller"))
            //{
            //    var role = new IdentityRole();
            //    role.Id = ID();
            //    role.Name = "Seller ";
            //    roleManager.Create(role);
            //}

            if (!roleManager.RoleExists("CarShowrooms"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "CarShowrooms";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Rent Car"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Rent Car";
                roleManager.Create(role);
            }

            //if (!roleManager.RoleExists("user"))
            //{
            //    var role = new IdentityRole();
            //    role.Id = ID();
            //    role.Name = "user";
            //    roleManager.Create(role);
            //}

            if (!roleManager.RoleExists("Bank"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Bank";
                roleManager.Create(role);
            }

        }

        public void CreateCategory()
        {
            Category category = new Category();
            List<CategoryDomain> cat = new List<CategoryDomain>()
            {
                new CategoryDomain{CatId=1,NameAr="سيارات",NameEn="Cars"},
                new CategoryDomain{CatId=2,NameAr="الشاحنات",NameEn="Trucks"},
                new CategoryDomain{CatId=4,NameEn="Accessories",NameAr="اكسسوارت"},
                new CategoryDomain{CatId=3,NameAr="الدرجات النارية ",NameEn="Motorcycle"},
                new CategoryDomain{CatId=5,NameAr="أسبيرات",NameEn="Auto Parts"},
            };
            foreach (var item in cat)
            {
                category.Add(item);
            }

        }
        public void Owinr(string UserId)
        {
            BusinessDomain model = new BusinessDomain()
            {
                BusinessAboutAR = "الوكيل",
                BusinessAboutEn = "Alwakeel",
                BusinessNameAR = "الوكيل",
                BusinessNameEn = "Alwakeel",
                BusinessRegistrationRate=DateTime.Now,
                BusinessType= "Admin"

            };
           int BId = business.AddReturnId(model);
            OwinrUser(UserId, BId);
        }

        public void OwinrUser(string UserId,int BId)
        {
            BusinessUserDomain model = new BusinessUserDomain()
            {
                BusinessId=BId,
                UserId=UserId

            };
            businessUsers.Add(model);
        }
        public string ID()
        {
            StringBuilder builder = new StringBuilder();
            Enumerable
               .Range(65, 26)
                .Select(e => ((char)e).ToString())
                .Concat(Enumerable.Range(97, 26).Select(e => ((char)e).ToString()))
                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => Guid.NewGuid())
                .Take(11)
                .ToList().ForEach(e => builder.Append(e));
            string id = builder.ToString();
            return id;
        }
    }
}
