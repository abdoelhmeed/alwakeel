﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        HomePage homePage = new HomePage();

        StatisticsSummary statistics = new StatisticsSummary();
        BusinessUsers businessUsers = new BusinessUsers();
        OrderRentCar Order = new OrderRentCar();
        public ActionResult Index()
        {
            ViewBag.Titles = " Dashboard";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public JsonResult GetStatistics() 
        {
           
            if (User.IsInRole("Admin"))
            {
               StatisticsSummaryDomain model= statistics.GetAll();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);

                StatisticsSummaryDomain model = statistics.GetByBrand((int)Buser.BusinessId);
           
                return Json(model, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetNewOrders() 
        {

            List<OrderRentCarDomain> model = new List<OrderRentCarDomain>();
            if (User.IsInRole("Admin"))
            {
                model = Order.GetAllNewsOrders();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = Order.GetNewsOrders((int)Buser.BusinessId).OrderByDescending(u => u.Id).ToList();

            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public JsonResult BayNowNew()
        {
            BayNow bay = new BayNow();
            if (User.IsInRole("Admin"))
            {
                List<BayNowViewDomain> model = bay.GetNew();
                return Json(model,JsonRequestBehavior.AllowGet);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<BayNowViewDomain> model = bay.GetNewByBuId((int)Buser.BusinessId);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}