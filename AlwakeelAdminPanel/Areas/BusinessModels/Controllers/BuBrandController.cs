﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{
    [Authorize]
    public class BuBrandController : Controller
    {
        BusinessBrand bbrand = new BusinessBrand();
        BusinessUsers businessUsers = new BusinessUsers();
        Brand brand = new Brand();
        Business business = new Business();
        // GET: BusinessModels/BuBrand
        public ActionResult Index()
        {
            ViewBag.Titles = "Business With Brand";
            List<BusinessBrandDoamin> model;
            if (User.IsInRole("Admin")) 
            {
                model = bbrand.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = bbrand.GetByBusinessId((int)Buser.BusinessId);
            }
               
            return View(model);
        }

       

        // GET: BusinessModels/BuBrand/Create
        public ActionResult Create()
        {
            List<BusinessDomain> BusinessModel = new List<BusinessDomain>(); ;
            if (User.IsInRole("Admin"))
            {

                BusinessModel = business.Get().Where(x => x.BusinessType == "Company").ToList();
                ViewBag.Business = BusinessModel.ToList();

            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessModel.Add( business.GetById((int)Buser.BusinessId));
                ViewBag.Business = BusinessModel.ToList();
            }


            List<BrandDomainViews> BrandModel = brand.Get();
            ViewBag.Brands = BrandModel.ToList();
            return PartialView("Create");
        }

        // POST: BusinessModels/BuBrand/Create
        [HttpPost]
        public JsonResult Create(BusinessBrandDoamin model)
        {
            try
            {
                int brandId =(int) model.BrandId;
                List<BusinessBrandDoamin> isHavsBusiness = bbrand.GetByBrand(brandId).ToList();
                if (isHavsBusiness.Count > 0)
                {
                    return Json("Sorry, added beforehand", JsonRequestBehavior.AllowGet);
                }

                bbrand.Add(model);
                return Json("Saved",JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return  Json("Not Save", JsonRequestBehavior.AllowGet);
            }
        }

       

        // GET: BusinessModels/BuBrand/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                bbrand.Delete(id);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }
          
        }

      
    }
}
