﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{
    [Authorize]
    public class ManagBusinessController : Controller
    {
        Business business = new Business();
        UserRole userRole = new UserRole();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: BusinessModels/ManagBusiness
        public ActionResult Index()
        {
            ViewBag.Titles = "Business";
            List<BusinessDomain> model = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                model = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                model.Add(bus);
            }

                return View(model);
        }
        [HttpGet]
       
        public ActionResult Create()
        {
            ViewBag.Titles = "Business";
            ViewBag.userRoles = userRole.GetRole();
            BusinessDomain model = new BusinessDomain();
            model.BusinessSpecial = false;
            ViewBag.Error = null;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(BusinessDomain model)
        {
            ViewBag.Titles = "Business";
          
            ViewBag.userRoles = userRole.GetRole();
            if ( model.BusinessPanelImageFIlle == null)
            {
                return View(model);
            }

            if (model.BusinessLogoFIlle == null)
            {
                return View(model);
            }

            if (business.isHasCompany(model))
            {
                model.BusinessLogo = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.BusinessLogoFIlle, "logo")), false);
                model.BusinessPanelImage = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.BusinessPanelImageFIlle, "PanelImage")), false);
                model.BusinessRegistrationRate = DateTime.Now;
                int val = business.AddReturnId(model);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Error = "Sorry, Business already exists";
                return View(model);
            }


          
        }
        [HttpGet]
       // BusinessModels/ManagBusiness/Details?Id=
        public ActionResult Details(int Id)
        {
            ViewBag.Titles = "Business";
            BusinessDomain modle=business.GetById(Id);
            return View(modle);
        }

        [HttpGet]
        public ActionResult Edit(int BId)
        {
            ViewBag.Titles = "Business";
            ViewBag.userRoles = userRole.GetRole();
            BusinessDomain modle = business.GetById(BId);
            return View(modle);
           
        }

        [HttpPost]
        public ActionResult Edit(BusinessDomain model)
        {
            ViewBag.Titles = "Business";
            ViewBag.userRoles = userRole.GetRole();
            if (model.BusinessPanelImageFIlle != null)
            {
                model.BusinessPanelImage = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.BusinessPanelImageFIlle, "PanelImage")), false);
            }

            if (model.BusinessLogoFIlle != null)
            {
                model.BusinessLogo = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.BusinessLogoFIlle, "logo")), false);
              
            }
            

            model.BusinessRegistrationRate = DateTime.Now;

            int val = business.Update(model);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            business.Delete(id);
            return RedirectToAction("Index");
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Business/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Business"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}