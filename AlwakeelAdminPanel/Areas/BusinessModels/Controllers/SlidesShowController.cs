﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{
    [Authorize]
    public class SlidesShowController : Controller
    {
        BSliderShow show = new BSliderShow();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: BusinessModels/SlidesShow
        public ActionResult Index()
        {
            ViewBag.Titles = "Slides Show";
            List<BSliderViewDomain> model = new List<BSliderViewDomain>();
            if (User.IsInRole("Admin"))
            {
                model = show.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<BSliderViewDomain> bus = show.GetByBId((int)Buser.BusinessId);
                model.AddRange(bus);
            }
            return View(model);
        }

     
        // GET: BusinessModels/SlidesShow/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Slides Show";
            if (User.IsInRole("Admin"))
            {
                List<BusinessDomain> BModel = business.Get();
                ViewBag.BModel = BModel.ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain BModel = business.GetById((int)Buser.BusinessId);
                ViewBag.BModel = BModel;
            }

            return View();
        }

        // POST: BusinessModels/SlidesShow/Create
        [HttpPost]
        public ActionResult Create(BusinessSliderDomain model)
        {
            ViewBag.Titles = "Slides Show";

            try
            {
                // TODO: Add insert logic here
                model.ImageUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.Image, "SlideShow")), false);
                show.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

       

        // GET: BusinessModels/SlidesShow/Delete/5
        public ActionResult Delete(int id)
        {
            show.Delete(id);
            return RedirectToAction("Index");
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Business/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Business"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

    }
}
