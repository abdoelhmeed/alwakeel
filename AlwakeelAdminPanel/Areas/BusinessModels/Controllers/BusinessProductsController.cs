﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{

    [Authorize]
    public class BusinessProductsController : Controller
    {
        BusinessProduct BP = new BusinessProduct();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: BusinessModels/BusinessProducts
        public ActionResult Index()
        {

            ViewBag.Titles = "Business Products";
            List<BusinessProductDomain> model = new List<BusinessProductDomain>();
            if (User.IsInRole("Admin"))
            {
                model = BP.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = BP.GetByBusinessId((int)Buser.BusinessId);
            }

            return View(model);
        }

        // GET: BusinessModels/BusinessProducts/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Business Products";
            if (User.IsInRole("Admin"))
            {
                List<BusinessDomain> BModel = business.Get();
                ViewBag.BModel = BModel.ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain BModel = business.GetById((int)Buser.BusinessId);
                ViewBag.BModel = BModel;
            }


            return View();
        }

        // POST: BusinessModels/BusinessProducts/Create
        [HttpPost]
        public ActionResult Create(BusinessProductDomain model)
        {
            ViewBag.Titles = "Business Products";
            if (User.IsInRole("Admin"))
            {
                List<BusinessDomain> BModel = business.Get();
                ViewBag.BModel = BModel.ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain BModel = business.GetById((int)Buser.BusinessId);
                ViewBag.BModel = BModel;
            }
            try
            {
                if (model.PimageUrlFile == null)
                {
                    return View(model);
                }


                model.PimageUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.PimageUrlFile, "Pimage")), false);
                BP.Add(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            ViewBag.Titles = "Business Products";
            if (User.IsInRole("Admin"))
            {
                List<BusinessDomain> BModel = business.Get();
                ViewBag.BModel = BModel.ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain BModel = business.GetById((int)Buser.BusinessId);
                ViewBag.BModel = BModel;
            }
            BusinessProductDomain model = BP.GetByPid(Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(BusinessProductDomain model)
        {
            ViewBag.Titles = "Business Products";
            if (User.IsInRole("Admin"))
            {
                List<BusinessDomain> BModel = business.Get();
                ViewBag.BModel = BModel.ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain BModel = business.GetById((int)Buser.BusinessId);
                ViewBag.BModel = BModel;
            }
            try
            {
                if (model.PimageUrlFile != null)
                {
                    model.PimageUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.PimageUrlFile, "Pimage")), false);
                }


              
                BP.Update(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: BusinessModels/BusinessProducts/Delete/5
        public ActionResult Delete(int id)
        {
            BP.Delete(id);
            return RedirectToAction("Index");
        }

        // BusinessModels/BusinessProducts/GetProductsType?BId=
        public JsonResult GetProductsType(int BId)
        {
            BusinessDomain BModel = business.GetById(BId);
            if (BModel.BusinessType.Contains("Company") || BModel.BusinessType.Contains("Admin"))
            {
                List<ProductsType> model = new List<ProductsType>() {
                   new ProductsType{PName="Tires" },
                    new ProductsType{PName="Battery" },
                    new ProductsType{PName="Oil" }
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else if (BModel.BusinessType.Contains("Agens"))
            {
                List<ProductsType> model = new List<ProductsType>() {
                  new ProductsType{PName=BModel.AgentType }
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<ProductsType> model = new List<ProductsType>() {
                  new ProductsType{PName=BModel.BusinessType }
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }
        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Business/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Business"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
