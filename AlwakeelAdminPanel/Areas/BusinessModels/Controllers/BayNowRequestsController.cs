﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{
    [Authorize]
    public class BayNowRequestsController : Controller
    {
        BayNow bay = new BayNow();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: BusinessModels/BayNowRequests
        public ActionResult New()
        {
            ViewBag.Titles = "New Orders ";
            if (User.IsInRole("Admin"))
            {
                List<BayNowViewDomain> model = bay.GetNew();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<BayNowViewDomain> model = bay.GetNewByBuId((int)Buser.BusinessId);
                return View(model);
            }
        }

        public ActionResult Done()
        {
            ViewBag.Titles = "Done Orders";
            if (User.IsInRole("Admin"))
            {
                List<BayNowViewDomain> model = bay.GetDone();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<BayNowViewDomain> model = bay.GetDoneByBuId((int)Buser.BusinessId);
                return View(model);
            }
        }

        public JsonResult SetDone(int Id) 
        {
            bay.Done(Id);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}