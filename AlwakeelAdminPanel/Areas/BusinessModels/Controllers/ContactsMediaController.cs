﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{
    [Authorize]
    public class ContactsMediaController : Controller
    {
        ContactMedia cm = new ContactMedia();
        Business business=new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: BusinessModels/ContactsMedia
        public ActionResult Index()
        {
            ViewBag.Titles = "Contact && Media";
            List<ContactMediaDomain> model = new List<ContactMediaDomain>();
            if (User.IsInRole("Admin"))
            {
                 model = cm.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = cm.GetBusinessId((int)Buser.BusinessId);
            }
            return View(model);
        }

       

        // GET: BusinessModels/ContactsMedia/Create
        public ActionResult Create()
        {
            List<BusinessDomain> BModel = new List<BusinessDomain>();


            if (User.IsInRole("Admin"))
            {
                BModel = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
               BModel.Add(business.GetById((int)Buser.BusinessId));
            }
            ViewBag.BModel = BModel.ToList();
            ContactMediaDomain model = new ContactMediaDomain();

            return PartialView("Create", model);
        }

        //POST: BusinessModels/ContactsMedia/Create
       [HttpPost]
        public JsonResult Create(ContactMediaDomain model)
        {
            try
            {
                List<BusinessDomain> BModel = business.Get();
                ViewBag.BModel = BModel.ToList();

                cm.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Not Saved", JsonRequestBehavior.AllowGet);
            }
        }


        // GET: BusinessModels/ContactsMedia/Delete/5
        public ActionResult Delete(int id)
        {
            cm.Delete(id);
            return RedirectToAction("Index");
        }

       
    }
}
