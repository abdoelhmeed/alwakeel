﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BusinessModels.Controllers
{
    [Authorize]
    public class BusinessUsersRetPasswordController : Controller
    {
        BusinessUsers businessUsers = new BusinessUsers();

        public ActionResult Index()
        {
            ViewBag.Titles = "Rset Password";

            if (User.IsInRole("Admin"))
            {
                List<BusinessUserDomain> model = businessUsers.GeUsers();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<BusinessUserDomain> model = businessUsers.GeUsersByBusiness((int)Buser.BusinessId);
                return View(model);
            }
        }
    }
}