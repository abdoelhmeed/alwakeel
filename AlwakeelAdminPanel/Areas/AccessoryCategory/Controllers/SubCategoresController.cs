﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AccessoryCategory.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SubCategoresController : Controller
    {
        ACMains MCat = new ACMains();
        ACSubs SCat = new ACSubs();
        // GET: AccessoryCategory/SubCategores
        public ActionResult Index()
        {
            ViewBag.Titles = "Sub Categores";
            List<ACSubDomainView> model = SCat.Get();
            return View(model);
        }

       
        // GET: AccessoryCategory/SubCategores/Create
        public ActionResult AddEdit(int id)
        {
            List<ACMainDomain> CatModel=MCat.Get();
            ViewBag.CatModel = CatModel.ToList();
            if (id > 0)
            {
                ACSubDomain model =SCat.GetById(id);
                return PartialView("AddEdit", model);
            }
            else
            {
                ACSubDomain model = new ACSubDomain();
                return PartialView("AddEdit", model);
            }
          
        }

        // POST: AccessoryCategory/SubCategores/Create
        [HttpPost]
        public JsonResult AddEdit(ACSubDomain model)
        {
            try
            {
                if (model.AcSubId > 0)
                {
                    SCat.Update(model);
                    return Json("Updated", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    SCat.Add(model);
                    return Json("Saved", JsonRequestBehavior.AllowGet);
                }
               
                
            }
            catch
            {
                return Json("OOPS", JsonRequestBehavior.AllowGet);
            }
        }

      

        // GET: AccessoryCategory/SubCategores/Delete/5
        public ActionResult Delete(int id)
        {
            SCat.Delete(id);
            return RedirectToAction("Index");
        }

       
    }
}
