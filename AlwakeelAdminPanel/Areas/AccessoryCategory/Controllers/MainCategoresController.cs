﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AccessoryCategory.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MainCategoresController : Controller
    {
        ACMains MCat = new ACMains();
        // GET: AccessoryCategory/MainCategores
        public ActionResult Index()
        {
            ViewBag.Titles = "Main Categores";
            List<ACMainDomain> model = MCat.Get();
            return View(model);
        }

      
        // GET: AccessoryCategory/MainCategores/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Main Categores";
            return View();
        }

        // POST: AccessoryCategory/MainCategores/Create
        [HttpPost]
        public ActionResult Create(ACMainDomain model)
        {
            ViewBag.Titles = "Main Categores";
            try
            {
                // TODO: Add insert logic here
                if (model.IconFile == null)
                {
                    return View(model);
                }
                
                model.ACatIcon = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.IconFile, "Accessory")), false);
                MCat.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

     

        // GET: AccessoryCategory/MainCategores/Delete/5
        public ActionResult Delete(int id)
        {
            MCat.Delete(id);
            return RedirectToAction("Index");
        }

       


        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Icons/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Icons"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

    }
}
