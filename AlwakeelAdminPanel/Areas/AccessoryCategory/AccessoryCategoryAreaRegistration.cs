﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AccessoryCategory
{
    public class AccessoryCategoryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AccessoryCategory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AccessoryCategory_default",
                "AccessoryCategory/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}