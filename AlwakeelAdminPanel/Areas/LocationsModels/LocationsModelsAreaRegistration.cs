﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.LocationsModels
{
    public class LocationsModelsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "LocationsModels";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LocationsModels_default",
                "LocationsModels/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}