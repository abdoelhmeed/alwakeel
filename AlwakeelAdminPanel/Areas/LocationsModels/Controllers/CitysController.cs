﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.LocationsModels.Controllers
{
    [Authorize]
    public class CitysController : Controller
    {
        State state = new State();
        City city = new City();
        // GET: LocationsModels/Citys
        public ActionResult Index()
        {
            ViewBag.Titles = "City";
            List<CityDomain> model = city.Get();
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            ViewBag.state = state.Get();
            if (Id > 0)
            {
                CityDomain model = city.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                CityDomain model = new CityDomain();
                return PartialView("AddEdit", model);
            }
        }


        [HttpPost]
        public JsonResult AddEdit(CityDomain model)
        {
            ViewBag.state = state.Get();
            if (model.CityId > 0)
            {
                city.Update(model);
                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                city.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(int id)
        {
            ViewBag.Titles = "City";
            city.Delete(id);
            return RedirectToAction("Index");
        }
    }
}