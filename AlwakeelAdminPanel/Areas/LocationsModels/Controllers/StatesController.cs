﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;

namespace AlwakeelAdminPanel.Areas.LocationsModels.Controllers
{
    [Authorize]
    public class StatesController : Controller
    {
        State state = new State();
        // GET: LocationsModels/States
        public ActionResult Index()
        {
            ViewBag.Titles = "State";
            List<StateDomain> model = state.Get();
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            if (Id > 0)
            {
                StateDomain model = state.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                StateDomain model = new StateDomain();
                return PartialView("AddEdit", model);
            }
        }


        [HttpPost]
        public JsonResult AddEdit(StateDomain model)
        {
            if (model.StateId > 0)
            {
                state.Update(model);
                return Json("Update",JsonRequestBehavior.AllowGet);
            }
            else
            {
                state.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(int id)
        {
            ViewBag.Titles = "State";
            state.Delete(id);
            return RedirectToAction("Index") ;
        }
    }
}