﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AlwakeelAdminPanel.Areas.LocationsModels.Controllers
{
    [Authorize]
    public class RegionsController : Controller
    {
        State state = new State();
        City city = new City();
        Region region = new Region();
        // GET: LocationsModels/Regions
        public ActionResult Index()
        {
            ViewBag.Titles = "Regions";
           List<RegionDomain> model =region.Get();
            return View(model);
        }


        public ActionResult AddEdit(int Id)
        {
            ViewBag.state = state.Get();
            if (Id > 0)
            {
                RegionDomain model = region.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                RegionDomain model = new RegionDomain();
                return PartialView("AddEdit", model);
            }
        }


        [HttpPost]
        public JsonResult AddEdit(RegionDomain model)
        {
            ViewBag.state = state.Get();
            if (model.RegionId > 0)
            {
                region.Update(model);
                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                region.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(int id)
        {
            ViewBag.Titles = "City";
            region.Delete(id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public JsonResult cityList(int StateId)
        {
           List<CityDomain>  model =city.GetByStateId(StateId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}