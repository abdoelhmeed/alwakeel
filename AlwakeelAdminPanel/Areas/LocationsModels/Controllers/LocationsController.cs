﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.LocationsModels.Controllers
{
    [Authorize]
    public class LocationsController : Controller
    {
        State state = new State();
        City city = new City();
        Region region = new Region();
        Location location = new Location();
        BusinessUsers businessUsers = new BusinessUsers();
        Business business = new Business();
        // GET: LocationsModels/Locations
        public ActionResult Index()
        {

            ViewBag.Titles = "Pos Locations";
            if (User.IsInRole("Admin"))
            {
                List<LocationDomainView> model = location.Get();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<LocationDomainView> model = location.GetByBId((int)Buser.BusinessId);
                return View(model);
            }

        }

        // GET: LocationsModels/Locations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LocationsModels/Locations/Create
        public ActionResult Create(int Id)
        {
            List<BusinessDomain> BList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                BList = business.Get().Where(x => x.BusinessType.Contains("Agens") || x.BusinessType.Contains("Insurance ")).ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain BU = business.GetById((int)Buser.BusinessId);
                BList.Add(BU);

            }


            ViewBag.state = state.Get();
            ViewBag.Business = BList.ToList();

            if (Id > 0)
            {
                LocationDomain model = location.GetById(Id);
                return PartialView("Create", model);
            }
            else
            {
                LocationDomain model = new LocationDomain();
                return PartialView("Create", model);
            }
           
        }


        // POST: LocationsModels/Locations/Create
        [HttpPost]
        public JsonResult Create(LocationDomain model)
        {
            if (model.posId > 0)
            {
                location.Update(model);
                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                location.Add(model);
                return Json("Seved", JsonRequestBehavior.AllowGet);
            }
        }



        // GET: LocationsModels/Locations/Delete/5
        public ActionResult Delete(int id)
        {
            location.Delete(id);
            return RedirectToAction("Index");
        }


        public JsonResult cityList(int StateId)
        {
            List<CityDomain> model = city.GetByStateId(StateId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RegionList(int cityId)
        {
            List<RegionDomain> model = region.GetByCityId(cityId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
