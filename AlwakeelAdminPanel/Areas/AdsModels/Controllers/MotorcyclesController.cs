﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsModels.Controllers
{
    [Authorize]
    public class MotorcyclesController : Controller
    {
        Motorcycle motorcycle = new Motorcycle();
         CarClasses cclass = new CarClasses();
        CarModel carmodel = new CarModel();
        Brand brand = new Brand();
        Color color = new Color();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        CSpecifications cs = new CSpecifications();
        ProductImage productImage = new ProductImage();
        FeaturesVehicle features = new FeaturesVehicle();
        // GET: AdsModels/Motorcycles
        public ActionResult Index()
        {
            ViewBag.Titles = "Motorcycles";
           
            if (User.IsInRole("Admin"))
            {
                List<MotorcycleViweDomain> motorcycleList = motorcycle.Get();
                return View(motorcycleList);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<MotorcycleViweDomain> motorcycleList = motorcycle.GetByBId((int)Buser.BusinessId);
                return View(motorcycleList);
            }
        }

        // GET: AdsModels/Motorcycles/Details/5
        public ActionResult Details(int PId)
        {
            ViewBag.Titles = "Motorcycles Details";
            MotorcycleViweDomain model = motorcycle.GetDetailsByPId(PId);
            return View(model);
        }

        // GET: AdsModels/Motorcycles/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Motorcycles";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            List<BrandDomainViews> BrandList = brand.GetByCatId(3).ToList();
            MotorcycleDomain model = new MotorcycleDomain();
            model.CatId = 3;
            ViewBag.color = color.Get();
            ViewBag.Brands = BrandList.ToList();
            ViewBag.Business = busList.ToList();

            return View(model);
        }

        // POST: AdsModels/Motorcycles/Create
        [HttpPost]
        public ActionResult Create(MotorcycleDomain model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                if (model.EndDate < DateTime.Now)
                {
                    model.EndDate = DateTime.Now.AddMonths(10);
                }

                if (model.ImageFileOne == null)
                {
                    ViewBag.eror = "select first Image  ";
                    return View(model);
                }

                if (model.ImageFileTow == null)
                {
                    ViewBag.eror = "select second Image  ";
                    return View(model);
                }

                if (model.ImageFilethree == null)
                {
                    ViewBag.eror = "select Third Image  ";
                    return View(model);
                }

                if (model.ImageFileFor == null)
                {
                    ViewBag.eror = "select Fourth Image  ";
                    return View(model);
                }
                List<string> images = new List<string>();
                string ImageFileOne = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileOne, "Product")), false);
                images.Add(ImageFileOne);
                string ImageFileTow = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileTow, "Product")), false);
                images.Add(ImageFileTow);
                string ImageFilethree = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFilethree, "Product")), false);
                images.Add(ImageFilethree);
                string ImageFileFor = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileFor, "Product")), false);
                images.Add(ImageFileFor);
                
             
                motorcycle.Add(model,images);
                return RedirectToAction("Index");
            }
            catch
            {
                List<BusinessDomain> busList = new List<BusinessDomain>();
                if (User.IsInRole("Admin"))
                {
                    busList = business.Get();
                }
                else
                {
                    string UserId = User.Identity.GetUserId();
                    BusinessUserDomain Buser = businessUsers.Get(UserId);
                    BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                    busList.Add(bus);
                }
                List<BrandDomainViews> BrandList = brand.GetByCatId(3).ToList();
               
                model.CatId = 3;
                ViewBag.color = color.Get();
                ViewBag.Brands = BrandList.ToList();
                ViewBag.Business = busList.ToList();
                return View(model);
            }
        }

        // GET: AdsModels/Motorcycles/Edit/5
        public ActionResult Edit(int PId)
        {
            ViewBag.Titles = "Motorcycles";


            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            List<BrandDomainViews> BrandList = brand.GetByCatId(3).ToList();

            MotorcycleDomain model = motorcycle.GetByPId(PId);
            ViewBag.color = color.Get();
            ViewBag.Brands = BrandList.ToList();
            ViewBag.Business = busList.ToList();
           
            return View(model);
        }

        // POST: AdsModels/Motorcycles/Edit/5
        [HttpPost]
        public ActionResult Edit(MotorcycleDomain model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                if (model.EndDate < DateTime.Now)
                {
                    model.EndDate = DateTime.Now.AddMonths(10);
                }

                motorcycle.Update(model);

                return RedirectToAction("Index");
            }
            catch
            {
                List<BusinessDomain> busList = new List<BusinessDomain>();
                if (User.IsInRole("Admin"))
                {
                    busList = business.Get();
                }
                else
                {
                    string UserId = User.Identity.GetUserId();
                    BusinessUserDomain Buser = businessUsers.Get(UserId);
                    BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                    busList.Add(bus);
                }
                List<BrandDomainViews> BrandList = brand.GetByCatId(3).ToList();
                ViewBag.color = color.Get();
                ViewBag.Brands = BrandList.ToList();
                ViewBag.Business = busList.ToList();
                return View(model);
            }
        }

        // GET: AdsModels/Motorcycles/Delete/5
        public ActionResult Delete(int PId)
        {
            motorcycle.delete(PId);
            return RedirectToAction("Index");
        }


        // /AdsModels/Accessories/ProductImages?PId
        [HttpGet]
        public ActionResult ProductImages(int PId)
        {
            List<ProductImageDomain> model = productImage.GetByPId(PId);
            return View(model);
        }

        // /AdsModels/Accessories/EditImage?Id
        [HttpGet]
        public ActionResult AddEditImage(int Id)
        {

            ProductImageDomain model = productImage.GetById(Id);
            return PartialView("AddEditImage", model);

        }

        [HttpPost]
        public ActionResult AddEditImage(ProductImageDomain model)
        {
            model.ImageUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.Image, "alwakeelonl_Accessories")), false);
            productImage.Update(model);
            return RedirectToAction("ProductImages", new { PId = model.PId });
        }




        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Products/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Products"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

        public JsonResult GetBrandList(int CatId)
        {
            string UserId = User.Identity.GetUserId();
            BusinessUserDomain Buser = businessUsers.Get(UserId);

            if (User.IsInRole("Company"))
            {
                List<BrandDomainViews> model = brand.GetBrandPyBusiness((int)Buser.BusinessId, CatId).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<BrandDomainViews> model = brand.GetByCatId(CatId).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetClassList(int BrandId)
        {
            List<CarClassDomainViews> model = cclass.GetByBrandId(BrandId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModel(int CarClassId)
        {
            List<CarModelDomainViews> model = carmodel.GetByCarClassId(CarClassId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFeatures(int modelId)
        {
            List<carSpecificationsDomain> model = cs.GetByModelId(modelId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
