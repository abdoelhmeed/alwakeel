﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsModels.Controllers
{
    [Authorize]
    public class VehiclesController : Controller
    {
        ProductVehicle vehicle = new ProductVehicle();
        Category category = new Category();
        CarClasses cclass = new CarClasses();
        CarModel carmodel = new CarModel();
        Brand brand = new Brand();
        Color color = new Color();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        CSpecifications cs = new CSpecifications();
        ProductImage productImage = new ProductImage();
        FeaturesVehicle features = new FeaturesVehicle();

        // GET: AdsModels/Vehicles
        public ActionResult Index()
        {
            ViewBag.Titles = "Vehicles";
            List<VehicleCustomModel> model = new List<VehicleCustomModel>();
            if (User.IsInRole("Admin"))
            {
                List<PVehicleViewDomain> PVehicleList = vehicle.Get();
                model = VehicleList(PVehicleList);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<PVehicleViewDomain> PVehicleList = vehicle.GetByBusiness((int)Buser.BusinessId);
                model = VehicleList(PVehicleList);
            }
            return View(model);
        }

        // GET: AdsModels/Vehicles/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.Titles = "Vehicles";
            PVehicleViewDomain Pvehicle = vehicle.GetById(id);
            VehicleCustomModel model = Vehicle(Pvehicle);
            return View(model);
        }

        // GET: AdsModels/Vehicles/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Vehicles";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }

            ViewBag.Category = category.GetByIds(1, 2);
            ViewBag.color = color.Get();
            ViewBag.Business = busList.ToList();
            return View();
        }

        // POST: AdsModels/Vehicles/Create
        [HttpPost]
        public ActionResult Create(PVehicleDomain model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.EndDate < DateTime.Now)
            {
                model.EndDate = DateTime.Now.AddMonths(10);
            }

            try
            {
                if (model.ImageFileOne == null)
                {
                    ViewBag.eror = "select first Image  ";
                    return View(model);
                }

                if (model.ImageFileTow == null)
                {
                    ViewBag.eror = "select second Image  ";
                    return View(model);
                }

                if (model.ImageFilethree == null)
                {
                    ViewBag.eror = "select Third Image  ";
                    return View(model);
                }

                if (model.ImageFileFor == null)
                {
                    ViewBag.eror = "select Fourth Image  ";
                    return View(model);
                }
                List<string> images = new List<string>();
                string ImageFileOne = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileOne, "One")), false);
                images.Add(ImageFileOne);
                string ImageFileTow = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileTow, "Tow")), false);
                images.Add(ImageFileTow);
                string ImageFilethree = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFilethree, "Three")), false);
                images.Add(ImageFilethree);
                string ImageFileFor = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileFor, "FileFor")), false);
                images.Add(ImageFileFor);
                model.AdEntryDate = DateTime.Now;

                vehicle.Add(model, images);

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.Titles = "Vehicles";
                List<BusinessDomain> busList = new List<BusinessDomain>();
                if (User.IsInRole("Admin"))
                {
                    busList = business.Get();
                }
                else
                {
                    string UserId = User.Identity.GetUserId();
                    BusinessUserDomain Buser = businessUsers.Get(UserId);
                    BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                    busList.Add(bus);
                }

                ViewBag.Category = category.GetByIds(1, 2);
                ViewBag.color = color.Get();
                ViewBag.Business = busList.ToList();
                return View(model);
            }
        }

        // /AdsModels/Vehicles/Delete?id= 5
        public ActionResult Delete(int id)
        {
            vehicle.delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int PId)
        {
            ViewBag.Titles = "Vehicles";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }

            ViewBag.Category = category.GetByIds(1, 2);
            ViewBag.color = color.Get();
            ViewBag.Business = busList.ToList();
            PVehicleDomain model = vehicle.GetByIdToEdit(PId);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PVehicleDomain model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            vehicle.Update(model);
            return RedirectToAction("Index");
        }

        public JsonResult GetBrandList(int CatId)
        {
            string UserId = User.Identity.GetUserId();
            BusinessUserDomain Buser = businessUsers.Get(UserId);

            if (User.IsInRole("Company"))
            {
                List<BrandDomainViews> model = brand.GetBrandPyBusiness((int)Buser.BusinessId, CatId).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<BrandDomainViews> model = brand.GetByCatId(CatId).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetClassList(int BrandId)
        {
            List<CarClassDomainViews> model = cclass.GetByBrandId(BrandId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModel(int CarClassId)
        {
            List<CarModelDomainViews> model = carmodel.GetByCarClassId(CarClassId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFeatures(int modelId)
        {
            List<carSpecificationsDomain> model = cs.GetByModelId(modelId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public List<VehicleCustomModel> VehicleList(List<PVehicleViewDomain> ListCar)
        {
            List<VehicleCustomModel> model = ListCar.Select(x => new VehicleCustomModel
            {
                BusinessName = x.BusinessNameEn,
                Category = x.CatNameEn,
                EngineCapacityCC = x.CEngineCapacityCC.ToString(),
                Price = x.AdPrice,
                VehicleInfoEn = x.BrandNameEn + " - " + x.CarClassNameEn + " - " + x.ModelYears,
                VehicleInfoAR = x.BrandNameAr + " - " + x.CarClassNameAr + " - " + x.ModelYears,
                CTransmission = x.CTransmission.ToString(),
                MileageKM = x.CMileageKM.ToString(),
                PId = (int)x.PId,
                ColorImage = x.ColorImage,
                ColorNameAr = x.ColorNameAr,
                ColorNameEn = x.ColorNameEn
            }).ToList();
            return model;

        }

        public VehicleCustomModel Vehicle(PVehicleViewDomain PVehicle)
        {
            VehicleCustomModel model = new VehicleCustomModel()
            {
                BusinessName = PVehicle.BusinessNameEn,
                Category = PVehicle.CatNameEn,
                EngineCapacityCC = PVehicle.CEngineCapacityCC.ToString(),
                Price = PVehicle.AdPrice,
                VehicleInfoEn = PVehicle.BrandNameEn + " - " + PVehicle.CarClassNameEn + " - " + PVehicle.ModelYears,
                VehicleInfoAR = PVehicle.BrandNameAr + " - " + PVehicle.CarClassNameAr + " - " + PVehicle.ModelYears,
                CTransmission = PVehicle.CTransmission.ToString(),
                MileageKM = PVehicle.CMileageKM.ToString(),
                PId = (int)PVehicle.PId,
                ColorImage = PVehicle.ColorImage,
                ColorNameAr = PVehicle.ColorNameAr,
                ColorNameEn = PVehicle.ColorNameEn

            };

            return model;

        }



        // GET: /AdsModels/Vehicles/getProductImage?PId=5
        public JsonResult getProductImage(int PId)
        {
            List<ProductImageDomain> model = productImage.GetByPId(PId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // /AdsModels/Vehicles/GetFeatures?PId = 5
        public JsonResult getFeature(int PId)
        {
            List<PFVVeiwDomain> model = features.GetByPId(PId);
            return Json(model, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult DeleteFeatures(int FId)
        {
            int res = features.Delete(FId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Products/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Products"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
