﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsModels.Controllers
{
    [Authorize]
    public class CcommercialAdsController : Controller
    {
        CommercialAds commercialAds = new CommercialAds();
        // GET: AdsModels/CcommercialAds
        public ActionResult Index()
        {
            ViewBag.Titles = "Ads Image";
            List<CommercialAdsDomain> model=commercialAds.Get();
            return View(model);
        }

    
        // GET: AdsModels/CcommercialAds/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Ads Image";
            return View();
        }

        // POST: AdsModels/CcommercialAds/Create
        [HttpPost]
        public ActionResult Create(CommercialAdsDomain model)
        {
            try
            {
                if (model.adsImageFill== null )
                {
                    return View(model);
                }
             
                model.EntryData = DateTime.Now;
                model.adsImage = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.adsImageFill, "homeImage")), false);
                if (model.UrlAds == "" || model.UrlAds==null)
                {
                    model.UrlAds = "#";
                }
                commercialAds.Add(model);
                return RedirectToAction("Index");

            }
            catch
            {
                return View(model);
            }
        }

      
  
    

        // POST: AdsModels/CcommercialAds/Delete/5
     
        public ActionResult Delete(int id)
        {
           
                commercialAds.Delete(id);
                return RedirectToAction("Index");
           
        }


        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Brand/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Brand"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
