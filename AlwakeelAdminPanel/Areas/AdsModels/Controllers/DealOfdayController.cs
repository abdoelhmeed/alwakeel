﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsModels.Controllers
{
    [Authorize]
    public class DealOfdayController : Controller
    {
        Business business = new Business();
        DealOfday dealOfday = new DealOfday();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: AdsModels/DealOfday
        public ActionResult Index()
        {
            ViewBag.Titles = "Deal Of day";

            if (User.IsInRole("Admin"))
            {
                List<DealOfdayViewDomain> model = dealOfday.GetAll();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<DealOfdayViewDomain> model = dealOfday.GetByBId((int)Buser.BusinessId);
                return View(model);
            }
        }

        // GET: AdsModels/DealOfday/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.Titles = "Deal Of Day";
            DealOfdayDomain model=dealOfday.GetById(id);
            return View(model);
        }

        // GET: AdsModels/DealOfday/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Deal Of Day";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            ViewBag.Business = busList.ToList();
            return View();
        }

        // POST: AdsModels/DealOfday/Create
        [HttpPost]
        public ActionResult Create(DealOfdayDomain model)
        {
            ViewBag.Titles = "Deal Of Day";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            ViewBag.Business = busList.ToList();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                if (model.FileImageOne == null)
                {
                    ViewBag.eror = "select first Image  ";
                    return View(model);
                }

                if (model.FileImageOne != null)
                {
                    model.ImageOne = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.FileImageOne, "dealeofday1")), false);
                }


                if (model.FileImageTow != null)
                {
                    model.ImageTow = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.FileImageTow, "dealeofday2")), false);
                }

                if (model.FileImageThree != null)
                {
                    model.ImageThree = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.FileImageThree, "dealeofday3")), false);
                }
                dealOfday.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: AdsModels/DealOfday/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Titles = "Deal Of Day";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            ViewBag.Business = busList.ToList();

            return View();
        }

        // POST: AdsModels/DealOfday/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int Id)
        {
            dealOfday.Delete(Id);
            return RedirectToAction("Index");
        }
      
        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Products/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Products"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
