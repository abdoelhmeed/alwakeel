﻿using AlwakeelBusinessLayer.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using AlwakeelAdminPanel.Models;
using System.IO;

namespace AlwakeelAdminPanel.Areas.AdsModels.Controllers
{
    [Authorize]
    public class AccessoriesController : Controller
    {
        ACMains MCat = new ACMains();
        ACSubs SCat = new ACSubs();
        Accessory accessory = new Accessory();
        Category category = new Category();
        CarClasses cclass = new CarClasses();
        CarModel carmodel = new CarModel();
        Brand brand = new Brand();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        AccessoryDetails accessoryDetails = new AccessoryDetails();
        ProductImage productImage = new ProductImage();
        // GET: AdsModels/Accessories
        public ActionResult Index()
        {
            ViewBag.Titles = "Accessories && Auto Parts";
            if (User.IsInRole("Admin"))
            {
                List<AccessoryViewDomain> model = accessory.Get().OrderByDescending(x => x.PId).ToList();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<AccessoryViewDomain> model = accessory.GetByBId((int)Buser.BusinessId);
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Titles = "Accessories && Auto Parts";
            ViewBag.CatMains = MCat.Get();
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }

            ViewBag.Category = category.GetByIds(4, 5);

            ViewBag.Business = busList.ToList();

            return View();

        }

        [HttpPost]
        public ActionResult Create(AccessoryDomain model)
        {
            ViewBag.Titles = "Accessories && Auto Parts";

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.EndDate < DateTime.Now)
            {
                model.EndDate = DateTime.Now.AddMonths(10);
            }

            if (model.ImageFileOne == null)
            {
                ViewBag.eror = "select first Image  ";
                return View(model);
            }

            if (model.ImageFileTow == null)
            {
                ViewBag.eror = "select second Image  ";
                return View(model);
            }

            if (model.ImageFilethree == null)
            {
                ViewBag.eror = "select Third Image  ";
                return View(model);
            }

            if (model.ImageFileFor == null)
            {
                ViewBag.eror = "select Fourth Image  ";
                return View(model);
            }
            List<string> images = new List<string>();
            string ImageFileOne = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileOne, "alwakeelonl_Accessories")), false);
            images.Add(ImageFileOne);
            string ImageFileTow = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileTow, "alwakeelonl_Accessories")), false);
            images.Add(ImageFileTow);
            string ImageFilethree = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFilethree, "alwakeelonl_Accessories")), false);
            images.Add(ImageFilethree);
            string ImageFileFor = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImageFileFor, "alwakeelonl_Accessories")), false);
            images.Add(ImageFileFor);
            model.Images = images;

            if (model.AccessoryForAll == true)
            {
                int res = accessory.Add(model);
                return RedirectToAction("Index");
            }
            else
            {
                int res = accessory.Add(model);
                return RedirectToAction("CrateModels", new { PId = res });
            }

        }
        [HttpGet]
        public ActionResult Edit(int PId)
        {
            ViewBag.Titles = "Accessories && Auto Parts";
            ViewBag.CatMains = MCat.Get();
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            ViewBag.Category = category.GetByIds(4, 5);
            ViewBag.Business = busList.ToList();
            AccessoryEditDomain model = accessory.GetByIdToEdit(PId);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AccessoryEditDomain model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.EndDate < DateTime.Now)
            {
                model.EndDate = DateTime.Now.AddMonths(10);
            }

            int res = accessory.Update(model);
            return RedirectToAction("Index");

        }


        public ActionResult Delete(int PId)
        {
            accessory.Delete(PId);
            return RedirectToAction("Index");
        }

        //  /AdsModels/Accessories/CrateModels?PId=
        [HttpGet]
        public ActionResult CrateModels(int PId)
        {
            ViewBag.Titles = "Models Details";
            AccessoryDetailsDomain model = new AccessoryDetailsDomain();
            model.PId = PId;
            ViewBag.Accessory = accessory.GetById(PId);
            ViewBag.accessoriesModels = accessoryDetails.GetByPId(PId);

            ViewBag.Category = category.GetByIds(1, 2);
            return View(model);
        }

        [HttpPost]
        public ActionResult CrateModels(AccessoryDetailsDomain model)
        {
            ViewBag.Titles = "Models Details";
            ViewBag.accessoriesModels = accessoryDetails.GetByPId((int)model.PId);
            ViewBag.Category = category.GetByIds(1, 2);
            accessoryDetails.Add(model);
            return RedirectToAction("CrateModels", new { PId = model.PId });
        }

        public ActionResult DeleteModels(int Id)
        {
            int PId = accessoryDetails.Delete(Id);
            return RedirectToAction("CrateModels", new { PId = PId });
        }

        // /AdsModels/Accessories/ProductImages?PId
        [HttpGet]
        public ActionResult ProductImages(int PId)
        {
            List<ProductImageDomain> model = productImage.GetByPId(PId);
            return View(model);
        }

        // /AdsModels/Accessories/EditImage?Id
        [HttpGet]
        public ActionResult AddEditImage(int Id)
        {

            ProductImageDomain model =  productImage.GetById(Id);
            return PartialView("AddEditImage", model);

        }

        [HttpPost]
        public ActionResult AddEditImage(ProductImageDomain model)
        {
            model.ImageUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.Image, "alwakeelonl_Accessories")), false);
            productImage.Update(model);
            return RedirectToAction("ProductImages", new { PId = model.PId });
        }

        // /AdsModels/Accessories/GetBrandList?CatId=
        public JsonResult GetBrandList(int CatId)
        {
            string UserId = User.Identity.GetUserId();
            BusinessUserDomain Buser = businessUsers.Get(UserId);

            if (User.IsInRole("Company"))
            {
                List<BrandDomainViews> model = brand.GetBrandPyBusiness((int)Buser.BusinessId, CatId).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<BrandDomainViews> model = brand.GetByCatId(CatId).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }
        // /AdsModels/Accessories/GetClassList?BrandId=
        public JsonResult GetClassList(int BrandId)
        {
            List<CarClassDomainViews> model = cclass.GetByBrandId(BrandId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        // /AdsModels/Accessories/GetModel?CarClassId=
        public JsonResult GetModel(int CarClassId)
        {
            List<CarModelDomainViews> model = carmodel.GetByCarClassId(CarClassId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        // /AdsModels/Accessories/GetSubAccessory?MId=
        public JsonResult GetSubAccessory(int MId)
        {
            List<ACSubDomainView> model = SCat.GetBYCatId(MId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccessoryModels(int PId)
        {
            List<AccessoryDetailsViewDomain> model = accessoryDetails.GetByPId(PId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Products/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Products"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}