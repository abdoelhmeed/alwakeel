﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class YouTubeVideosController : Controller
    {
        VideoYouTube videoYouTube = new VideoYouTube();
        // GET: AdsModels/YouTubeVideos
        public ActionResult Index()
        {
            ViewBag.Titles = "Video YouTube";
            List<VideoDomain>model= videoYouTube.Get().OrderBy(x=>x.VideoDate).ToList();
            return View(model);
        }

       

        // GET: AdsModels/YouTubeVideos/Create
        public ActionResult Create()
        {
            VideoDomain model = new VideoDomain();
            return PartialView("Create", model);
        }

        // POST: AdsModels/YouTubeVideos/Create
        [HttpPost]
        public JsonResult Create(VideoDomain model)
        {
            try
            {
                // TODO: Add insert logic here
                videoYouTube.Add(model);
                return Json("Saved",JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Not Save", JsonRequestBehavior.AllowGet);
            }
        }

       
     
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                videoYouTube.Delete(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
