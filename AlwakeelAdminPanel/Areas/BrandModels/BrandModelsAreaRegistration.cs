﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BrandModels
{
    public class BrandModelsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BrandModels";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BrandModels_default",
                "BrandModels/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}