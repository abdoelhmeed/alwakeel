﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BrandModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CSpecificatController : Controller
    {
        CarClasses cclass = new CarClasses();
        CarModel carmodel = new CarModel();
        Category category = new Category();
        Brand brand = new Brand();
        CSpecifications cs = new CSpecifications();
        public ActionResult Index()
        {
            ViewBag.Titles = "Model Specifications";
            List<CarModelDomainViews> model = carmodel.Get();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int ModelId)
        {
           ViewBag.CS= cs.GetByModelId(ModelId);
            CarModelDomain modelOfCar = carmodel.GetById(ModelId);
            CarClassDomain classOFName = cclass.GetById((int)modelOfCar.CarClassId);
            BrandDomain BrandOfName = brand.GetById((int)classOFName.BrandId);
            var cat = category.GetById((int)BrandOfName.CatId);

            ViewBag.ModelName = modelOfCar.ModelYears;
            ViewBag.cclassName = classOFName.CarClassNameEn +" - "+ classOFName.CarClassNameEn;
            ViewBag.BrandName = BrandOfName.BrandNameEn+" - "+ BrandOfName.BrandNameEn;
            ViewBag.BrandCategory = cat.NameEn+" - "+ cat.NameAr;

            carSpecificationsDomain model = new carSpecificationsDomain();

            model.ModelId = ModelId;
            model.BrandId = BrandOfName.BrandId;
            model.CarClassId = classOFName.CarClassId;
            model.CatId = cat.CatId;
            return View(model);
        }

      

        [HttpPost]
        public ActionResult Create(carSpecificationsDomain model)
        {
            ViewBag.CS = cs.GetByModelId((int)model.ModelId);
            CarModelDomain modelOfCar = carmodel.GetById((int)model.ModelId);
            CarClassDomain classOFName = cclass.GetById((int)modelOfCar.CarClassId);
            BrandDomain BrandOfName = brand.GetById((int)classOFName.BrandId);
            var cat = category.GetById((int)BrandOfName.CatId);

            ViewBag.ModelName = modelOfCar.ModelYears;
            ViewBag.cclassName = classOFName.CarClassNameEn + " - " + classOFName.CarClassNameEn;
            ViewBag.BrandName = BrandOfName.BrandNameEn + " - " + BrandOfName.BrandNameEn;
            ViewBag.BrandCategory = cat.NameEn + " - " + cat.NameAr;
            if (model.IconFile !=null)
            {
  
                model.SpeIcon = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.IconFile, "Icon")), false);
            }
           

            if (ModelState.IsValid)
            {
                cs.Add(model);
                return RedirectToAction("Create", new { ModelId = model.ModelId });
            }
            else
            {
                return View(model);
            }

          
           
        }

        public ActionResult Delete(int id)
        {
            carSpecificationsDomain model = cs.GetById(id);
            cs.Delete(id);
            return RedirectToAction("Create", new { ModelId = model.ModelId });
        }


        public JsonResult getSc(int ModelId)
        {
            List<carSpecificationsDomain> model = cs.GetByModelId(ModelId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Icons/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Icons"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}