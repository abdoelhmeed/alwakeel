﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Implementation;

namespace AlwakeelAdminPanel.Areas.BrandModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CarsClassController : Controller
    {
        Brand brand = new Brand();
        CarClasses cclass = new CarClasses();
        Category Cat = new Category();
        public ActionResult Index()
        {
            ViewBag.Titles = "Class";
           List<CarClassDomainViews> model= cclass.Get();
            return View(model);
        }

      

       [HttpGet]
        public ActionResult AddEdit(int Id)
        {
           ViewBag.Cat = Cat.GetByIds(0,3);
            if (Id > 0)
            {
                CarClassDomain model=  cclass.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                CarClassDomain model = new CarClassDomain();
                return PartialView("AddEdit", model);
            }
          
        }

        // POST: BrandModels/CarsClass/Create
        [HttpPost]
        public JsonResult AddEditC(CarClassDomain model)
        {
            ViewBag.Cat = Cat.GetByIds(0, 3);
            try
            {
                if (model.CarClassId > 0)
                {
                    cclass.Update(model);
                    return Json("Update", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (cclass.IsHasClass(model))
                    {
                        cclass.Add(model);
                        return Json("Saved", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Sorry, Class already exists", JsonRequestBehavior.AllowGet);
                    }
                   
                }
            }
            catch
            {
                return Json("try agany", JsonRequestBehavior.AllowGet);
            }
        }


        
        public JsonResult GetBrand(int CatId)
        {
           var model= brand.GetByCatId(CatId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
       
        // GET: BrandModels/CarsClass/Delete/5
        public ActionResult Delete(int id)
        {
            cclass.Delete(id);
            return RedirectToAction("Index");
        }

      
    }
}
