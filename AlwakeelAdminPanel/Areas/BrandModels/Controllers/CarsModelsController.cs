﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelBusinessLayer.Implementation;

namespace AlwakeelAdminPanel.Areas.BrandModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CarsModelsController : Controller
    {
        CarClasses cclass = new CarClasses();
        CarModel carmodel = new CarModel();
        Category category = new Category();
        GeneralSpecification general = new GeneralSpecification();
        Brand brand = new Brand();
        // GET: BrandModels/CarsModels
        public ActionResult Index()
        {
            ViewBag.Titles = "Model";
            List<CarModelDomainViews> model = carmodel.Get();
            return View(model);
        }



        // GET: BrandModels/CarsModels/Create
        public ActionResult AddEdit(int Id)
        {
            ViewBag.Category = category.GetByIds(0, 3).ToList();
            ViewBag.general = general.Get();
            if (Id > 0)
            {
                CarModelDomain model = carmodel.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                CarModelDomain model = new CarModelDomain();
                model.ModelYears = DateTime.Now.Year;
                return PartialView("AddEdit", model);
            }

        }

        // POST: BrandModels/CarsModels/Create
        [HttpPost]
        public JsonResult AddOrEdit(CarModelDomain model)
        {
            try
            {
                if (model.ModelId > 0)
                {
                    carmodel.Update(model);
                    return Json("Updated", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (carmodel.ishasModel((int)model.ModelYears, (int)model.CarClassId))
                    {
                        carmodel.Add(model);
                        return Json("Saved", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Sorry, Model already exists", JsonRequestBehavior.AllowGet);
                    }

                }

            }
            catch
            {
                return Json("Try Agen", JsonRequestBehavior.AllowGet);
            }
        }



        // GET: BrandModels/CarsModels/Delete/5
        public ActionResult Delete(int id)
        {
            carmodel.Delete(id);
            return RedirectToAction("Index");
        }

        public JsonResult GetBrandList(int CatId)
        {
            List<BrandDomainViews> model = brand.GetByCatId(CatId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClassList(int BrandId)
        {
            List<CarClassDomainViews> model = cclass.GetByBrandId(BrandId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
