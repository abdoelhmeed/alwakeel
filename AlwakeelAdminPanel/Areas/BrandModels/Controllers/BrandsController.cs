﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Implementation;
using System.IO;
using AlwakeelAdminPanel.Models;

namespace AlwakeelAdminPanel.Areas.BrandModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BrandsController : Controller
    {
        Brand brand = new Brand();
        Category Cat = new Category();
        // GET: BrandModels/Brands
        public ActionResult Index()
        {
            ViewBag.Titles = "Brands";

            List<BrandDomainViews> model = brand.Get().ToList();
            return View(model);
        }


        // GET: BrandModels/Brands/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Brands";
            ViewBag.Error = null;
            ViewBag.Category = Cat.GetByIds(0, 3).ToList();
            return View();
        }

        // POST: BrandModels/Brands/Create
        [HttpPost]
        public ActionResult Create(BrandDomain model)
        {
            ViewBag.Titles = "Brands";
          
            ViewBag.Category = Cat.GetByIds(0, 3).ToList();
     
            try
            {
                if (brand.isHasBrand(model))
                {
                    if (model.ImgeFile == null)
                    {
                        return View(model);
                    }
                    model.BrandsLogo = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImgeFile, "Brands")), false);
                    brand.Add(model);
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = "Sorry, Brand already exists";
                    return View(model);
                }
               
            }
            catch
            {
                return View(model);
            }
        }

        // GET: BrandModels/Brands/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Category = Cat.GetByIds(0, 3).ToList();
            BrandDomain model= brand.GetById(id);
            return View(model);
        }

        // POST: BrandModels/Brands/Edit/5
        [HttpPost]
        public ActionResult Edit( BrandDomain model)
        {
            ViewBag.Category = Cat.GetByIds(0, 3).ToList();
           
            
            try
            {
                if (model.ImgeFile != null)
                {
                  
                    model.BrandsLogo = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.ImgeFile, "Brands")), false);
                }
                brand.Update(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

      
        public ActionResult Delete(int id)
        {
            brand.Delete(id);
            return RedirectToAction("Index");
        }

      


        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Brand/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Brand"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
