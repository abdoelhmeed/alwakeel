﻿using AlwakeelBusinessLayer.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelDomainLayer;
using System.IO;
using AlwakeelAdminPanel.Models;

namespace AlwakeelAdminPanel.Areas.BrandModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GeneralSpecificationsController : Controller
    {
        GeneralSpecification general = new GeneralSpecification();
        // GET: BrandModels/GeneralSpecifications
        public ActionResult Index()
        {
            ViewBag.Titles = "General Specifications";
            List<SpecificationDomain>model= general.Get();
            return View(model);
        }

        // GET: BrandModels/GeneralSpecifications/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BrandModels/GeneralSpecifications/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "General Specifications";
            return View();
        }

        // POST: BrandModels/GeneralSpecifications/Create
        [HttpPost]
        public ActionResult Create(SpecificationDomain model)
        {
            ViewBag.Titles = "General Specifications";
            try
            {
                model.Icons = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.Image, "Brands")), false);
                general.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: BrandModels/GeneralSpecifications/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Titles = "General Specifications";
            return View();
        }

        // POST: BrandModels/GeneralSpecifications/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            ViewBag.Titles = "General Specifications";
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BrandModels/GeneralSpecifications/Delete/5
        public ActionResult Delete(int id)
        {
            ViewBag.Titles = "General Specifications";
            general.Delete(id);
            return RedirectToAction("Index");
        }

        // POST: BrandModels/GeneralSpecifications/Delete/5
      

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Brand/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Brand"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
