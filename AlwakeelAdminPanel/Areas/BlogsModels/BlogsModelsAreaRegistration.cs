﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.BlogsModels
{
    public class BlogsModelsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BlogsModels";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BlogsModels_default",
                "BlogsModels/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}