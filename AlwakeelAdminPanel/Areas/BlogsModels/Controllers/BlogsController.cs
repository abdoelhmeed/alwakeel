﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelAdminPanel.Models;

namespace AlwakeelAdminPanel.Areas.BlogsModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BlogsController : Controller
    {
        Blog bl = new Blog();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: BlogsModels/Blogs
        public ActionResult Index()
        {
            ViewBag.Titles = " Blogs";
            if (User.IsInRole("Admin"))
            {
                List<BlogDomain> model = bl.Get().OrderBy(x=> x.blogDate).ToList();
                return View(model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                List<BlogDomain> model = bl.GetByPublish(business.GetById((int)Buser.BusinessId).BusinessNameEn).OrderBy(x => x.blogDate).ToList();
                return View(model);
            }
                
        }

        // GET: BlogsModels/Blogs/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.Titles = " Blogs";
            BlogDomain model = bl.GetById(id);
            return View(model);
        }

        // GET: BlogsModels/Blogs/Create
        public ActionResult Create()
        {
            ViewBag.Titles = " Blogs";
            BlogDomain model = new BlogDomain();
            string UserId = User.Identity.GetUserId();
            BusinessUserDomain Buser = businessUsers.Get(UserId);
            model.PublishBy = business.GetById((int)Buser.BusinessId).BusinessNameEn;
            model.blogDate = DateTime.Now;
            return View(model);
        }

        // POST: BlogsModels/Blogs/Create
      
        [HttpPost, ValidateInput(false)]
        public ActionResult Create(BlogDomain model)
        {
            ViewBag.Titles = " Blogs";
            try
            {
                if (model.IamgeFile == null)
                {
                    return View(model);
                }
                // TODO: Add insert logic here
                model.blogIamgeUrl=UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.IamgeFile, "blog")), false);
                bl.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

       

        // GET: BlogsModels/Blogs/Delete/5
        public ActionResult Delete(int id)
        {
            bl.Delete(id);
            return RedirectToAction("Index");
        }

        public  string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Blogs/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Blogs"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }




    }
}
