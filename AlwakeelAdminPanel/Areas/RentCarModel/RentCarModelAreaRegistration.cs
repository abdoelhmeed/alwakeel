﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.RentCarModel
{
    public class RentCarModelAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RentCarModel";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RentCarModel_default",
                "RentCarModel/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}