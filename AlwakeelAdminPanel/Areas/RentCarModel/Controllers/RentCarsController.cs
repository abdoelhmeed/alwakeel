﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.RentCarModel.Controllers
{
    [Authorize]
    public class RentCarsController : Controller
    {
        Category category = new Category();
        CarClasses cclass = new CarClasses();
        CarModel carmodel = new CarModel();
        Brand brand = new Brand();
        Color color = new Color();
        Business business = new Business();
        BusinessUsers businessUsers = new BusinessUsers();
        RentsCars rentsCars = new RentsCars();
        // GET: RentCarModel/RentCars
        public ActionResult Index()
        {
            ViewBag.Titles = "Rent Cars";
            List<RentCarViweDomain> model = new List<RentCarViweDomain>();
            if (User.IsInRole("Admin"))
            {
                model = rentsCars.GetBusiness().ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = rentsCars.GetByBId((int)Buser.BusinessId).OrderByDescending(u => u.Id).ToList();

            }
            return View(model);
        }

        // GET: RentCarModel/RentCars/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RentCarModel/RentCars/Create
        public ActionResult Create()
        {
            ViewBag.Titles = "Rent Car";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            ViewBag.Category = category.GetByIds(1, 2);
            ViewBag.Business = busList.ToList();

            return View();
        }

        // POST: RentCarModel/RentCars/Create
        [HttpPost]
        public ActionResult Create(RentCarDomain model)
        {
            try
            {
                model.ImageURl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.File, "RentCars")), false);
                rentsCars.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                List<BusinessDomain> busList = new List<BusinessDomain>();
                if (User.IsInRole("Admin"))
                {
                    busList = business.Get();
                }
                else
                {
                    string UserId = User.Identity.GetUserId();
                    BusinessUserDomain Buser = businessUsers.Get(UserId);
                    BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                    busList.Add(bus);
                }
                ViewBag.Category = category.GetByIds(1, 2);
                ViewBag.Business = busList.ToList();
                return View(model);
            }
        }

        // GET: RentCarModel/RentCars/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Titles = "Rent Car";
            List<BusinessDomain> busList = new List<BusinessDomain>();
            if (User.IsInRole("Admin"))
            {
                busList = business.Get();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                busList.Add(bus);
            }
            ViewBag.Category = category.GetByIds(1, 2);
            ViewBag.Business = busList.ToList();
            RentCarDomain model = rentsCars.GetByEditId(id);
            return View(model);
        }

        // POST: RentCarModel/RentCars/Edit/5
        [HttpPost]
        public ActionResult Edit(RentCarDomain model)
        {
            try
            {
                if (model.File !=null)
                {
                    model.ImageURl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.File, "RentCars")), false);
                }

                rentsCars.Update(model);
                return RedirectToAction("Index");
               
            }
            catch
            {
                ViewBag.Titles = "Rent Car";
                List<BusinessDomain> busList = new List<BusinessDomain>();
                if (User.IsInRole("Admin"))
                {
                    busList = business.Get();
                }
                else
                {
                    string UserId = User.Identity.GetUserId();
                    BusinessUserDomain Buser = businessUsers.Get(UserId);
                    BusinessDomain bus = business.GetById((int)Buser.BusinessId);
                    busList.Add(bus);
                }
                ViewBag.Category = category.GetByIds(1, 2);
                ViewBag.Business = busList.ToList();
            
                return View(model);
            }
        }

        // GET: RentCarModel/RentCars/Delete/5
        public ActionResult Delete(int id)
        {
            rentsCars.Delete(id);
            return RedirectToAction("Index");
        }


        public ActionResult Available(int id) 
        {
            rentsCars.isAvailable(id);
            return RedirectToAction("Index");
        }

        // GET: RentCarModel/RentCars/Delete/5
        public JsonResult GetBrandList(int CatId)
        {
            List<BrandDomainViews> model = brand.GetByCatId(CatId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // GET: RentCarModel/RentCars/Delete/5
        public JsonResult GetClassList(int BrandId)
        {
            List<CarClassDomainViews> model = cclass.GetByBrandId(BrandId).ToList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        // GET: RentCarModel/RentCars/Delete/5
        public JsonResult GetModel(int CarClassId)
        {
            List<CarModelDomainViews> model = carmodel.GetByCarClassId(CarClassId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Products/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Products"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
