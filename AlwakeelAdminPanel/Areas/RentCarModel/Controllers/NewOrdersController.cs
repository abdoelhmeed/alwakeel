﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.AspNet.Identity;

namespace AlwakeelAdminPanel.Areas.RentCarModel.Controllers
{
    [Authorize]
    public class NewOrdersController : Controller
    {
        OrderRentCar Order = new OrderRentCar();
        BusinessUsers businessUsers = new BusinessUsers();
        // GET: RentCarModel/NewOrders
        public ActionResult Index()
        {
            ViewBag.Titles = "News Orders";
            List<OrderRentCarDomain> model = new List<OrderRentCarDomain>();
            if (User.IsInRole("Admin"))
            {
                model = Order.GetAllNewsOrders();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = Order.GetNewsOrders((int)Buser.BusinessId).OrderByDescending(u => u.Id).ToList();

            }
            return View(model);
        }

        // GET: RentCarModel/NewOrders/NotAvailable
        public ActionResult NotAvailable(int Id) 
        {
            Order.NotAvailableOrder(Id);
            return RedirectToAction("Index");
        }

        // GET: /RentCarModel/NewOrders/Done?Id=
        public ActionResult Done(int Id)
        {
            Order.DoneOrder(Id);
            return RedirectToAction("Index");
        }

        public ActionResult OldOrders() 
        {
            ViewBag.Titles = "Orders";
            List<OrderRentCarDomain> model = new List<OrderRentCarDomain>();
            if (User.IsInRole("Admin"))
            {
                model = Order.GetOrder().OrderBy(x=>x.Status).ToList();
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                BusinessUserDomain Buser = businessUsers.Get(UserId);
                model = Order.GetOrderByBId((int)Buser.BusinessId).OrderByDescending(u => u.Id).OrderBy(x=> x.Status).ToList();

            }
            return View(model);
        }
    }
}