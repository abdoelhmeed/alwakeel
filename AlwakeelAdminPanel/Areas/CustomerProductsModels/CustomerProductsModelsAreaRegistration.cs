﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.CustomerProductsModels
{
    public class CustomerProductsModelsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CustomerProductsModels";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CustomerProductsModels_default",
                "CustomerProductsModels/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}