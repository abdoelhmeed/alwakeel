﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelBusinessLayer.Implementation.PordectsAPI;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.CustomerProductsModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductsMnagementController : Controller
    {
        Accessory accessory = new Accessory();
        Customerproducts customerproducts = new Customerproducts();
        // GET: CustomerProductsModels/ProductsMnagement
        public ActionResult Index()
        {
            ViewBag.Titles = "Customer Products";
            List<CustomerproductsDomain> model= customerproducts.GetNotActive();

            return View(model);
        }

        public ActionResult ActiveProducts()
        {
            ViewBag.Titles = "Active Products";

            return View();
        }

        public ActionResult SoldProducts()
        {
            ViewBag.Titles = "Sold Products";
            return View();
        }


        public ActionResult ProductsDetails(int PId) 
        {
            ViewBag.Titles = "Product Details";
            CustomerproductsDomain model = customerproducts.GetDetails(PId);
            return View(model);
        }

        public JsonResult GetProducts() 
        {
            List<CustomerproductsDomain> model = customerproducts.GetNotActive();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActivted()
        {
            List<CustomerproductsDomain> model = customerproducts.GetActivted();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSold()
        {
            List<CustomerproductsDomain> model = customerproducts.GetSold();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //  /CustomerProductsModels/ProductsMnagement/Delete?PId=
        public JsonResult Delete(int PId)
        {
            accessory.Delete(PId);
            return Json("delete",JsonRequestBehavior.AllowGet);
        }

        //  /CustomerProductsModels/ProductsMnagement/Delete?PId=
        public JsonResult Activation(int PId)
        {
            customerproducts.SetActivat(PId);
            return Json("Activated", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Sold(int PId)
        {
            customerproducts.SetSold(PId);
            return Json("Sold", JsonRequestBehavior.AllowGet);
        }


        //API
    }
}