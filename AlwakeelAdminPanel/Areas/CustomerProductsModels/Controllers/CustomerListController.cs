﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation.PordectsAPI;

namespace AlwakeelAdminPanel.Areas.CustomerProductsModels
{
    [Authorize(Roles = "Admin")]
    public class CustomerListController : Controller
    {
        CustomersList customersList = new CustomersList();
        Pordect pordect = new Pordect();
        // GET: CustomerProductsModels/CustomerList
        public ActionResult Index()
        {
            ViewBag.Titles = "Customer List";
            List<CustomerListDomain> model = customersList.Get();
            return View(model);
        }

        [HttpGet]
        public JsonResult SendProductsList()
        {

           
            string Card;
            string Cards=null;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/MeilTemplete/card.html")))
            {
                Card = reader.ReadToEnd();
            }

            foreach (var item in pordect.MailForCompanyPordect())
            {
                Card.Replace("{Id}", "http://alwakeelonline.com/Products/Details/"+item.PId.ToString());
                string imageUrl = "src=" + item.Image + "";
                Card.Replace("{Imagepordect}", imageUrl);
                Card.Replace(" {PordectName}", item.NameEN);
                Cards = Cards + Card;
            }


            string body;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/MeilTemplete/alwakeelonline.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{CardPordect}", Cards);

            bool IsSendEmail = Mails.Send("abdoelhmeedsudan@gmail.com", "New", body);
            //foreach (var item in customersList.Get())
            //{
            //    if (item.Email != null && item.Email != "" && item.Email != "No")
            //    {
            //        bool IsSendEmail = Mails.Send("abdoelhmeedsudan@gmail.com", "New", body);
            //    }

            //}



            return Json("Done", JsonRequestBehavior.AllowGet);
        }
    }
}