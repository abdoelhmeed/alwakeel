﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.CustomerProductsModels.Controllers
{
    public class CustomerRentCarsController : Controller
    {
        RentsCars rentsCars = new RentsCars();
        // GET: CustomerProductsModels/CustomerRentCars
        public ActionResult Index()
        {
            ViewBag.Titles = "Customer Rent Cars";
            List<RentCarViweCustomerDomain> model=rentsCars.GetCustomer().OrderByDescending(x=> x.Id).ToList();

            return View(model);
        }
        public ActionResult Delete(int id)
        {
            rentsCars.Delete(id);
            return RedirectToAction("Index");
        }
        public ActionResult Available(int id)
        {
            rentsCars.isAvailable(id);
            return RedirectToAction("Index");
        }
    }
}