﻿using AlwakeelAdminPanel.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsProductModels.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductColorController : Controller
    {
        Color color = new Color();
        // GET: AdsProductModels/ProductColor
        public ActionResult Index()
        {
            ViewBag.Titles = "Product Color";
            List<ColorDomain> model = color.Get();
            ViewBag.Colors = model.ToList();
            return View();
        }


        // POST: AdsProductModels/ProductColor/Create
        [HttpPost]
        public ActionResult Create(ColorDomain model)
        {
            try
            {
                // TODO: Add insert logic here
                if (model.IconFile == null)
                {
                    return View(model);
                }
              
                model.colorIcon = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(AddImage(model.IconFile, "color")), false);
                color.Add(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

       
        public ActionResult Delete(int id)
        {
            color.Delete(id);
            return RedirectToAction("Index");
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Image/Icons/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/Icons"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

    }
}
