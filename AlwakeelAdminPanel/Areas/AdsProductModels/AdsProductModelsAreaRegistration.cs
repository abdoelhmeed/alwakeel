﻿using System.Web.Mvc;

namespace AlwakeelAdminPanel.Areas.AdsProductModels
{
    public class AdsProductModelsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdsProductModels";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AdsProductModels_default",
                "AdsProductModels/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}