//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlwakeelRepositoryLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_DetailsOptions
    {
        public int Id { get; set; }
        public string OptionNameAr { get; set; }
        public string OptionNameEn { get; set; }
        public Nullable<int> OId { get; set; }
    }
}
