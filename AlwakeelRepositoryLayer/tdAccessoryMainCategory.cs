//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlwakeelRepositoryLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tdAccessoryMainCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tdAccessoryMainCategory()
        {
            this.tdAccessorySubCategory = new HashSet<tdAccessorySubCategory>();
        }
    
        public int ACatId { get; set; }
        public string ACatNameAr { get; set; }
        public string ACatNameEn { get; set; }
        public string ACatIcon { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tdAccessorySubCategory> tdAccessorySubCategory { get; set; }
    }
}
