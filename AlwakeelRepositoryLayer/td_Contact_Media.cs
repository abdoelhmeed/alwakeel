//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlwakeelRepositoryLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_Contact_Media
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string contact { get; set; }
        public Nullable<int> BusinessId { get; set; }
    
        public virtual td_Business td_Business { get; set; }
    }
}
