//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlwakeelRepositoryLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_CommercialAds
    {
        public int Id { get; set; }
        public string TitleAr { get; set; }
        public string TitleEn { get; set; }
        public string adsImage { get; set; }
        public string AdsType { get; set; }
        public Nullable<System.DateTime> EntryData { get; set; }
        public string UrlAds { get; set; }
    }
}
