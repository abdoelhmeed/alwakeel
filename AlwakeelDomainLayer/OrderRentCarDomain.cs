﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class OrderRentCarDomain
    {
        public int Id { get; set; }
        public string carInfo { get; set; }
        public Nullable<int> RId { get; set; }
        [Display(Name="Name")]
        public string Name { get; set; }
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }
        [Display(Name = "Sart Date")]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Display(Name = "End Date")]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }
    }

    public class OrdesDmonain
    {
        [Required]
        public Nullable<int> RId { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength(maximumLength:9,MinimumLength =9,ErrorMessage ="")]
        public string Phone { get; set; }
        [Required]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Required]
        public Nullable<System.DateTime> EndDate { get; set; }
      
    }
}
