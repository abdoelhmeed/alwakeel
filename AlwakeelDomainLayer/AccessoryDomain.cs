﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class AccessoryDomain
    {
        public int PId { get; set; }
        [Display(Name = "Category")]
        public Nullable<int> CatId { get; set; }
        [Required]
        [Display(Name = "Title English")]
        public string AccessoryTitleEN { get; set; }
        [Required]
        [Display(Name = "Title Arabic")]
        public string AccessoryTitleAR { get; set; }

        public Nullable<int> ACatId { get; set; }
        public Nullable<int> AcSubId { get; set; }
        [Display(Name = "For All models")]
        [Required]
        public Nullable<bool> AccessoryForAll { get; set; }
        [Required]
        [Display(Name = "Describe English")]
        public string DescribeEN { get; set; }

        [Required]
        [Display(Name = "Describe Arabic")]
        public string DescribeAR { get; set; }

        [Display(Name = "Business")]
        public Nullable<int> businessId { get; set; }
        [Required]
        [Display(Name = "Price")]
        public Nullable<decimal> AdPrice { get; set; }
        [Display(Name = "Status")]
        public string AdStatus { get; set; }
        [Display(Name = "Is New ?")]
        [Required]
        public Nullable<bool> isNew { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Date")]
        [Required]
        public DateTime EndDate { get; set; }


        public HttpPostedFileBase ImageFileOne { get; set; }
        public HttpPostedFileBase ImageFileTow { get; set; }
        public HttpPostedFileBase ImageFilethree { get; set; }
        public HttpPostedFileBase ImageFileFor { get; set; }
        public List<string> Images { get; set; }

    }

    public class AccessoryEditDomain
    {
        public int PId { get; set; }
        [Display(Name = "Category")]
        public Nullable<int> CatId { get; set; }

        [Display(Name = "Title English")]
        [Required]
        public string AccessoryTitleEN { get; set; }

        [Display(Name = "Title Arabic")]
        [Required]
        public string AccessoryTitleAR { get; set; }
        public Nullable<int> ACatId { get; set; }
        public Nullable<int> AcSubId { get; set; }
        [Display(Name = "For All models")]
        [Required]
        public Nullable<bool> AccessoryForAll { get; set; }
       
        [Display(Name = "Business")]
        public Nullable<int> businessId { get; set; }
        [Required]
        [Display(Name = "Price")]
        public Nullable<decimal> AdPrice { get; set; }
        [Display(Name = "Status")]
        public string AdStatus { get; set; }
        [Display(Name = "Is New ?")]
        [Required]
        public Nullable<bool> isNew { get; set; }
        [Required]
        [Display(Name = "Describe Arabic")]
        public string DescribeAR { get; set; }
        [Required]
        [Display(Name = "Describe English")]
        public string DescribeEN { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Date")]
        [Required]
        public DateTime EndDate { get; set; }
    }

    public class AccessoryViewDomain
    {
        public int PId { get; set; }
        [Display(Name = "Category")]
        public Nullable<int> CatId { get; set; }
       
        [Display(Name = "Product Classification")]
        public Nullable<int> AcSubId { get; set; }
        [Display(Name = "For All models")]
        public Nullable<bool> AccessoryForAll { get; set; }
        [Display(Name = "Describe Arabic")]
        public string DescribeAR { get; set; }
        [Display(Name = "Business")]
        public Nullable<int> businessId { get; set; }
        [Display(Name = "Price")]
        public Nullable<decimal> AdPrice { get; set; }
        [Display(Name = "Status")]
        public string AdStatus { get; set; }
        [Display(Name = "Is New ?")]
        public Nullable<bool> isNew { get; set; }
        public List<string> Images { get; set; }
        public List<AccessoryDetailsViewDomain> accessoryDetail { get; set; }

        [Display(Name = "Category Name AR")]
        public string CatNameAr { get; set; }
        [Display(Name = "Category Name EN")]
        public string CatNameEn { get; set; }

        [Display(Name = "Sub Name EN")]
        public string ASNameEn { get; set; }
        [Display(Name = "Sub Name AR")]
        public string AcSNameAr { get; set; }

        [Display(Name = "Main Name AR")]
        public string AMCatNameAr { get; set; }
        [Display(Name = "Main Name EN")]
        public string AMCatNameEn { get; set; }
        [Display(Name = "Main Icon")]
        public string AMCatIcon { get; set; }
        [Display(Name = "Title Arabic")]
        public string AccessoryTitleAR { get; set; }
        [Display(Name = "Title English")]
        public string AccessoryTitleEN { get; set; }
        [Display(Name = "Describe English")]
        public string DescribeEN { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Date")]
        [Required]
        public DateTime EndDate { get; set; }
    }
}
