﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public  class StateDomain
    {
        [Required]
        [Display(Name = "Id")]
        public int StateId { get; set; }
        [Required]
        [Display(Name = "State Name Arabic")]
        public string StateNameAr { get; set; }
        [Required]
        [Display(Name = "State Name English")]
        public string StateNameEn { get; set; }
    }
}
