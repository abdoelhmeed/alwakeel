﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class LocationDomain
    {
        [Display(Name ="Id")]
        public int posId { get; set; }
        [Display(Name = "Region")]
        public Nullable<int> posRegion { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        [MaxLength(10)]
        [MinLength(10)]
        public string posPhoneNumber { get; set; }

        [Display(Name = "City Id")]
        public Nullable<int> CityId { get; set; }

        [Display(Name = "State Id")]
        public int StateId { get; set; }
        [Display(Name ="POS Name")]
        public string POSName { get; set; }
        [Display(Name = "POS Type")]
        public string POStype { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string posEmail { get; set; }
        public Nullable<int> BusinessId { get; set; }
    }


    public class LocationDomainView
    {
        [Display(Name = "Id")]
        public int posId { get; set; }
        [Display(Name = "Region")]
        public Nullable<int> posRegion { get; set; }
        [Display(Name = "Region  Name AR")]
        public string RegionNameAr { get; set; }
     
        [Display(Name = "Region Name EN")]
        public string RegionNameEn { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        [MaxLength(10)]
        [MinLength(10)]
        public string posPhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string posEmail { get; set; }
        public Nullable<int> BusinessId { get; set; }

        [Display(Name = "Business Name En")]
        public string BusinessNameEn { get; set; }
        [Required]
        [Display(Name = "Business Name AR")]
        public string BusinessNameAR { get; set; }

        [Display(Name = "State Name AR")]
        public string StateNameAr { get; set; }
        [Display(Name = "State Name EN")]
        public string StateNameEn { get; set; }
        [Display(Name = "City Name AR")]
        public string CityNameAr { get; set; }
        [Display(Name = "City Name EN")]
        public string CityNameEn { get; set; }

        [Display(Name = "POS Name")]
        public string POSName { get; set; }
        [Display(Name = "POS Type")]
        public string POStype { get; set; }
    }
}
