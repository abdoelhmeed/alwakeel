﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class ContactMediaDomain
    {
        
        [Display(Name ="Id")]
        public int Id { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }
        [Display(Name = "contact")]
        [Required]
        public string contact { get; set; }
        [Display(Name = "Business Id")]
        public Nullable<int> BusinessId { get; set; }
        [Display(Name = "Business Name EN")]
        public string BusinessNameEn { get; set; }
        [Display(Name = "Business Name AR")]
        public string BusinessNameAR { get; set; }
    }
}
