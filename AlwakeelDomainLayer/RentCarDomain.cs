﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class RentCarDomain
    {
        public int Id { get; set; }
        public int CatId {get;set;}
        public Nullable<int> BusinessId { get; set; }
        public string UserId { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> CarClassId { get; set; }
        public Nullable<int> ModelId { get; set; }
        public string ImageURl { get; set; }
        public Nullable<bool> Available { get; set; }
       public HttpPostedFileBase File { get; set; }
    }

    public class RentCarAPIDomain
    {
        public int Id { get; set; }
        [Required]
        public int CatId { get; set; }
        [Required]
        public Nullable<int> BrandId { get; set; }
        [Required]
        public Nullable<int> CarClassId { get; set; }
        [Required]
        public Nullable<int> ModelId { get; set; }
        [Required]
        public string Image { get; set; }
    
    }

    public class RentCarViweDomain
    {
        public int Id { get; set; }
        public Nullable<int> BusinessId { get; set; }
        [Display(Name = "Business  English")]
        public string BusinessNameEn { get; set; }
        [Display(Name = "Business  Arabic")]
        public string BusinessNameAR { get; set; }
        [Display(Name = "Location Arabic")]
        public string BusinessLocationAr { get; set; }
        [Display(Name = "Location English")]
        public string BusinessLocationEn { get; set; }
        public string UserId { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> CarClassId { get; set; }
        public Nullable<int> ModelId { get; set; }
        public string ImageURl { get; set; }
        public Nullable<bool> Available { get; set; }


      
        [Display(Name = "Class Arabic")]
        public string CarClassNameAr { get; set; }
      
        [Display(Name = "Class English")]
        public string CarClassNameEn { get; set; }

        [Display(Name = "Model Years")]
        public Nullable<int> ModelYears { get; set; }


        [Display(Name = "Brand  Arabic")]
        public string BrandNameAr { get; set; }
        [Display(Name = "Brand  English")]
        public string BrandNameEn { get; set; }
        [Display(Name = "Brand Logo")]
        public string BrandsLogo { get; set; }
    }


    public class RentCarViweCustomerDomain
    {
        public int Id { get; set; }
       
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> CarClassId { get; set; }
        public Nullable<int> ModelId { get; set; }
        public string ImageURl { get; set; }
        public Nullable<bool> Available { get; set; }
        public string Name  { get; set; }
        public string Contact { get; set; }
        [Display(Name = "Class Arabic")]
        public string CarClassNameAr { get; set; }

        [Display(Name = "Class English")]
        public string CarClassNameEn { get; set; }

        [Display(Name = "Model Years")]
        public Nullable<int> ModelYears { get; set; }


        [Display(Name = "Brand  Arabic")]
        public string BrandNameAr { get; set; }
        [Display(Name = "Brand  English")]
        public string BrandNameEn { get; set; }
        [Display(Name = "Brand Logo")]
        public string BrandsLogo { get; set; }
    }

    public class RentCarViweApiDomain
    {
        public int RId { get; set; }
        public string OwnerNameAR { get; set; }
        public string OwnerNameEn { get; set; }
        public string phone { get; set; }
     
        public string CarInfoAR { get; set; }
        public string ImageURl { get; set; }
        public string Logo { get; set; }
        public string CarInfoEn { get; set; }
    }
}
