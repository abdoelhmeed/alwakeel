﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class BusinessSliderDomain
    {
        public int id { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<int> BusinessId { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }

    public class BSliderViewDomain
    {
        public int id { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<int> BusinessId { get; set; }
        public string BNameAR { get; set; }
        public string BNameEn { get; set; }
    }
}
