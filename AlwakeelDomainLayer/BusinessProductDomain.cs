﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class BusinessProductDomain
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Product Name EN")]
        public string PNameEN { get; set; }
        [Display(Name = "Product Name AR")]
        public string PNameAR { get; set; }
        [Display(Name = "Pimage Url")]
        public string PimageUrl { get; set; }

        [Display(Name = "Pimage Url")]
        public HttpPostedFileBase PimageUrlFile { get; set; }


        [Display(Name = "Business Id")]
        public Nullable<int> BusinessId { get; set; }

        [Required]
        [Display(Name = "Business Name En")]
        public string BusinessNameEn { get; set; }
        [Required]
        [Display(Name = "Business Name AR")]
        public string BusinessNameAR { get; set; }
        [Required]
        [Display(Name = "Describe Arabic")]
        [StringLength(maximumLength: 300)]
        public string DescribeAR { get; set; }
        [Required]
        [Display(Name = "Describe English")]
        [StringLength(maximumLength:300)]
        public string DescribeEN { get; set; }

        [Required]
        [Display(Name = "Price")]
        public Nullable<decimal> Price { get; set; }

        public string ProductType { get; set; }

    }
}
