﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public class ACSubDomain
    {
        [Display(Name = "Sub  Id")]
        public int AcSubId { get; set; }
        [Required]
        [Display(Name = "S Name En")]
        public string AcSNameEn { get; set; }
        [Display(Name = "S Name AR")]
        [Required]
        public string AcSNameAr { get; set; }
        [Display(Name = "Main Id")]
        public Nullable<int> ACatId { get; set; }
    }



    public class ACSubDomainView
    {
        [Display(Name = "Sub  Id")]
        public int AcSubId { get; set; }
        [Display(Name = "S Name En")]
        public string AcSNameEn { get; set; }
        [Display(Name = "S Name AR")]
        public string AcSNameAr { get; set; }
        [Display(Name = "M Name AR")]
        public string ACatNameAr { get; set; }
        [Display(Name = "M Name EN")]
        public string ACatNameEn { get; set; }
        [Display(Name = "Main Id")]
        public Nullable<int> ACatId { get; set; }
    }
}
