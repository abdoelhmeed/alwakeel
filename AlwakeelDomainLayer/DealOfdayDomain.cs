﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class DealOfdayDomain
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Title Ar")]
        public string TitleAr { get; set; }
        [Required]
        [Display(Name = "Title En")]
        public string TitleEn { get; set; }
        [Required]
        [Display(Name = "Old Price")]
        public decimal OldPrice { get; set; }
        [Required]
        [Display(Name = "New Price")]
        public decimal NewPrice { get; set; }
        [Required]
        [Display(Name = "Description Ar")]
        public string DescriptionAr { get; set; }
        [Required]
        [Display(Name = "Description En")]
        public string DescriptionEn { get; set; }
        [Required]
        public int NumberOfDays { get; set; }
        public string ImageOne { get; set; }
        public string ImageTow { get; set; }
        public string ImageThree { get; set; }
        public HttpPostedFileBase FileImageOne { get; set; }
        public HttpPostedFileBase FileImageTow { get; set; }
        public HttpPostedFileBase FileImageThree { get; set; }
        public int? BusinessId { get; set; }
        public string UserId { get; set; }
    }

    public class DealOfdayViewDomain
    {
        public int Id { get; set; }
        public string TitleAr { get; set; }
        public string TitleEn { get; set; }
        public decimal OldPrice { get; set; }
        public decimal NewPrice { get; set; }
        public string DescriptionAr { get; set; }
        public string DescriptionEn { get; set; }
        public int NumberOfDays { get; set; }
        public List<string> Images { get; set; }
        public string BusinessNameAr { get; set; }
        public string BusinessNameEn { get; set; }
        public string BusinessLogo { get; set; }
        public int BusinessId { get; set; }
    }
}
