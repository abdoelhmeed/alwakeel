﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class BusinessBrandDoamin
    {
        [Display(Name ="Id")]
        public int Id { get; set; }
        [Display(Name = "Business Id")]
        public Nullable<int> BusinessId { get; set; }
        [Display(Name = "Brand Id")]
        public Nullable<int> BrandId { get; set; }
        [Display(Name = "Brand Name AR")]
        public string BrandNameAr { get; set; }
        [Display(Name = "Brand Name EN")]
        public string BrandNameEn { get; set; }
        [Display(Name = "Brand Image")]
        public string BrandImage { get; set; }
        [Display(Name = "Business Name En")]
        public string BusinessNameEn { get; set; }
        [Display(Name = "Business Name AR")]
        public string BusinessNameAR { get; set; }
        [Display(Name = "Business Name AR")]
        public string BusinessImage { get; set; }
    }
}
