﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class SpecificationDomain
    {
        public int Id { get; set; }
        [Required]
        public string NameAR { get; set; }
        [Required]
        public string NameEn { get; set; }
        public string Icons { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}
