﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class BrandDomain
    {

        [Required]
        [Display(Name = "Brand Id")]
        public int BrandId { get; set; }
        [Required]
        [Display(Name = "Brand Name Arabic")]
        public string BrandNameAr { get; set; }
        [Required]
        [Display(Name = "Brand Name English")]
        public string BrandNameEn { get; set; }
        [Required]
        [Display(Name = "Brand Logo")]
        public string BrandsLogo { get; set; }
        [Required]
        [Display(Name = "Brand Category")]
        public Nullable<int> CatId { get; set; }
        [Required]
        [Display(Name = "Brand Country")]
        public string BramdCountry { get; set; }

        public HttpPostedFileBase ImgeFile { get; set; }
    }

    public class BrandDomainViews
    {
        [Display(Name = "Brand Id")]
        public int BrandId { get; set; }
        [Display(Name = "Brand Name Arabic")]
        public string BrandNameAr { get; set; }
        [Display(Name = "Brand Name English")]
        public string BrandNameEn { get; set; }
        [Display(Name = "Brand Logo")]
        public string BrandsLogo { get; set; }
        public Nullable<int> CatId { get; set; }
        [Display(Name = "Category Name English")]
        public string CatNameEn { get; set; }
        [Display(Name = "Category Name Arabic")]
        public string CatNameAr { get; set; }
        [Display(Name = "Brand Country")]
        public string BramdCountry { get; set; }
    }
}
