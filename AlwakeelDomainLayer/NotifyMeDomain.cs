﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class NotifyMeDomain
    {
        public int Id { get; set; }
        public string Cemail { get; set; }
        public string PTitleOne { get; set; }
        public string PTitle { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public string Stutus { get; set; }
    }


    public class SubscribeDomain
    {
        [EmailAddress]
        public string Email { get; set; }
    }
}
