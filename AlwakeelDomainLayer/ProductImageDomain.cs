﻿using System.Web;

namespace AlwakeelDomainLayer
{
    public class ProductImageDomain
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public int PId { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}
