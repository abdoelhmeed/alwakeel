﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class MotorcycleViweDomain
    {
        public int PId { get; set; }
        [Display(Name = "Category")]
        public string Category { get; set; }
        [Display(Name = "Category")]
        public string CategoryAr { get; set; }
        [Display(Name = "Motorcycle Info AR")]
        public string VehicleInfoAR { get; set; }
        [Display(Name = "Motorcycle Info EN")]
        public string VehicleInfoEn { get; set; }
        [Display(Name = "Price")]
        public decimal Price { get; set; }
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }
        [Display(Name = "Engine Capacity CC")]
        public int EngineCapacityCC { get; set; }
     
        [Display(Name = "Mileage KM")]
        public int MileageKM { get; set; }
        [Display(Name = "Color")]
        public int ColorId { get; set; }
        [Display(Name = "Color Name AR")]
        public string ColorNameAr { get; set; }
        [Display(Name = "Color Name En")]
        public string ColorNameEn { get; set; }
        [Display(Name = "Color Image")]
        public string ColorImage { get; set; }
        public bool? New { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
    }
}
