﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class AboutAlwakeel
    {
        public int BusinessId { get; set; }
        public string BusinessNameEn { get; set; }
        public string BusinessNameAR { get; set; }
        public string BusinessLogo { get; set; }
        public string BusinessAboutEn { get; set; }
        public string BusinessAboutAR { get; set; }
        public string BusinessPanelImage { get; set; }
        public string BusinessLocationAr { get; set; }
        public string BusinessLocationEn { get; set; }
        public List<Contact> Contact { get; set; }

    }


    
}
