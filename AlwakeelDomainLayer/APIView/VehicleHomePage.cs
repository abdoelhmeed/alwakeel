﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class VehicleHomePage
    {
        public int PId { get; set; }
        public string CatNameAR { get; set; }
        public string CatNameEn { get; set; }
        public string VehicleInfoEn { get; set; }
        public string VehicleInfoAR { get; set; }
        public string NewEn { get; set; }
        public string NewAR { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
        public Nullable<int> Views { get; set; }
        public Nullable<int> love { get; set; }
    }

    public class VehicleList
    {
        public int PId { get; set; }
        public string CatNameAR { get; set; }
        public string CatNameEn { get; set; }
        public string VehicleInfoEn { get; set; }
        public string VehicleInfoAR { get; set; }
        public string NewEn { get; set; }
        public string NewAR { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
    }
}
