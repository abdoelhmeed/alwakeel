﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
   public class CompanyWithBrand
    {
        public int? BrandId { get; set; }
        public string BrandNameAr { get; set; }
        public string BrandNameEn { get; set; }
        public string BrandsLogo { get; set; }
    }
}
