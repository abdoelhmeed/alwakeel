﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class PorductDetailModel
    {
        public string ownerNameAR;
        public string ownerNameEn;

        public int PId { get; set; }
        public string Category { get; set; }
        public int CatId { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public Nullable<decimal> Price { get; set; }
        public List<string> Images { get; set; }
        public string NewAR { get; set; }
        public string NewEn { get; set; }
        public string DescribeAR { get; set; }
        public string DescribeEN { get; set; }
        public Nullable<int> Views { get; set; }
        public Nullable<int> love { get; set; }


        public string ColorIcon { get; set; }
        public string ColorAR { get; set; }
        public string ColorEn { get; set; }

        public Nullable<int> EngineCapacityCC { get; set; }
        public Nullable<int> MileageKM { get; set; }

        public List<CarFeature> CarsFeature { get; set; }

        public string AccessoryTypeAR { get; set; }
        public string AccessoryTypeEn { get; set; }

        public Nullable<bool> AccessoryForAll { get; set; }
        public Nullable<int> businessId { get; set; }
        public string userId { get; set; }
       public List<AccessoryDetailsViewDomain> AccessoryModels { get; set; }
        public List<PordectList> SimilarAds { get; set; }
        public int? AccessorySubTypeId { get; set; }
        public int? CarClassId { get; set; }


        public List<Contact> contacts { get; set; }
        public string Logo { get; set; }
    }

    public class CarFeature
    {
        public string FeatureAr { get; set; }
        public string FeatureEn { get; set; }
        public string Icons { get; set; }
    }

    public class SimilarContact
    {
        public List<PordectList> SimilarAds { get; set; }
        public List<Contact> contacts { get; set; }
    }


}
