﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class AccessoryAutoPartList
    {
        public int PId { get; set; }
        public string Category { get; set; }
        public string AccessoryTitleEN { get; set; }
        public Nullable<bool> AccessoryForAll { get; set; }
        public Nullable<decimal> AdPrice { get; set; }
        public string Images { get; set; }
        public string NewAR { get; set; }
        public string NewEn { get; set; }
        public string AccessoryTitleAR { get; set; }
        public Nullable<int> Views { get; set; }
        public Nullable<int> love { get; set; }
    }
}
