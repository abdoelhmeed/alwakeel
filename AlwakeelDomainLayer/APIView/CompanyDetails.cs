﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class CompanyDetails
    {
        public int BusinessId { get; set; }
        public string BusinessType { get; set; }
        public string BusinessNameEn { get; set; }
        public string BusinessNameAR { get; set; }
        public string BusinessLogo { get; set; }
        public string BusinessAboutEn { get; set; }
        public string BusinessAboutAR { get; set; }
        public List<string> BusinessPanelImage { get; set; }
        public List<CompanyWithBrand> companyWithBrand { get; set; }
        public List<Contact> Contact { get; set; }
        public List<AgentProduct> Products { get; set; }
        public string BusinessLocationAr { get; set; }
        public string BusinessLocationEn { get; set; }
    }
}
