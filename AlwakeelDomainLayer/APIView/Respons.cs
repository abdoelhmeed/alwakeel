﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class Respons
    {
        public string MssAR { get; set; }
        public string MssEn { get; set; }
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
