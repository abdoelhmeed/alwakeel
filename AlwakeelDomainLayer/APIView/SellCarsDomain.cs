﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer.APIView
{
    public class SellCarsDomain
    {
        public int PId { get; set; }
        [Required]
        public int CatId { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public int CarClassId { get; set; }
        [Required]
        public int ModelId { get; set; }
        [Required]
        public decimal AdPrice { get; set; }
        [Required]
        public string DescribeAR { get; set; }
        [Required]
        public string DescribeEN { get; set; }
        [Required]
        public bool isNew { get; set; }
        [Required]
        public int colorId { get; set; }
        [Required]
        public int CEngineCapacityCC { get; set; }
        [Required]
        public int CMileageKM { get; set; }
        public string UserId { get; set; }
        [Required]
        public List<string> ImagePordect { get; set; }
        public List<int> Features { get; set; }

    }

    public class CustomerproductsDomain
    {
        public int? CatId { get; set; }
        [Display(Name = "Id")]
        public int? PId { get; set; }
        [Display(Name = "Category")]
        public string Category { get; set; }
        [Display(Name = "Price")]
        public decimal Price { get; set; }
        [Display(Name = "Entry Date")]
        public DateTime AdEntryDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime AdendDate { get; set; }
        [Display(Name = "IS New ?")]
        public string NewEn { get; set; }
        [Display(Name = "Color")]
        public string ColorName { get; set; }
        [Display(Name = "Engine Capacity CC")]
        public int? CEngineCapacityCC { get; set; }
        [Display(Name = "Mileage KM")]
        public int? CMileageKM { get; set; }
        public string UserId { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Contact")]
        public string Contact { get; set; }
        [Display(Name = "Describe AR")]
        public string DescribeAR { get; set; }
        [Display(Name = "Describe En")]
        public string DescribeEN { get; set; }
        public string Info { get; set; }

    }


    


    public class SellAccessoryDomain
    {
        public int PId { get; set; }
        [Required]
        public int CatId { get; set; }
        [Required]
        public int MainAccessoriesType { get; set; }
        [Required]
        public int SubAccessoriesType { get; set; }
        [Required]
        public string AccessoryTitleEN { get; set; }
        [Required]
        public string AccessoryTitleAR { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string DescribeAR { get; set; }
        [Required]
        public string DescribeEN { get; set; }
        [Required]
        public bool isNew { get; set; }
        public string UserId { get; set; }
        [Required]
        public List<string> ImagePordect { get; set; }
        public bool AccessoryForAll { get; set; }
        public List<AccessorySupportDetails> SupportDetails { get; set; }
    }

    public class AccessorySupportDetails
    {
        public int CatId { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public int CarClassId { get; set; }
        [Required]
        public int ModelId { get; set; }
    }

}
