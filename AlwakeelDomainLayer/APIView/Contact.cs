﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.APIView
{
    public class Contact
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string contact { get; set; }
        public string Icons   { get; set; }
    }
}
