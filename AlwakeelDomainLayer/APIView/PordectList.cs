﻿using System;
using System.Collections.Generic;

namespace AlwakeelDomainLayer.APIView
{
    public class PordectList
    {
        public int PId { get; set; }
        public string Category { get; set; }
        public int CatId { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Images { get; set; }
        public string NewAR { get; set; }
        public string NewEn { get; set; }
        public Nullable<int> Views { get; set; }
        public Nullable<int> love { get; set; }
    }

    public class Filter
    {
        public int CatId { get; set; }
        public int BrandId { get; set; }
        public int ClassId { get; set; }
        public int ModelId { get; set; }
        public int coloreId { get; set; }
        public decimal price { get; set; }
        public int BId { get; set; }
        public int MainAccessoriesId { get; set; }
        public int SubAccessoriesId { get; set; }
        public bool IsAccessories { get; set; }
        public bool ISNew { get; set; }
        public int Skep { get; set; }
        public int Take { get; set; }
    }

    public class MainAccessories
    {
        public int MainAccessoriesId { get; set; }
        public string NameEn { get; set; }
        public string NameAR { get; set; }
        public string Icos { get; set; }
      
    }


    public class ManiWithSubAccessories
    {
        public int MainAccessoriesId { get; set; }
        public string NameEn { get; set; }
        public string NameAR { get; set; }
        public string Icos { get; set; }
        public List<SubAccessories> SubList { get; set; }
    }

    public class SubAccessories
    {
        public int SubAccessoriesId { get; set; }
        public string NameEn { get; set; }
        public string NameAR { get; set; }
    }


    public class Colors
    {
        public int coloreId { get; set; }
        public string NameEn { get; set; }
        public string NameAR { get; set; }
        public string Icon { get; set; }
    }

}
