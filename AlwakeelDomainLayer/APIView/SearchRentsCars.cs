﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer.APIView
{
    public class SearchRentsCars
    {
        
        public int CatId { get; set; }
        [Required]
        public int BrandId { get; set; }
        [Required]
        public int CarClassId { get; set; }
        [Required]
        public int ModelId { get; set; }

        public int Take { get; set; }
        public int Skip { get; set; }
    }
}
