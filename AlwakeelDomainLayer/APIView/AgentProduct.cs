﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AlwakeelDomainLayer.APIView
{
    public class AgentProduct
    {
        public int id { get; set; }
        public string PNameEN { get; set; }
        public string PNameAR { get; set; }
        public string PimageUrl { get; set; }
        public decimal? Price { get; set; }
        public string PType { get; set; }

        public string DescribeAR { get; set; }
        public string DescribeEN { get; set; }
    }
}