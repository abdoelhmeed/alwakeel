﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer.APIView
{
    public class ExternalAccessoryDomain
    {
        public int PId { get; set; }
        public Nullable<int> CatId { get; set; }
        [Required]
        public string AccessoryTitleEN { get; set; }
        [Required]
        public string AccessoryTitleAR { get; set; }
        [Required]
        public int SubAccessory { get; set; }
        [Required]
        public int MamAccessory { get; set; }
        [Required]
        public Nullable<bool> AccessoryForAll { get; set; }
        [Required]
        public string DescribeEN { get; set; }
        [Required]
        public string DescribeAR { get; set; }
        [Required]
        public Nullable<decimal> AdPrice { get; set; }
        [Required]
        public bool isNew { get; set; }
        [Required]
        public List<string> Images { get; set; }

    }

   
    public class ExternalAccessoryViewDomain
    {
        public int PId { get; set; }
        [Display(Name = "Category")]
        public Nullable<int> CatId { get; set; }
       
        [Display(Name = "Product Classification")]
        public Nullable<int> AcSubId { get; set; }
        [Display(Name = "For All models")]
        public Nullable<bool> AccessoryForAll { get; set; }
        [Display(Name = "Describe Arabic")]
        public string DescribeAR { get; set; }
        [Display(Name = "Business")]
        public Nullable<int> businessId { get; set; }
        [Display(Name = "Price")]
        public Nullable<decimal> AdPrice { get; set; }
        [Display(Name = "Status")]
        public string AdStatus { get; set; }
        [Display(Name = "Is New ?")]
        public Nullable<bool> isNew { get; set; }
        public List<string> Images { get; set; }
        public List<AccessoryDetailsViewDomain> accessoryDetail { get; set; }

        [Display(Name = "Category Name AR")]
        public string CatNameAr { get; set; }
        [Display(Name = "Category Name EN")]
        public string CatNameEn { get; set; }

        [Display(Name = "Sub Name EN")]
        public string ASNameEn { get; set; }
        [Display(Name = "Sub Name AR")]
        public string AcSNameAr { get; set; }

        [Display(Name = "Main Name AR")]
        public string AMCatNameAr { get; set; }
        [Display(Name = "Main Name EN")]
        public string AMCatNameEn { get; set; }
        [Display(Name = "Main Icon")]
        public string AMCatIcon { get; set; }
        [Display(Name = "Title Arabic")]
        public string AccessoryTitleAR { get; set; }
        [Display(Name = "Title English")]
        public string AccessoryTitleEN { get; set; }
        [Display(Name = "Describe English")]
        public string DescribeEN { get; set; }
    }
}
