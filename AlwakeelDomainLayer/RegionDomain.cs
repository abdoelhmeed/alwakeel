﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public class RegionDomain
    {
        [Required]
        [Display(Name = "Region Id")]
        public int RegionId { get; set; }
        [Required]
        [Display(Name = "Region  Name AR")]
        public string RegionNameAr { get; set; }
        [Required]
        [Display(Name = "Region Name EN")]
        public string RegionNameEn { get; set; }
        [Display(Name = "City Id")]
        public Nullable<int> CityId { get; set; }
        [Display(Name = "State Id")]
        public int StateId { get; set; }
        [Display(Name = "State Name Arabic")]
        public string StateNameAr { get; set; }
        [Display(Name = "State Name English")]
        public string StateNameEn { get; set; }
        [Display(Name = "City Name Arabic")]
        public string CityNameAr { get; set; }
        [Display(Name = "City Name Arabic")]
        public string CityNameEn { get; set; }
    }
}
