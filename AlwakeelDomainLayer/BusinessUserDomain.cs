﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public  class BusinessUserDomain
    {
        public string UserId { get; set; }
        public Nullable<int> BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string UserName { get; set; }
        public string typeOrRole { get; set; }
    }

}
