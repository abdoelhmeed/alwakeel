﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public class AccessoryDetailsDomain
    {

        public int Id { get; set; }
        public int CatId { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> CarClassId { get; set; }
        public Nullable<int> ModelId { get; set; }
        public Nullable<int> PId { get; set; }
    }

    public class AccessoryDetailsViewDomain
    {

        public int Id { get; set; }
        public string BrandNameAR { get; set; }
        public string BrandNameEN { get; set; }

        public string ClassNameAR { get; set; }
        public string ClassNameEN { get; set; }
      
        public Nullable<int> ModelId { get; set; }

        public Nullable<int> ModelYears { get; set; }
        public Nullable<int> PId { get; set; }
    }
}
