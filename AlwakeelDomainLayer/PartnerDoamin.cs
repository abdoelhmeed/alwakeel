﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public class PartnerDoamin
    {
        public string Id { get; set; }
       
        [Display(Name = "Business Name EN")]
        public string userNameEn { get; set; }
        
       
        [Display(Name = "Business Name AR")]
        public string userNameAr { get; set; }
       
        [Display(Name = "Logo")]
        public string userLogo { get; set; }
       
        [Display(Name = "About Business EN")]
        public string userAboutEn { get; set; }

       
        [Display(Name = "About Business AR")]
        public string userAboutAr { get; set; }

        [Display(Name = "Panel Image")]
        public DateTime userregistrationdate { get; set; }
        public string userPanelImage { get; set; }

    }
}
