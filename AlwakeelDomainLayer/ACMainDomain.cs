﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    /// <summary>
    /// Accessory Sub Category
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public class ACMainDomain
    {
        [Display(Name ="M Cat Id")]
        public int ACatId { get; set; }
        [Display(Name = "M Name Ar")]
        [Required]
        public string ACatNameAr { get; set; }
        [Display(Name = "M Name EN")]
        [Required]
        public string ACatNameEn { get; set; }
        [Display(Name = "Icon")]
        public string ACatIcon { get; set; }

        public HttpPostedFileBase IconFile { get; set; }
    }
}
