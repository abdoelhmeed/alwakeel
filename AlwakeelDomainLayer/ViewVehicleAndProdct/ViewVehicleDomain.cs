﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.ViewVehicleAndProdct
{
    public class ProdeuctDetailsDomain
    {
        public int PId { get; set; }
        public int BrandId { get; set; }
        public int ClassId { get; set; }
        public int ModelId { get; set; }
        public string type { get; set; }
        public string NameEn { get; set; }
        public string NameAR { get; set; }
        public List<string> Image { get; set; }
        public decimal Price { get; set; }
        public Seller Seller { get; set; }
        public  List<Contact> Contacts { get; set; }
        public List<Features> Features { get; set; }
        public List<SimilarAds> SimilarsAds { get; set; }


    }

  
    public class  Contact
    {
        public string Type { get; set; }
        public string ourContact { get; set; }
    }

    public class Seller
    {
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string Logo { get; set; }
        public DateTime RegDate { get; set; }
    }

    public class Features
    {
        public string FNameAR { get; set; }
        public string FNameEN { get; set; }
        public string FLogo { get; set; }
    }

    public class SimilarAds 
    {
        public int PId { get; set; }
        public string VehicleTypeAR { get; set; }
        public int ClassId { get; set; }
        public int ModelId { get; set; }
        public string VehicleTypeEN { get; set; }
        public string VehicleInfoAR { get; set; }
        public string VehicleInfoEN { get; set; }
        public string VehicleInfoImage { get; set; }
        public decimal VehiclePrice { get; set; }

    }





    public class ViewVehicleListDomain
    {
        public int PId { get; set; }
        public string VehicleTypeAR { get; set; }
        public int BrandId { get; set; }
        public int ClassId { get; set; }
        public int ModelId { get; set; }
        public string VehicleTypeEN { get; set; }
        public string VehicleInfoAR { get; set; }
        public string VehicleInfoEN { get; set; }
        public string VehicleInfoImage { get; set; }
        public decimal VehiclePrice { get; set; }
    }


}
