﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class VideoDomain
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Video Name EN")]
        public string VideoNameAr { get; set; }
        [Display(Name = "Video Name EN")]
        public string VideoNameEn { get; set; }
        [Display(Name = "Video Url")]
        public string VideoUrl { get; set; }
        [Display(Name = "Video Name Ar")]
        public System.DateTime VideoDate { get; set; }
    }
}
