﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class PVehicleViewDomain
    {
        [Display(Name = "Id")]
        public int PId { get; set; }


        [Display(Name = "Category")]
        public int CatId { get; set; }


        [Display(Name = "Cat NameAR")]
        public string CatNameAr { get; set; }


        [Display(Name = "Cat Name EN")]
        public string CatNameEn { get; set; }


        [Display(Name = "Brand")]
        public int BrandId { get; set; }


        [Display(Name = "Brand Name Arabic")]
        public string BrandNameAr { get; set; }


        [Display(Name = "Brand Name English")]
        public string BrandNameEn { get; set; }


        [Display(Name = "Class")]
        public int CarClassId { get; set; }


        [Display(Name = "Class Name AR")]
        public string CarClassNameAr { get; set; }

        [Display(Name = "Class Name EN")]
        public string CarClassNameEn { get; set; }


        [Display(Name = "Model")]
        public int ModelId { get; set; }

        [Display(Name = "Model Years")]
        public Nullable<int> ModelYears { get; set; }


        [Display(Name = "Price")]
        public decimal AdPrice { get; set; }


        [Display(Name = "Entry Date")]
        public DateTime AdEntryDate { get; set; }


      


        [Display(Name = "Status")]
        public string AdStatus { get; set; }


        [Display(Name = "IS New ?")]
        public bool isNew { get; set; }


        [Display(Name = "Color")]
        public int CColors { get; set; }

        [Display(Name = "Color Name AR")]
        public string ColorNameAr { get; set; }
        [Display(Name = "Color Name En")]
        public string ColorNameEn { get; set; }

        [Display(Name = "Color Image")]
        public string ColorImage { get; set; }

        [Display(Name = "Transmission")]
        public int? CTransmission { get; set; }


        [Display(Name = "Engine Capacity CC")]
        public int CEngineCapacityCC { get; set; }


        [Display(Name = "Mileage KM")]
        public int CMileageKM { get; set; }

        [Display(Name = "Business")]
        public int businessId { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Business Name En")]
        public string BusinessNameEn { get; set; }


        [Display(Name = "Business Name AR")]
        public string BusinessNameAR { get; set; }
        public string DescribeAR { get; set; }
        public string DescribeEN { get; set; }
    }

}
