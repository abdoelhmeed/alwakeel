﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public  class VehicleCustomModel
    {
        public int PId { get; set; }
        [Display(Name = "Category")]
        public string Category { get; set; }

        [Display(Name = "Category")]
        public string CategoryAr { get; set; }

        [Display(Name = "Vehicle Info AR")]
        public string VehicleInfoAR { get; set; }
        [Display(Name = "Vehicle Info EN")]
        public string VehicleInfoEn { get; set; }
        [Display(Name = "Price")]
        public decimal Price { get; set; }
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }
        [Display(Name = "Engine Capacity CC")]
        public string EngineCapacityCC { get; set; }
        [Display(Name = "Transmission")]
        public string CTransmission { get; set; }
        [Display(Name = "Mileage KM")]
        public string MileageKM { get; set; }
        [Display(Name = "Color")]
        public int ColorId { get; set; }
        [Display(Name = "Color Name AR")]
        public string ColorNameAr { get; set; }
        [Display(Name = "Color Name En")]
        public string ColorNameEn { get; set; }
        [Display(Name = "Color Image")]
        public string ColorImage { get; set; }

        public bool? New { get; set; }
    }
}
