﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class CarClassDomain
    {
        [Required]
        [Display(Name = "Brand Id")]
        public int CarClassId { get; set; }
        [Required]
        [Display(Name = "Class Name Arabic")]
        public string CarClassNameAr { get; set; }
        [Required]
        [Display(Name = "Class Name English")]
        public string CarClassNameEn { get; set; }
        [Required]
        [Display(Name = "Brand Name")]
        public Nullable<int> BrandId { get; set; }

        [Required]
        [Display(Name = "Category")]
        public Nullable<int> CatId { get; set; }
    }


    public class CarClassDomainViews
    {
       
        [Display(Name = "Class Id")]
        public int CarClassId { get; set; }
      
        [Display(Name = "Class Name Arabic")]
        public string CarClassNameAr { get; set; }
      
        [Display(Name = "Class Name English")]
        public string CarClassNameEn { get; set; }
       
        [Display(Name = "Brand Name")]
        public Nullable<int> BrandId { get; set; }
        [Display(Name = "Brand Name Arabic")]
        public string BrandNameAr { get; set; }
      
        [Display(Name = "Brand Name English")]
        public string BrandNameEn { get; set; }
    }
}
