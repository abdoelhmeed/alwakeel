﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
  public  class CityDomain
    {
        [Display(Name = "City Id")]
        public int CityId { get; set; }
        [Display(Name = "City Name AR")]
        public string CityNameAr { get; set; }
        [Display(Name = "City Name EN")]
        public string CityNameEn { get; set; }
        [Display(Name = "State Id")]
        public Nullable<int> StateId { get; set; }
        [Display(Name = "State Name Arabic")]
        public string StateNameAr { get; set; }
        [Display(Name = "State Name English")]
        public string StateNameEn { get; set; }
    }
}
