﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class PFVVeiwDomain
    {
        [Display(Name = "Feature Id")]
        public int FId { get; set; }
        [Display(Name = "P F Id")]
        public Nullable<int> PId { get; set; }
        [Display(Name = "Specification Id")]
        public Nullable<int> SpeId { get; set; }
        [Display(Name = "Auto Specifications AR")]
        public string SpeAr { get; set; }
        [Display(Name = "Auto Specifications EN")]
        public string SpeEn { get; set; }
        [Display(Name = "Icons")]
        public string Icons { get; set; }
    }

}
