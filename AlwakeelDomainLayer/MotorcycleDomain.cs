﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class MotorcycleDomain
    {
        [Display(Name = "Id")]
        public int PId { get; set; }
        public int CatId { get; set; }
        [Display(Name = "Brand")]
        [Required]
        public int BrandId { get; set; }
        [Display(Name = "Class")]
        [Required]
        public int CarClassId { get; set; }
        [Display(Name = "Model")]
        [Required]
        public int ModelId { get; set; }
        [Display(Name = "Price")]
        [Required]
        public decimal AdPrice { get; set; }

        [Display(Name = "IS New ?")]
        [Required]
        public bool isNew { get; set; }
        [Display(Name = "Color")]
        [Required]
        public int CColors { get; set; }

        [Display(Name = "Engine Capacity CC")]
        [Required]
        public int CEngineCapacityCC { get; set; }
        [Display(Name = "Mileage KM")]
        [Required]
        public int CMileageKM { get; set; }
        [Display(Name = "Describe Arabic")]
        [Required]
        public string DescribeAR { get; set; }
        [Display(Name = "Describe English")]
        [Required]
        public string DescribeEN { get; set; }
        public int businessId { get; set; }
        public HttpPostedFileBase ImageFileOne { get; set; }
        public HttpPostedFileBase ImageFileTow { get; set; }
        public HttpPostedFileBase ImageFilethree { get; set; }
        public HttpPostedFileBase ImageFileFor { get; set; }

        [Display(Name = "Motorcycle Features ")]
        public List<int> Features { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Date")]
        [Required]
        public DateTime EndDate { get; set; }
    }
}
