﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
   public class CarModelDomain
   {
        [Required]
        [Display(Name = "Model Id")]
        public int ModelId { get; set; }
        [Required]
        [Display(Name = "Model Years")]
        public Nullable<int> ModelYears { get; set; }
        [Required]
        [Display(Name = "Class Id")]
        public Nullable<int> CarClassId { get; set; }
        public int BrandId { get; set; }
        [Display(Name = "Category Id")]
        public Nullable<int> CatId { get; set; }
       public List<int> GeneralSpec { get; set; }
        
    }

    public class CarModelDomainViews
    {
        [Required]
        [Display(Name = "Model Id")]
        public int ModelId { get; set; }
        [Required]
        [Display(Name = "Model Years")]
        public Nullable<int> ModelYears { get; set; }
        [Required]
        [Display(Name = "Class Id")]
        public Nullable<int> CarClassId { get; set; }
        public int BrandId { get; set; }
        [Display(Name = "Category Id")]
        public Nullable<int> CatId { get; set; }

        [Display(Name = "Brand Name Arabic")]
        public string BrandNameAr { get; set; }
        [Display(Name = "Brand Name English")]
        public string BrandNameEn { get; set; }

        [Display(Name = "Category Name Arabic")]
        public string CategoryNameAr { get; set; }
        [Display(Name = "Category Name English")]
        public string CategoryNameEn { get; set; }

        [Display(Name = "Class Name Arabic")]
        public string CarClassNameAr { get; set; }

        [Display(Name = "Class Name English")]
        public string CarClassNameEn { get; set; }
    }
}
