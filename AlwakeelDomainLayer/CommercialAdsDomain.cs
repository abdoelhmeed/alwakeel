﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class CommercialAdsDomain
    {
        [Display(Name ="Id")]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Title AR")]
        public string TitleAr { get; set; }
        [Required]
        [Display(Name = "Title EN")]
        public string TitleEn { get; set; }
        [Required]
        [Display(Name = "ads Image")]
        public string adsImage { get; set; }
        [Required]
        [Display(Name = "Type Image")]
        public string AdsType { get; set; }
        [Display(Name = "Entry Data")]
        public DateTime EntryData { get; set; }

        [Display(Name = "URl(Link)")]
        public string UrlAds { get; set; }

        [Display(Name = "ads Image Fill")]
        public HttpPostedFileBase adsImageFill { get; set; }
    }

}
