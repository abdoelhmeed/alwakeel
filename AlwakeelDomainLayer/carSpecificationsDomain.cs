﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class carSpecificationsDomain
    {
        [Required]
        [Display(Name = "Specifications Id")]
        public int SpeId { get; set; }
        [Required]
        [Display(Name = "Auto Specifications AR")]
        public string SpeAr { get; set; }
        [Required]
        [Display(Name = "Auto Specifications EN")]
        public string SpeEn { get; set; }
      
        [Display(Name = "Specifications ICon")]
        public string SpeIcon { get; set; }
       
        [Display(Name = "Brand")]
        public Nullable<int> BrandId { get; set; }
       
        [Display(Name = "Class")]
        public Nullable<int> CarClassId { get; set; }
       
        [Display(Name = "Model")]
        public Nullable<int> ModelId { get; set; }
      
        [Display(Name = "Category")]
        public int CatId { get; set; }

        [Display(Name = "Specifications File")]
        public HttpPostedFileBase IconFile { get; set; }

    }

    public class FeaturesDomain
    {
        public int FId { get; set; }
        public string FNameAR { get; set; }
        public string FNameEn { get; set; }
        public string FIcon { get; set; }
    }

}
