﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class BusinessDomain
    {
      
        [Display(Name = "Id")]
        public int BusinessId { get; set; }
        [Required]
        [Display(Name = "Name English")]
        public string BusinessNameEn { get; set; }
        [Required]
        [Display(Name = "Name Arabic")]
        public string BusinessNameAR { get; set; }
        [Required]
        [Display(Name = "Logo")]
        public string BusinessLogo { get; set; }
        [Required]
        [Display(Name = "About English")]
        public string BusinessAboutEn { get; set; }
        [Required]
        [Display(Name = " About Arabic")]
        public string BusinessAboutAR { get; set; }
        [Required]
        [Display(Name = "Registration Date")]
        public System.DateTime BusinessRegistrationRate { get; set; }
        [Required]
        [Display(Name = "Panel Image")]
        public string BusinessPanelImage { get; set; }
        [Required]
        [Display(Name = "Business Type")]
        public string BusinessType { get; set; }
        [Display(Name = "Panel Image")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase BusinessPanelImageFIlle { get; set; }

        [Required]
        [Display(Name ="Location Arabic")]
        public string BusinessLocationAr { get; set; }
        [Display(Name = "Location English")]
        [Required]
        public string BusinessLocationEn { get; set; }

        [Display(Name = "Logo")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase BusinessLogoFIlle { get; set; }

        [Display(Name = "is Special Company?")]
        public Nullable<bool> BusinessSpecial { get; set; }

        [Display(Name = "Agent Type")]
        public string AgentType { get; set; }

        public Nullable<bool> active { get; set; }
    }
}
