﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class PFVehicleDomain
    {
        [Display(Name = "Feature Id")]
        public int FId { get; set; }
        [Display(Name = "P F Id")]
        public Nullable<int> PId { get; set; }
        [Display(Name = "Specification Id")]
        public Nullable<int> SpeId { get; set; }
    }

}
