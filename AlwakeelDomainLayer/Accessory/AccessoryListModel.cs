﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer.Accessory
{
    public class AccessoryListModel
    {
        public int PId { get; set; }
        public int CatId { get; set; }
        public string CatName { get; set; }
        public string AccessoryTitle { get; set; }
        public string Image { get; set; }
        public Nullable<decimal> AdPrice { get; set; }
    }
}
