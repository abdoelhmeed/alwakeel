﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelDomainLayer
{
    public class CategoryDomain
    {
        public int CatId { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
