﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{

    public  class BlogDomain
    {
        [Display(Name ="Id")]
        public int blogId { get; set; }
        [Display(Name = "Title EN")]
        [Required]
        public string blogTitleEn { get; set; }
        [Display(Name = "Title AR")]
        [Required]
        public string blogTitleAr { get; set; }
        [Display(Name = "Body En")]
        [Required]
       
        public string blogBodyEn { get; set; }
        [Display(Name = "Body AR")]
        [Required]
        
        public string blogBodyAr { get; set; }
        [Display(Name = "Publish Date")]
        public System.DateTime blogDate { get; set; }
        [Display(Name = "Iamge")]
        public string blogIamgeUrl { get; set; }
        [Display(Name = "Views")]
        public Nullable<int> ViewsNuber { get; set; }
        public string PublishBy { get; set; }
        public HttpPostedFileBase IamgeFile { get; set; }

    }
}
