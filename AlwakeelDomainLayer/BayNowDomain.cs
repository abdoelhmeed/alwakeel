﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AlwakeelDomainLayer
{
    public class BayNowDomain
    {
        public int Id { get; set; }
        [Required]
        public string EmpName { get; set; }
        [Required]
        public string contact { get; set; }
        [Required]
        public string Product { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public int BusinessId { get; set; }
        [Required]
        public string DescriptionOrMote { get; set; }
    }


    public class BayNowViewDomain
    {
        public int Id { get; set; }
        [Display(Name ="Name")]
        public string EmpName { get; set; }
        public string contact { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public int BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string Done { get; set; }
        public string DescriptionOrMote { get; set; }
    }
}
