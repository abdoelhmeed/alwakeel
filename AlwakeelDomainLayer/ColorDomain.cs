﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AlwakeelDomainLayer
{
    public class ColorDomain
    {
        [Display(Name ="Id")]
        public int colorId { get; set; }
        [Display(Name = "Name AR")]
        [Required]
        public string colorIdNameAR { get; set; }
        [Display(Name = "Name EN")]
        [Required]
        public string colorIdNameEN { get; set; }
        [Display(Name = "Icon")]
        public string colorIcon { get; set; }
        [Display(Name = "Icon")]
        public HttpPostedFileBase IconFile { get; set; }
    }
}
