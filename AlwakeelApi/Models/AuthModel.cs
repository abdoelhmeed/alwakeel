﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AlwakeelApi.Models
{
    public static class AuthModel
    {
      
        public static dynamic GenerateToken(ApplicationUser user)
        {
            OtpCode userinfo = new OtpCode();

            Userinfo myInfo = userinfo.UserInfo(user.Id);
            var tokenExpiration = TimeSpan.FromDays(20);
            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            identity.AddClaim(new Claim("role", "User"));
            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };
            var ticket = new AuthenticationTicket(identity, props);
            var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
            JObject tokenResponse = new JObject(
                new JProperty("access_token", accessToken),
                new JProperty("token_type", "bearer"),
               new JProperty("FullName", myInfo.FullName),
                new JProperty("contact", myInfo.Mycontact));
               
            return tokenResponse;
        }
        public static async Task<string> SendCode(string Phone, string body)
        {
            HttpClient smsHttp = new HttpClient();
            var user = "Alwakeel";
            var password = "Alwakeel@123";
            var sender = "Alwakeel";

            string smsUrl = "http://ivorycell.info/webacc.aspx?user=" + user + "&pwd=" + password + "&smstext=" + body + "&Sender=" + sender + "&Nums=249" + Phone;
            HttpResponseMessage res = await smsHttp.GetAsync(smsUrl);
            if (res.IsSuccessStatusCode)
            {
                // Get the response
                var customerJsonString = res.Content.ReadAsStringAsync();
                return customerJsonString.Result;
            }
            else
            {
                return "The message has not been sent. Try again";
            }
        }
        public static bool SendEmail(string toEmail, string subject, string emailBody)
        {


            string senderEmail = "Alerts@alwakeelonline.com";
            string senderPassword = "Abdo@0489";

            SmtpClient client = new SmtpClient("mail.alwakeelonline.com", 8889);
            client.EnableSsl = false;
            client.Timeout = 100000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(senderEmail, senderPassword);

            MailMessage mailMessage = new MailMessage(senderEmail, toEmail, subject, emailBody);
            mailMessage.IsBodyHtml = true;
            mailMessage.BodyEncoding = UTF8Encoding.UTF8;
            client.Send(mailMessage);

            return true;


        }
    }
}