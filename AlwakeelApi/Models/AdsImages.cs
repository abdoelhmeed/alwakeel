﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlwakeelApi.Models
{
    public class AdsImages
    {
        public string Image { get; set; }
        public string Link { get; set; }
    }


    public class AdsOrderImages
    {
        public  int Number { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
    }
}