﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlwakeelApi.Models
{
    public class Response
    {
        public string Status { get; set; }
        public string MessageAr { get; set; }
        public string MessageEn { get; set; }

    }
}