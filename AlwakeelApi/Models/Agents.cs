﻿using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlwakeelApi.Models
{

    public class Agents
    {
        public int BusinessId { get; set;}
        public string BusinessNameEn { get; set;}
        public string BusinessNameAR { get; set; }
        public string BusinessLogo { get; set; }
        public string BusinessAboutEn { get; set;}
        public string BusinessAboutAR { get; set; }
        public List<string> BusinessPanelImage { get; set;}
        public string AgentType { get; set;}
        public List<AgentProduct> Product { get; set;}
        public List<Contact> Contact { get; set;}
    }
}