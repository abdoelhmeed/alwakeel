﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/subscribe")]
    public class subscribeController : ApiController
    {
        NotifyMeSubscribe notify = new NotifyMeSubscribe();
        [HttpPost]
        [Route("CustomerSubscribe")]
        public IHttpActionResult CustomerSubscribe(SubscribeDomain model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }
            notify.AddSubscribe(model);
            return Ok(new Response { Status = "success", MessageAr = "تم الاضافة بنجاح", MessageEn = "successfully" });
        }
    }
}
