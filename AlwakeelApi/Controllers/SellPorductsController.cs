﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelBusinessLayer.Implementation.PordectsAPI;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/SellPorducts")]
    public class SellPorductsController : ApiController
    {
        Category category = new Category();
        ACMains MAccessory = new ACMains();
        ACSubs subs = new ACSubs();
        Brand brand = new Brand();
        CarClasses carClasses = new CarClasses();
        CarModel carModel = new CarModel();
        Customerproducts sell = new Customerproducts();
        CSpecifications cs = new CSpecifications();
      

        [HttpGet]
        [Route("GetCarsCategories")]
        public List<CategoryDomain> GetCarsCategories()
        {
            List<CategoryDomain> model = category.GetByIds(1, 3);
            return model;
        }

        [HttpGet]
        [Route("GetCarsBrand")]
        public List<BrandDomainViews> GetCarsBrand(int CatId)
        {
            List<BrandDomainViews> model = brand.GetByCatId(CatId);
            return model;
        }

        [HttpGet]
        [Route("GetCarsClass")]
        public List<CarClassDomainViews> GetCarsClass(int BrandId)
        {
            List<CarClassDomainViews> model = carClasses.GetByBrandId(BrandId);
            return model;
        }

        [HttpGet]
        [Route("GetCarsModel")]
        public List<CarModelDomainViews> GetCarsModel(int CarClassId)
        {
            List<CarModelDomainViews> model = carModel.GetByCarClassId(CarClassId);
            return model;
        }

        [HttpGet]
        [Route("GetFeaturesModel")]
        public List<carSpecificationsDomain> GetFeaturesModel(int ModelId)
        {
            List<carSpecificationsDomain> model = cs.GetByModelId(ModelId);
            return model;
        }

        [HttpGet]
        [Route("GetColors")]
        public List<Colors> GetColors()
        {
            List<Colors> model = sell.GetApiColors();
            return model;
        }

        [HttpPost]
        [Route("SellCras")]
        public IHttpActionResult SellCras(SellCarsDomain model)
        {

            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }

            if (model.ImagePordect.Count <= 0)
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا لا يوجد صور", MessageEn = "Sorry, no pictures" });
            }

            if (model.ImagePordect == null)
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا لا يوجد صور", MessageEn = "Sorry, no pictures" });
            }

            List<string> images = new List<string>();

            foreach (var item in model.ImagePordect)
            {
                string ImgeUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(Save(item)), false);
                images.Add(ImgeUrl);
            }

            model.ImagePordect = images;
            string userId = User.Identity.GetUserId();
            model.UserId = userId;
            int res = 0;/*=sell.AddCars(model);*/
            if (res > 0)
            {
                return Ok(new Response { Status = "success", MessageAr = "تم أضافة المركبة بنجاح", MessageEn = "Vehicle has been added successfully " });
            }
            else
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا حاول مراة اخري", MessageEn = "Sorry, try again" });
            }

          

        }




        [HttpGet]
        [Route("GetAccessoriesCategories")]
        public List<CategoryDomain> GetAccessoriesCategories()
        {
            List<CategoryDomain> model = category.GetByIds(4, 5);
            return model;
        }

        [HttpGet]
        [Route("GetMainAccessoriesType")]
        public List<MainAccessories> GetMainAccessoriesType()
        {
            List<MainAccessories> model = MAccessory.GetAPIMainAccessories();
            return model;
        }


        [HttpGet]
        [Route("GetSubAccessoriesType")]
        public List<SubAccessories> GetSubAccessoriesType(int MainAccessoriesType)
        {
            List<SubAccessories> model = subs.GetAPISubAccessories(MainAccessoriesType);
            return model;
        }


        [HttpPost]
        [Route("Sellaccessory")]
        public IHttpActionResult Sellaccessory(SellAccessoryDomain model)
        {

            if (model.ImagePordect.Count <= 0)
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا لا يوجد صور", MessageEn = "Sorry, no pictures" });
            }

            if (model.ImagePordect == null)
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا لا يوجد صور", MessageEn = "Sorry, no pictures" });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.ImagePordect == null)
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا لا يوجد صور", MessageEn = "Sorry, no pictures" });
            }

            List<string> images = new List<string>();

            foreach (var item in model.ImagePordect)
            {
                string ImgeUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(Save(item)), false);
                images.Add(ImgeUrl);
            }

            model.ImagePordect = images;
            string userId = User.Identity.GetUserId();
            model.UserId = userId;
            int res = sell.AddAccessory(model);
            if (res > 0)
            {
                return Ok(new Response { Status = "success", MessageAr = "تم أضافة المركبة بنجاح", MessageEn = "Vehicle has been added successfully " });
            }
            else
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا حاول مراة اخري", MessageEn = "Sorry, try again" });
            }



        }

        private string Save(string Image)
        {
            string SaveFullPath;
            var crop = Regex.Replace(Image, @"^data:.+;base64,", string.Empty);
            byte[] imageLength = Convert.FromBase64String(crop);
            string ReName = "alwakeelonline" + DateTime.Now.Ticks.ToString() + ".jpg";
            var folderName = "/Porducts/Images/" + ReName;
            SaveFullPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Porducts/Images"), ReName);
            File.WriteAllBytes(SaveFullPath, imageLength);
            return folderName;
        }
    }
}
