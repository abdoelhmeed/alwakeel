﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/RentCar")]
    public class RentCarController : ApiController
    {
        RentsCars rentsCars = new RentsCars();
        OrderRentCar Order = new OrderRentCar();
        [HttpGet]
        [Route("ShearhCars")]
        public List<RentCarViweApiDomain> ShearhCars([FromUri] SearchRentsCars model)
        {

            List<RentCarViweApiDomain> CarsList = rentsCars.GetAPIShearhCars(model);
            return CarsList;
        }



        [HttpGet]
        [Route("GetRentCars")]
        public List<RentCarViweApiDomain> GetRentCars([FromUri]SkipTakeDomain model)
        {
            if (model == null)
            {
                model.Skep = 0;
                model.Take = 20;
            }
            
            List<RentCarViweApiDomain> CarsList = rentsCars.GetRentAPICars(model);
            return CarsList;
        }

        [HttpPost]
        [Route("OrderCars")]
        public IHttpActionResult OrderCars(OrdesDmonain model)
        {
            if (model.Phone.Length < 9)
            {
                Respons respons = new Respons { MssAR = "9xxxxxxxx"+"عفوا رقم الهاتف غير صحيح", MssEn = "Sorry, the phone number is incorrect. 9xxxxxxxx", Status = "success" };
                return Ok(respons);
            }
            if (ModelState.IsValid)
            {
                // Do something with the product (not shown).
                int res = Order.NewOrder(model);

                Respons respons = new Respons { MssAR = "تم راسال الطلب", MssEn = "The request is complete", Id = res, Status = "success" };
              
                return Ok(respons);
            }
            else
            {
                return Ok(ModelState);
            }
           
        
        }



        private string Save(string Image)
        {
            string SaveFullPath;
            var crop = Regex.Replace(Image, @"^data:.+;base64,", string.Empty);
            byte[] imageLength = Convert.FromBase64String(crop);
            string ReName = "alwakeelonline" + DateTime.Now.Ticks.ToString() + ".jpg";
            var folderName = "/Porducts/Images/" + ReName;
            SaveFullPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Porducts/Images"), ReName);
            File.WriteAllBytes(SaveFullPath, imageLength);
            return folderName;
        }

    }
}
