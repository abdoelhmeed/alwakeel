﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using AlwakeelDomainLayer.ViewVehicleAndProdct;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/SearchVehicles")]
    public class SearchVehiclesController : ApiController
    {
        Category category = new Category();
        Brand brand = new Brand();
        CarClasses carClasses = new CarClasses();
        CarModel carModel = new CarModel();
        ProductVehicle Vehicle = new ProductVehicle();
      


        [Route("GetCategory")]
        [HttpGet]
        public IEnumerable<CategoryDomain> GetCategory()
        {
            IEnumerable<CategoryDomain> model = category.GetByIds(1, 3);
            return model;
        }


        [Route("GetBrand")]
        [HttpGet]
        public IEnumerable<BrandDomainViews> GetBrand()
        {
            List<BrandDomainViews> model = brand.Get().ToList();
            return model;
        }

        [Route("GetBrandByCategoryId")]
        [HttpGet]
        public IEnumerable<BrandDomainViews> GetBrandByCategoryId(int Id)
        {
            List<BrandDomainViews> model = brand.GetByCatId(Id);
            return model;
        }

        [Route("GetClass")]
        [HttpGet]
        public IEnumerable<CarClassDomainViews> GetClass()
        {
            List<CarClassDomainViews> model = carClasses.Get();
            return model;
        }


        [Route("GetClassByBrandId")]
        [HttpGet]
        public IEnumerable<CarClassDomainViews> GetClassByBrandId(int Id)
        {
            List<CarClassDomainViews> model = carClasses.GetByBrandId(Id);
            return model;
        }


        [Route("GetModel")]
        [HttpGet]
        public IEnumerable<CarModelDomainViews> GetModel()
        {
            List<CarModelDomainViews> model = carModel.Get();
            return model;
        }


        [Route("GetModelByClassId")]
        [HttpGet]
        public IEnumerable<CarModelDomainViews> GetModelByClassId(int Id)
        {
            List<CarModelDomainViews> model = carModel.GetByCarClassId(Id);
            return model;
        }


        [Route("GetVehiclesList")]
        [HttpGet]
        public IEnumerable<VehicleList> GetVehiclesList(int CatId)
        {
            if (CatId == 2 || CatId==1)
            {
                List<VehicleList> model = Vehicle.GetApiVehicleLists(CatId, 20);
                return model;
            }
            else
            {
                List<VehicleList> model = new List<VehicleList>();
                return model;
            }
         
        }

    
    }
}
