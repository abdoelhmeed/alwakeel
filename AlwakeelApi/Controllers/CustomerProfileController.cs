﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelBusinessLayer.Implementation.PordectsAPI;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/CustomerProfile")]
    public class CustomerProfileController : ApiController
    {

        Customerproducts my = new Customerproducts();
        Customerproducts customerproducts = new Customerproducts();
        RentsCars rentsCars = new RentsCars();
        OrderRentCar orders = new OrderRentCar();


        [HttpGet]
        [Route("MyProducts")]
        public List<CustomerproductsDomain> MyProducts()
        {
            string userId = User.Identity.GetUserId();

            List<CustomerproductsDomain> model= my.GetAPICustomerProducts(userId);

            return model;
        }

        [HttpGet]
        [Route("MyRentCars")]
        public List<RentCarViweApiDomain> MyRentCars()
        {
            string userId = User.Identity.GetUserId();

            List<RentCarViweApiDomain> model = rentsCars.GetRentCarsCustomerById(userId);

            return model;
        }


        [Authorize]
        [HttpPost]
        [Route("RentsCarAds")]
        public IHttpActionResult RentsCarAds(RentCarAPIDomain model)
        {
            string userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                string ImgeUrl = UplodImage.ResolveServerUrl(VirtualPathUtility.ToAbsolute(Save(model.Image)), false);
                model.Image = ImgeUrl;

                RentCarDomain rentCarDomain = new RentCarDomain()
                {
                    ModelId = model.ModelId,
                    ImageURl = model.Image,
                    Available = false,
                    CarClassId = model.CarClassId,
                    BrandId = model.BrandId,
                    CatId = model.CatId,
                    UserId = userId
                };
                rentsCars.AddCustomer(rentCarDomain);
                return Ok(new Response { Status = "success", MessageAr = "تم أضافة المركبة بنجاح", MessageEn = "Vehicle has been added successfully " });
            }
            else
            {
                return Ok(ModelState);
            }
        }

        [HttpPost]
        [Route("DeleteCarRent")]
        public IHttpActionResult DeleteCarRent(int RId) 
        {
            rentsCars.Delete(RId);
            return Ok(new Response { Status = "success", MessageAr = "تم الحزف المركبة بنجاح", MessageEn = "Delete successfully " });

        }

        [HttpGet]
        [Route("NewRentCarOrders")]
        public List<OrderRentCarDomain> NewRentCarOrders()
        {
            string userId = User.Identity.GetUserId();

            List<OrderRentCarDomain> model = orders.GetAPNewsOrders(userId);

            return model;
        }

        [HttpPost]
        [Route("SetSold")]
        public IHttpActionResult SetSold(int PId)
        {
            customerproducts.SetSold(PId);
            return Ok(new Response { Status = "success", MessageAr = "تم بنجاح", MessageEn = "successfully " });
        }


        [HttpPost]
        [Route("SetRentCarOrderDone")]
        public IHttpActionResult SetRentCarOrderDone(int Id)
        {
            orders.DoneOrder(Id);
            return Ok(new Response { Status = "success", MessageAr = "تم بنجاح", MessageEn = "successfully " });
        }



        private string Save(string Image)
        {
            string SaveFullPath;
            var crop = Regex.Replace(Image, @"^data:.+;base64,", string.Empty);
            byte[] imageLength = Convert.FromBase64String(crop);
            string ReName = "alwakeelonline" + DateTime.Now.Ticks.ToString() + ".jpg";
            var folderName = "/Porducts/Images/" + ReName;
            SaveFullPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Porducts/Images"), ReName);
            File.WriteAllBytes(SaveFullPath, imageLength);
            return folderName;
        }

    }
}
