﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/Filters")]
    public class FiltersController : ApiController
    {
        Category category = new Category();
        ACMains MAccessory = new ACMains();
        ACSubs subs = new ACSubs();
        Color colors = new Color();
        Brand brand = new Brand();
        CarClasses carClasses = new CarClasses();
        CarModel carModel = new CarModel();
        CSpecifications cs = new CSpecifications();




        [HttpGet]
        [Route("GetCarsCategories")]
        public List<CategoryDomain> GetCarsCategories()
        {
            List<CategoryDomain> model = category.GetByIds(1, 3);
            return model;
        }

        [HttpGet]
        [Route("GetAllCategories")]
        public List<CategoryDomain> GetAllCategories()
        {
            List<CategoryDomain> model = category.GetByAll();
            return model;
        }

        [HttpGet]
        [Route("GetBrand")]
        public List<BrandDomainViews> GetBrand(int CatId)
        {
            List<BrandDomainViews> model = brand.GetByCatId(CatId);
            return model;
        }

        [HttpGet]
        [Route("GetClass")]
        public List<CarClassDomainViews> GetClass(int BrandId)
        {
            List<CarClassDomainViews> model = carClasses.GetByBrandId(BrandId);
            return model;
        }

        [HttpGet]
        [Route("GetModel")]
        public List<CarModelDomainViews> GetModel(int CarClassId)
        {
            List<CarModelDomainViews> model = carModel.GetByCarClassId(CarClassId);
            return model;
        }


        [HttpGet]
        [Route("GetFeatures")]
        public List<FeaturesDomain> GetFeatures(int modelId)
        {
            List<FeaturesDomain> model = cs.GetAPIFeatures(modelId); ;
            return model;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Route("GetAccessoriesCategory")]
        public List<CategoryDomain> GetAccessoriesCategory()
        {
            List<CategoryDomain> model = category.GetByIds(4, 5);
            return model;
        }

        [HttpGet]
        [Route("GetMainAccessoriesType")]
        public List<MainAccessories> GetMainAccessoriesType()
        {
            List<MainAccessories> model = MAccessory.GetAPIMainAccessories();
            return model;
        }

        [HttpGet]
        [Route("GetSubAccessoriesType")]
        public List<SubAccessories> GetSubAccessoriesType(int MainAccessoriesType)
        {
            List<SubAccessories> model = subs.GetAPISubAccessories(MainAccessoriesType);
            return model;
        }

        [HttpGet]
        [Route("GetMainWithSubAccessory")]
        public List<ManiWithSubAccessories> GetMainWithSubAccessory()
        {
            List<ManiWithSubAccessories> model = MAccessory.GetManiWithSubAccessories();
            return model;
        }

        [HttpGet]
        [Route("GetColors")]
        public List<Colors> GetColors()
        {
            List<Colors> model = colors.GetApiColors();
            return model;
        }


    }
}
