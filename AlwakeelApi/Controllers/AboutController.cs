﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/AboutAlwakeel")]
    public class AboutController : ApiController
    {
        Business _business = new Business();
        [HttpGet]
        [Route("About")]
        public AboutAlwakeel About()
        {
            AboutAlwakeel model = _business.GetAbout();
            return model;
        }


        [HttpPost]
        [Route("Contact")]
        public IHttpActionResult Contact(AlwakeelContact model)
        {

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/MailTemplate/mail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Subject}", model.subject);
            body = body.Replace("{FullName}", model.FullName);
            body = body.Replace("{Email}", model.Email);
            body = body.Replace("{Phone}", model.Phone);
            body = body.Replace("{Title}", model.Title);
            bool IsSendEmail = AuthModel.SendEmail("Info@alwakeelonline.com", model.Title, body);
            return Ok(new Response { Status = "success", MessageAr = "نشكرك علي مراسلتنا", MessageEn = "Thank you for contacting us" });
        }
    }



}
