﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Auth")]
    public class AuthController : ApiController
    {
        OtpCode otp = new OtpCode();
        Random _random = new Random();
        ApplicationDbContext db = new ApplicationDbContext();

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }

            if (model.LoginType=="" || model.LoginType==null)
                return Ok(new Response { Status = "Error", MessageAr = "عفوا حدد نوع الدخول", MessageEn = "Sorry, select the type of entry" });

            string email = "";
            string phone = "";
            string Password = "Abdo@0489";
            if (model.LoginType == "e")
            {


                var Email = _userManager.FindByEmail(model.Email);
                if (Email != null)
                    return Ok(new Response { Status = "Error", MessageAr = "عفوا البريد الالكتروني موجود مسبقا", MessageEn = "Sorry, email already exists" });


               string uEmail= otp.FindEmail(model.Email);
                if (uEmail != null)
                    return Ok(new Response { Status = "Error", MessageAr = "عفوا البريد الالكتروني موجود مسبقا", MessageEn = "Sorry, email already exists" });



                email = model.Email;
                phone = _random.Next(100000, 123456789).ToString();
            }

            if (model.LoginType == "p")
            {

                if (model.PhoneNumber.Length != 9)
                    return Ok(new Response { Status = "Error", MessageAr = "عفوا ادخل رقم الهاتف بصورة صحيحة 9xxxxxxxx او 1xxxxxxxx", MessageEn = "Sorry, enter the correct phone number 9xxxxxxxx or 1xxxxxxxx" });


                if (otp.IsAlreadyExists(model.PhoneNumber)) 
                {
                    string c = _random.Next(1000, 100000).ToString();
                    string otpCode1= otp.UpdateCode(model.PhoneNumber, c);
                    string mss1 = "Your Verification number is " + otpCode1;
                    string sendmss = await AuthModel.SendCode(model.PhoneNumber, mss1);
                    return Ok(new Response { Status = "success", MessageAr = "تم ارسال كود التفعيل الي الهاتف", MessageEn = "The activation code has been sent to the phone" });
               
                }
                else
                {
                    phone = model.PhoneNumber;
                    email = "Alwakeel" + _random.Next(1, 100000000) + "@no.com";
                }
                   
              
            }
            string UserName = "alwakeel" + _random.Next(1, 100000000);

            var user = new ApplicationUser() { UserName = UserName, Email = email, PhoneNumber = phone };
            IdentityResult result = await _userManager.CreateAsync(user, Password);
            if (!result.Succeeded)
            {
                return Ok(new Response { Status = "Error", MessageAr = ".............................", MessageEn = ".............." });
            }
            UserLoginDomain log = new UserLoginDomain()
            {
                FullName = model.FullName,
                Id = user.Id,
                UCode = _random.Next(1000, 100000).ToString(),
                UEmail = email,
                UPhone = phone,
                Utype = model.LoginType
            };
            string otpCode = otp.Add(log);
            string mss = "Your Verification number is " + otpCode;
            if (model.LoginType == "p")
            {
                string sendmss = await AuthModel.SendCode(model.PhoneNumber, mss);
                return Ok(new Response { Status = "success", MessageAr = "تم ارسال كود التفعيل الي الهاتف", MessageEn = "The activation code has been sent to the phone" });
            }
            else
            {
                string body = string.Empty;
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/MailTemplate/AccountConfirmation.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{code}", otpCode);
                body = body.Replace("{UserName}", model.FullName);
                bool IsSendEmail = AuthModel.SendEmail(model.Email,"Account Confirmation", body);
                return Ok(new Response { Status = "success", MessageAr = "تم التسجيل بنجاح فعل الحساب من البريد الالكتروني", MessageEn = "The Registration was successful. Verify your account from  mail" });
            }

          
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("Verification")]
        public IHttpActionResult Verification([FromBody] VerificationModel model)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
         
            if (model.Email != null || !String.IsNullOrEmpty(model.Email))
            {

                string UserId = otp.FindByCodeEmail(model.code, model.Email);
                if (UserId !=null)
                {
                    var user = _userManager.FindById(UserId);
                    dynamic token = AuthModel.GenerateToken(user);
                    return Ok(new { Status = "success", token = token });
                }
              
            }
            else
            {
                if (model.PhoneNumber.Length != 9)
                    return Ok(new Response { Status = "Error", MessageAr = "عفوا ادخل رقم الهاتف بصورة صحيحة 9xxxxxxxx او 1xxxxxxxx", MessageEn = "Sorry, enter the correct phone number 9xxxxxxxx or 1xxxxxxxx" });

                string UserId = otp.FindByCodePhoneNumber(model.code, model.PhoneNumber);
                if (UserId != null)
                {
                    var user = _userManager.FindById(UserId);
                    dynamic token = AuthModel.GenerateToken(user);
                    return Ok(new { Status = "success", token = token });
                }

            }

            return Ok(new Response { Status = "Failed", MessageAr = "عفوا بيانات الدخول غير صحيحة", MessageEn = "Sorry, the login data is incorrect" });
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public async Task<IHttpActionResult> LoginAsync(LogiModel model)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }
            if (model.Email != null || model.Email == "" )
            {
                

                var user = _userManager.FindByEmail(model.Email);
                if (user != null)
                {
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/MailTemplate/AccountConfirmation.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    string otpCode = otp.FindByEmail(model.Email);
                    if (otpCode != "no")
                    {
                        string mss = "Your Verification number is " + otpCode;
                        body = body.Replace("{code}", otpCode);
                        body = body.Replace("{UserName}", model.Email);
                        bool IsSendEmail = AuthModel.SendEmail(model.Email, "Account Confirmation", body);
                        return Ok(new Response { Status = "success", MessageAr = "تم التسجيل بنجاح فعل الحساب من البريد الالكتروني", MessageEn = "The Registration was successful. Verify your account from  mail" });

                    }
                }
              
            }
            else
            {
                if (model.PhoneNumber.Length != 9)
                    return Ok(new Response { Status = "Error", MessageAr = "عفوا ادخل رقم الهاتف بصورة صحيحة 9xxxxxxxx او 1xxxxxxxx", MessageEn = "Sorry, enter the correct phone number 9xxxxxxxx or 1xxxxxxxx" });


                
                string otpCode = otp.FindByPhoneNumber(model.PhoneNumber);
                if (otpCode != "no")
                {
                    string mss1 = "Your Verification number is " + otpCode;
                    string sendmss = await AuthModel.SendCode(model.PhoneNumber, mss1);
                    return Ok(new Response { Status = "success", MessageAr = "تم ارسال كود التفعيل الي الهاتف", MessageEn = "The activation code has been sent to the phone" });
                }
            }

            return Ok(new Response { Status = "Failed", MessageAr = "عفوا  أسم المستخدم  غير صحيحة", MessageEn = "Sorry, the  username is incorrect" });

        }
       
    }
}
