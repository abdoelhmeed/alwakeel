﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelBusinessLayer.Implementation.PordectsAPI;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController
    {
      
        Pordect pordect = new Pordect();

        [HttpGet]
        [Route("GetPordectList")]
        public List<PordectList> GetPordectList([FromUri]Filter filter)
        {
            List<PordectList> model = pordect.pordectLists(filter);
            return model;
        }

        [HttpGet]
        [Route("GetPorduct")]
        public PorductDetailModel GetPorduct(int PId)
        {
            PorductDetailModel model = pordect.PorductDetail(PId);
            return model;
        }


        [HttpGet]
        [Route("Setlove")]
        public string Setlove(int PId)
        {
            int res = pordect.Setlove(PId);
            if (res > 0)
            {
                return "Loved";
            }
            else
            {
                return "not Loved";
            }
        }
    }
}
