﻿using AlwakeelApi.Models;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{

    [RoutePrefix("api/AgentCompany")]
    public class AgentCompanyController : ApiController
    {

        Business business = new Business();
        ContactMedia CM = new ContactMedia();
        BusinessProduct BP = new BusinessProduct();
        BusinessBrand Bbrand = new BusinessBrand();
        BSliderShow sliderShow = new BSliderShow();
        BayNow bay = new BayNow();
        Random _random = new Random();

        [HttpGet]
        [Route("GetCompanyById")]
        public CompanyDetails GetCompanyDetails(int businessId)
        {
            CompanyDetails model=business.CompanyDetail(businessId);
            return model;
        }


        [HttpPost]
        [Route("NewBayNow")]
        public IHttpActionResult NewBayNow(BayNowDomain model) 
        {
            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }
            bay.Add(model);
            return Ok(new Response { Status = "success", MessageAr = "تم أضافة الطلب بنجاح", MessageEn = "The request has been added successfully" });

        }

        [HttpGet]
        [Route("GetInsuranceById")]
        public Agents GetInsuranceById(int businessId)
        {
            BusinessDomain bu = business.GetById(businessId);
            List<BusinessProductDomain> bup = BP.GetByBusinessId(businessId);
            List<AgentProduct> agentProduct = bup.Select(x => new AgentProduct
            {
                id = x.id,
                PimageUrl = x.PimageUrl,
                PNameAR = x.PNameAR,
                PNameEN = x.PNameEN,
                Price = x.Price,
                PType = x.ProductType
            }).ToList();
            List<ContactMediaDomain> cm = CM.GetBusinessId(businessId);
            List<Contact> ContactMedia = cm.Select(x => new Contact
            {
                contact = x.contact,
                Id = x.Id,
                Type = x.Type
            }).ToList();
            Agents agent = new Agents()
            {
                AgentType = bu.AgentType,
                BusinessAboutAR = bu.BusinessAboutAR,
                BusinessAboutEn = bu.BusinessAboutEn,
                BusinessId = bu.BusinessId,
                BusinessLogo = bu.BusinessLogo,
                BusinessNameAR = bu.BusinessNameAR,
                BusinessNameEn = bu.BusinessNameEn,
                BusinessPanelImage = sliderShow.GetImageListByBId(businessId),
                Product = agentProduct,
                Contact = ContactMedia

            };
            return agent;
        }

        [HttpGet]
        [Route("GetAgentOil")]
        public List<companyModel> GetAgentOil()
        {
            int OrderNumber = _random.Next(1,6);

            List<companyModel> model = business.GetAgentByProduct("Oil");

            if (OrderNumber == 1)
            {
                return model.OrderByDescending(x => x.BusinessId).ToList();
            }

            if (OrderNumber == 2)
            {
                return model.OrderByDescending(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 3)
            {
                return model.OrderByDescending(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 4)
            {
                return model.OrderBy(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 5)
            {
                return model.OrderBy(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 6)
            {
                return model.OrderBy(x => x.BusinessId).ToList();
            }

            return model;
        }

        [HttpGet]
        [Route("GetAgentBattery")]
        public List<companyModel> GetAgentBattery()
        {
            int OrderNumber = _random.Next(1, 6);

            List<companyModel> model = business.GetAgentByProduct("Battery");
            if (OrderNumber == 1)
            {
                return model.OrderByDescending(x => x.BusinessId).ToList();
            }

            if (OrderNumber == 2)
            {
                return model.OrderByDescending(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 3)
            {
                return model.OrderByDescending(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 4)
            {
                return model.OrderBy(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 5)
            {
                return model.OrderBy(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 6)
            {
                return model.OrderBy(x => x.BusinessId).ToList();
            }
            return model;
        }

        [HttpGet]
        [Route("GetAgentAccessories")]
        public List<companyModel> GetAgentAccessories()
        {
            int OrderNumber = _random.Next(1, 6);
            List<companyModel> model = business.GetAgentByProduct("Accessories");
            if (OrderNumber == 1)
            {
                return model.OrderByDescending(x => x.BusinessId).ToList();
            }

            if (OrderNumber == 2)
            {
                return model.OrderByDescending(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 3)
            {
                return model.OrderByDescending(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 4)
            {
                return model.OrderBy(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 5)
            {
                return model.OrderBy(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 6)
            {
                return model.OrderBy(x => x.BusinessId).ToList();
            }
            return model;

        }

        [HttpGet]
        [Route("GetAgentSpareparts")]
        public List<companyModel> GetAgentSpareparts()
        {
            int OrderNumber = _random.Next(1, 6);
            List<companyModel> model = business.GetAgentByProduct("Spareparts");
            if (OrderNumber == 1)
            {
                return model.OrderByDescending(x => x.BusinessId).ToList();
            }

            if (OrderNumber == 2)
            {
                return model.OrderByDescending(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 3)
            {
                return model.OrderByDescending(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 4)
            {
                return model.OrderBy(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 5)
            {
                return model.OrderBy(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 6)
            {
                return model.OrderBy(x => x.BusinessId).ToList();
            }
            return model;

        }

        [HttpGet]
        [Route("GetAgentTires")]
        public List<companyModel> GetAgentTires()
        {
            int OrderNumber = _random.Next(1, 6);
            List<companyModel> model = business.GetAgentByProduct("Tires");

            if (OrderNumber == 1)
            {
                return model.OrderByDescending(x => x.BusinessId).ToList();
            }

            if (OrderNumber == 2)
            {
                return model.OrderByDescending(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 3)
            {
                return model.OrderByDescending(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 4)
            {
                return model.OrderBy(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 5)
            {
                return model.OrderBy(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 6)
            {
                return model.OrderBy(x => x.BusinessId).ToList();
            }
            return model;
        }

        [HttpGet]
        [Route("GetInsurance")]
        public List<companyModel> GetInsurance()
        {
            int OrderNumber = _random.Next(1, 6);
            List<companyModel> modelList = new List<companyModel>();
            List<BusinessDomain> Insur = business.Get();

            foreach (var item in Insur)
            {
                if (item.BusinessType.Contains("Insurance"))
                {
                    companyModel model = new companyModel()
                    {
                        BusinessId = item.BusinessId,
                        BusinessLogo = item.BusinessLogo,
                        BusinessNameAR = item.BusinessNameAR,
                        BusinessNameEn = item.BusinessNameEn,
                        BusinessAboutAR = item.BusinessAboutAR,
                        BusinessAboutEn = item.BusinessAboutEn
                    };

                    modelList.Add(model);
                }

            }

            if (OrderNumber == 1)
            {
                return modelList.OrderByDescending(x => x.BusinessId).ToList();
            }

            if (OrderNumber == 2)
            {
                return modelList.OrderByDescending(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 3)
            {
                return modelList.OrderByDescending(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 4)
            {
                return modelList.OrderBy(x => x.BusinessNameAR).ToList();
            }

            if (OrderNumber == 5)
            {
                return modelList.OrderBy(x => x.BusinessNameEn).ToList();
            }


            if (OrderNumber == 6)
            {
                return modelList.OrderBy(x => x.BusinessId).ToList();
            }

            return modelList;
        }

        [HttpGet]
        [Route("GetRentCarCompanys")]
        public List<companyModel> GetRentCarCompanys()
        {
           
            List<companyModel> model = business.GetRentCarCompanys("Rent Car");

            
            return model;
        }
    }
}
