﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/Blog")]
    public class BlogPageController : ApiController
    {
        Blog blog = new Blog();

        [HttpGet]
        [Route("GetBlog")]
        public List<BlogDomain> GetBlog()
        {
            List<BlogDomain> model = blog.Get().OrderByDescending(x => x.blogId).ToList();
            return model;
        }

        [HttpGet]
        [Route("GetBlog")]
        public BlogDomain GetByBlogId(int Id)
        {
            BlogDomain model = blog.ViewByBlpgById(Id);
            return model;
        }
    }
}
