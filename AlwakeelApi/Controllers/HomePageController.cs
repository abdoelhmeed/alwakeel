﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Implementation;
using AlwakeelApi.Models;
using AlwakeelDomainLayer.Accessory;
using AlwakeelDomainLayer.APIView;
using AlwakeelBusinessLayer.Implementation.PordectsAPI;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/HomePage")]
    public class HomePageController : ApiController
    {
        CommercialAds commercialAds = new CommercialAds();
        Business business = new Business();
        VideoYouTube videoYouTube = new VideoYouTube();
        Brand brand = new Brand();
        Blog blog = new Blog();
        BusinessBrand Bbrand = new BusinessBrand();
        Category category = new Category();
        ProductVehicle productVehicle = new ProductVehicle();
        ProductImage productImage = new ProductImage();
        Accessory accessory = new Accessory();
        Pordect pordect = new Pordect();
        Random _random = new Random();
        DealOfday DealOfday = new DealOfday();




        [HttpGet]
        [Route("GetSlider")]
        public List<CommercialAdsDomain> GetSlider()
        {
            List<CommercialAdsDomain> model = commercialAds.GetByType("Slider Image").OrderByDescending(x=> x.Id).Take(6).ToList();
            return model;
        }

        [HttpGet]
        [Route("GetAdsImage")]
        public List<CommercialAdsDomain> GetAdsImage()
        {
            List<CommercialAdsDomain> model = commercialAds.GetByType("Panle Image");
            return model;
        }

        [HttpGet]
        [Route("GetHeaderAds")]
        public List<AdsImages> GetHeaderAds()
        {
            int I = 0;
            
            List<AdsOrderImages> modellist = new List<AdsOrderImages>();
            List<AdsImages> model = commercialAds.GetByType("header Ads").OrderByDescending(u => u.Id).Select(x => new AdsImages
            {
                Image = x.adsImage,
                Link = x.UrlAds
            }).ToList();
        


            foreach (var item in model)
            {
                I = I + 1;
                AdsOrderImages adsOrderImages = new AdsOrderImages()
                {
                    Image = item.Image,
                    Link = item.Link,
                    Number = I
                };
                modellist.Add(adsOrderImages);

            }

            if (modellist.Count > 0)
            {
                int num = _random.Next(1, modellist.Count+1);
                List<AdsImages> rmodel = modellist.Where(x => x.Number == num)
                    .Select(e=> new AdsImages {
                        Image = e.Image,
                        Link = e.Link

                    }).ToList();

                return rmodel.Take(1).ToList();
            }



            return model.Take(1).ToList();

        }

        [HttpGet]
        [Route("GetMiddleAds")]
        public List<AdsImages> GetMiddleAds()
        {
            List<AdsImages> model = commercialAds.GetByType("Middle Ads").OrderByDescending(u=> u.Id).Select(x=> new AdsImages
            { 
            Image=x.adsImage,
            Link=x.UrlAds
            }).Take(2).ToList();
            return model;
        }

        [HttpGet]
        [Route("GetFooterAds")]
        public List<AdsImages> GetFooterAds()
        {
            List<AdsImages> model = commercialAds.GetByType("footer ads").OrderByDescending(u => u.Id).Select(x => new AdsImages
            {
                Image = x.adsImage,
                Link = x.UrlAds
            }).Take(2).ToList();
            return model;
           
        }


        [HttpGet]
        [Route("GetYouTubeVideos")]
        public List<VideoDomain> GetYouTubeVideos()
        {
            List<VideoDomain> model = videoYouTube.Get().OrderByDescending(x => x.VideoDate).ToList();
            return model.Take(5).ToList();
        }

        [HttpGet]
        [Route("GetBlog")]
        public List<BlogDomain> GetBlog()
        {
            List<BlogDomain> model = blog.Get().OrderByDescending(x => x.blogDate).Take(5).ToList();
            return model;
        }

        [HttpGet]
        [Route("GetCompaniesByCatId")]
        public List<CompanyDetails> GetCompaniesByCatId(int CatId)
        {
            List<CompanyDetails> model = business.GetAPICompanies(CatId);

            return model;
           
        }


        [HttpGet]
        [Route("GetCertificates")]
        public List<VehicleHomePage> GetCertificates()
        {

            List<VehicleHomePage> model = productVehicle.GetCertificates();
           
            return model;
        }

        [HttpGet]
        [Route("GetFeatured")]
        public List<VehicleHomePage> GetFeatured(int CatId)
        {
            List<VehicleHomePage> model= productVehicle.GetAPIFeatured(CatId);
            return model;
        }


        [HttpGet]
        [Route("GetDealOfday")]
        public List<DealOfdayViewDomain> GetDealOfday()
        {
            List<DealOfdayViewDomain> model =DealOfday.GetToApi();
            return model;
        }


        [HttpGet]
        [Route("GetAccessory")]
        public List<AccessoryAutoPartList> GetAccessory(int CatId)
        {
            List<AccessoryAutoPartList> model = new List<AccessoryAutoPartList>();
            if (CatId==4 || CatId==5)
            {
                Accessory accessory = new Accessory();
                model = accessory.GetAPIAccessory(CatId, 20).ToList();
            }
            return model;
        }

        [HttpGet]
        [Route("Setlove")]
        public IHttpActionResult Setlove(int Id)
        {
           int res= pordect.Setlove(Id);
            if (res > 0)
            {
                return Ok(new Response { Status = "success", MessageAr = "تم  بنجاح", MessageEn = "successfully " });
            }
            else
            {
                return Ok(new Response { Status = "Error", MessageAr = "عفوا حاول مراة اخري", MessageEn = "Sorry, try again" });
            }
        }

    }
}
