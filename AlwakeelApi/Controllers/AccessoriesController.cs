﻿using AlwakeelBusinessLayer.Implementation;
using AlwakeelDomainLayer.Accessory;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlwakeelApi.Controllers
{
    [RoutePrefix("api/Accessories")]
    public class AccessoriesController : ApiController
    {
        Accessory accessory = new Accessory();

        [HttpGet]
        [Route("GetAccessories")]
        public List<AccessoryAutoPartList> GetAccessories(int CatId,int Take)
        {
            List<AccessoryAutoPartList> model = accessory.GetAPIAccessory(CatId,Take);
            return model;
        }
    }
}
