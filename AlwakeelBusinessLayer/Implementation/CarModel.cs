﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
   public class CarModel : Base, ICarsModel
    {
        CSpecifications cSpecifications = new CSpecifications();
        public int Add(CarModelDomain model)
        {
            td_CarModel cm = new td_CarModel()
            {
                CarClassId = model.CarClassId,
                ModelYears = model.ModelYears
            };

            db.td_CarModel.Add(cm);
            int res = db.SaveChanges();
            if (res > 0)
            {
                foreach (var item in model.GeneralSpec)
                {
                    AllModelSpecification specification = db.AllModelSpecification.Find(item);
                    carSpecificationsDomain NewSpec = new carSpecificationsDomain();
                    NewSpec.BrandId = model.BrandId;
                    NewSpec.CarClassId = model.CarClassId;
                    NewSpec.ModelId = cm.ModelId;
                    NewSpec.SpeAr = specification.NameAR;
                    NewSpec.SpeEn = specification.NameEn;
                    NewSpec.SpeIcon = specification.Icons;
                    
                   
                    cSpecifications.Add(NewSpec);
                }
             
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            try
            {
                int res = 0;
                td_CarModel model = db.td_CarModel.Find(Id);
                if (model != null)
                {
                    db.Entry(model).State = EntityState.Deleted;
                    res = db.SaveChanges();
                    if (res > 0)
                    {
                        res = 1;
                    }
                    else
                    {
                        res = -1;
                    }
                }

                return res;
            }
            catch (Exception)
            {

                return -1;
            }
        }

        public List<CarModelDomainViews> Get()
        {
            List<CarModelDomainViews> model = db.td_CarModel.Select(x => new CarModelDomainViews
            {
                BrandId = (int)x.td_CarClass.BrandId,
                BrandNameAr = x.td_CarClass.td_Brand.BrandNameAr,
                 BrandNameEn = x.td_CarClass.td_Brand.BrandNameEn,
                 CarClassId=x.CarClassId,
                 CarClassNameAr=x.td_CarClass.CarClassNameAr,
                 CarClassNameEn=x.td_CarClass.CarClassNameEn,
                 CategoryNameAr=x.td_CarClass.td_Brand.td_Category.NameAr,
                 CategoryNameEn= x.td_CarClass.td_Brand.td_Category.NameEn,
                 CatId=x.td_CarClass.td_Brand.td_Category.CatId,
                 ModelId=x.ModelId,
                 ModelYears=x.ModelYears

            }).ToList();
            return model;
        }

        public List<CarModelDomainViews> GetByCarClassId(int CarClassId)
        {
            List<CarModelDomainViews> model = db.td_CarModel.Where(u=> u.CarClassId==CarClassId).Select(x => new CarModelDomainViews
            {
                BrandId = (int)x.td_CarClass.BrandId,
                BrandNameAr = x.td_CarClass.td_Brand.BrandNameAr,
                BrandNameEn = x.td_CarClass.td_Brand.BrandNameEn,
                CarClassId = x.CarClassId,
                CarClassNameAr = x.td_CarClass.CarClassNameAr,
                CarClassNameEn = x.td_CarClass.CarClassNameEn,
                CategoryNameAr = x.td_CarClass.td_Brand.td_Category.NameAr,
                CategoryNameEn = x.td_CarClass.td_Brand.td_Category.NameEn,
                CatId = x.td_CarClass.td_Brand.td_Category.CatId,
                ModelId = x.ModelId,
                ModelYears = x.ModelYears

            }).ToList();
            return model;
        }

        public CarModelDomain GetById(int Id)
        {
            td_CarModel x = db.td_CarModel.Find(Id);
            CarModelDomain model = new CarModelDomain()
            {
                BrandId=(int)x.td_CarClass.BrandId,
                CarClassId = (int)x.CarClassId,
                CatId = (int)x.td_CarClass.td_Brand.CatId,
                ModelId = x.ModelId,
                ModelYears=x.ModelYears

            };
            return model;
        }

        public int Update(CarModelDomain model)
        {

            td_CarModel cm = db.td_CarModel.Find(model.ModelId);

            if (cm !=null)
            {
                cm.CarClassId = model.CarClassId;
                cm.ModelYears = model.ModelYears;
                db.Entry(cm).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        


        }

        public bool ishasModel(int Years, int CarClass) 
        {
            List<td_CarModel> cars = db.td_CarModel.Where(x => x.ModelYears == Years && x.td_CarClass.CarClassId == CarClass).ToList();
            if (cars.Count <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
