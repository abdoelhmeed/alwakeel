﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Business : Base, IBusiness
    {
        Random _random = new Random();
        public int Activeion(int BId)
        {
            td_Business business = db.td_Business.Find(BId);
            if (business != null)
            {
                if (business.active == null)
                {
                    business.active = true;
                    db.Entry(business).State = EntityState.Modified;
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    business.active = !business.active;
                    db.Entry(business).State = EntityState.Modified;
                    db.SaveChanges();
                    return 1;
                }
            }
            else
            {
                return 0;
            }
        }

        public int AddReturnId(BusinessDomain model)
        {
            td_Business business = new td_Business()
            {
                BusinessAboutAR = model.BusinessAboutAR,
                BusinessAboutEn = model.BusinessAboutEn,
                BusinessLogo = model.BusinessLogo,
                BusinessNameAR = model.BusinessNameAR,
                BusinessNameEn = model.BusinessNameEn,
                BusinessPanelImage = model.BusinessPanelImage,
                BusinessRegistrationRate = model.BusinessRegistrationRate,
                BusinessType = model.BusinessType,
                AgentType = model.AgentType,
                BusinessLocationAr = model.BusinessLocationAr,
                BusinessLocationEn = model.BusinessLocationEn,
                BusinessSpecial = model.BusinessSpecial,
                active = true

            };

            db.td_Business.Add(business);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return business.BusinessId;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            try
            {
                td_Business model = db.td_Business.Find(Id);
                if (model != null)
                {
                    db.Entry(model).State = EntityState.Deleted;
                    int res = db.SaveChanges();
                    if (res > 0)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }

                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {

                return -1;
            }
        }

        public List<BusinessDomain> Get()
        {
            List<BusinessDomain> model = db.td_Business.Select(x => new BusinessDomain
            {
                BusinessId = x.BusinessId,
                BusinessAboutAR = x.BusinessAboutAR,
                BusinessAboutEn = x.BusinessAboutEn,
                BusinessLogo = x.BusinessLogo,
                BusinessNameAR = x.BusinessNameAR,
                BusinessNameEn = x.BusinessNameEn,
                BusinessPanelImage = x.BusinessPanelImage,
                BusinessRegistrationRate = x.BusinessRegistrationRate,
                BusinessType = x.BusinessType,
                AgentType = x.AgentType,
                BusinessLocationAr = x.BusinessLocationAr,
                BusinessLocationEn = x.BusinessLocationEn,
                BusinessSpecial = x.BusinessSpecial,
                active = true

            }).ToList();

            return model;
        }

        public BusinessDomain GetById(int Id)
        {
            td_Business x = db.td_Business.Find(Id);
            if (x != null)
            {
                BusinessDomain model = new BusinessDomain()
                {
                    BusinessId = x.BusinessId,
                    BusinessAboutAR = x.BusinessAboutAR,
                    BusinessAboutEn = x.BusinessAboutEn,
                    BusinessLogo = x.BusinessLogo,
                    BusinessNameAR = x.BusinessNameAR,
                    BusinessNameEn = x.BusinessNameEn,
                    BusinessPanelImage = x.BusinessPanelImage,
                    BusinessRegistrationRate = x.BusinessRegistrationRate,
                    BusinessType = x.BusinessType,
                    AgentType = x.AgentType,
                    BusinessLocationAr = x.BusinessLocationAr,
                    BusinessLocationEn = x.BusinessLocationEn,
                    BusinessSpecial = x.BusinessSpecial,
                    active = true
                };
                return model;
            }
            else
            {
                BusinessDomain model = new BusinessDomain();
                return model;
            }


        }

        public int Update(BusinessDomain model)
        {

            td_Business business = db.td_Business.Find(model.BusinessId);
            business.BusinessAboutAR = model.BusinessAboutAR;
            business.BusinessAboutEn = model.BusinessAboutEn;
            business.BusinessLogo = model.BusinessLogo;
            business.BusinessNameAR = model.BusinessNameAR;
            business.BusinessNameEn = model.BusinessNameEn;
            business.BusinessPanelImage = model.BusinessPanelImage;
            business.BusinessRegistrationRate = model.BusinessRegistrationRate;
            business.BusinessType = model.BusinessType;
            business.AgentType = model.AgentType;
            business.BusinessLocationAr = model.BusinessLocationAr;
            business.BusinessLocationEn = model.BusinessLocationEn;
            business.BusinessSpecial = model.BusinessSpecial;


            db.Entry(business).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        List<BusinessDomain> IBusiness.GetType(string Type)
        {
            List<BusinessDomain> model = db.td_Business.Where(u => u.BusinessType == Type).Select(x => new BusinessDomain
            {
                BusinessAboutAR = x.BusinessAboutAR,
                BusinessAboutEn = x.BusinessAboutEn,
                BusinessLogo = x.BusinessLogo,
                BusinessNameAR = x.BusinessNameAR,
                BusinessNameEn = x.BusinessNameEn,
                BusinessPanelImage = x.BusinessPanelImage,
                BusinessRegistrationRate = x.BusinessRegistrationRate,
                BusinessType = x.BusinessType,
                AgentType = x.AgentType,
                BusinessLocationAr = x.BusinessLocationAr,
                BusinessLocationEn = x.BusinessLocationEn,
                BusinessSpecial = x.BusinessSpecial,
                active = true

            }).ToList();

            return model;
        }

        public List<companyModel> GetAgentByProduct(string ProType)
        {
            List<companyModel> models = new List<companyModel>();
            List<td_Business> BList = db.td_Business.ToList();
            foreach (var item in BList)
            {
                if (item.AgentType != null)
                {
                    if (item.AgentType.Equals(ProType))
                    {
                        companyModel model = new companyModel()
                        {
                            BusinessId = item.BusinessId,
                            BusinessLogo = item.BusinessLogo,
                            BusinessNameAR = item.BusinessNameAR,
                            BusinessNameEn = item.BusinessNameEn,
                            BusinessAboutAR = item.BusinessAboutAR,
                            BusinessAboutEn = item.BusinessAboutEn
                        };

                        models.Add(model);
                    }
                }
                else
                {
                    var PModel = db.td_BusinessProduct.Where(x => x.BusinessId == item.BusinessId && x.ProductType.Contains(ProType)).ToList();
                    if (PModel.Count > 0)
                    {
                        companyModel model = new companyModel()
                        {
                            BusinessId = item.BusinessId,
                            BusinessLogo = item.BusinessLogo,
                            BusinessNameAR = item.BusinessNameAR,
                            BusinessNameEn = item.BusinessNameEn,
                            BusinessAboutAR = item.BusinessAboutAR,
                            BusinessAboutEn = item.BusinessAboutEn
                        };

                        models.Add(model);
                    }
                }
            }
            return models;
        }


        public List<companyModel> GetRentCarCompanys(string ProType)
        {
            List<companyModel> models = new List<companyModel>();
            List<td_Business> BList = db.td_Business.ToList();
            foreach (var item in BList)
            {
                if (item.BusinessType == ProType)
                {
                  
                        companyModel model = new companyModel()
                        {
                            BusinessId = item.BusinessId,
                            BusinessLogo = item.BusinessLogo,
                            BusinessNameAR = item.BusinessNameAR,
                            BusinessNameEn = item.BusinessNameEn,
                            BusinessAboutAR = item.BusinessAboutAR,
                            BusinessAboutEn = item.BusinessAboutEn
                        };

                        models.Add(model);
                   
                }
                else
                {
                    var PModel = db.tdRentCar.Where(x => x.BusinessId == item.BusinessId).ToList();
                    if (PModel.Count > 0)
                    {
                        companyModel model = new companyModel()
                        {
                            BusinessId = item.BusinessId,
                            BusinessLogo = item.BusinessLogo,
                            BusinessNameAR = item.BusinessNameAR,
                            BusinessNameEn = item.BusinessNameEn,
                            BusinessAboutAR = item.BusinessAboutAR,
                            BusinessAboutEn = item.BusinessAboutEn
                        };

                        models.Add(model);
                    }
                }
            }
            return models;
        }


        public CompanyDetails CompanyDetail(int BId)
        {
            td_Business _Business = db.td_Business.Find(BId);
            CompanyDetails model = new CompanyDetails()
            {
                BusinessAboutAR = _Business.BusinessAboutAR,
                BusinessAboutEn = _Business.BusinessAboutEn,
                BusinessId = _Business.BusinessId,
                BusinessLogo = _Business.BusinessLogo,
                BusinessNameAR = _Business.BusinessNameAR,
                BusinessNameEn = _Business.BusinessNameEn,
                BusinessLocationAr = _Business.BusinessLocationAr,
                BusinessLocationEn = _Business.BusinessLocationEn,
                BusinessType = _Business.BusinessType,
                BusinessPanelImage = _Business.td_BusinessSlider.Select(x => x.ImageUrl).Take(3).ToList(),
                companyWithBrand = _Business.td_BusinessBrands.Select(x => new CompanyWithBrand
                {
                    BrandsLogo = x.td_Brand.BrandsLogo,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    BrandId = x.BrandId
                }).ToList(),
                Contact = _Business.td_Contact_Media.Select(x => new Contact
                {
                    contact = x.contact,
                    Id = x.Id,
                    Type = x.Type,
                    Icons = GetIconClass(x.Type)

                }).ToList(),
                Products = _Business.td_BusinessProduct.Select(x => new AgentProduct
                {
                    id = x.id,
                    PimageUrl = x.PimageUrl2,
                    PNameAR = x.PNameAR,
                    PNameEN = x.PNameEN,
                    Price = x.Price,
                    PType = x.ProductType,
                    DescribeAR=x.DescribeAR,
                    DescribeEN=x.DescribeEN

                }).ToList()
            };

            return model;

        }

        public bool isHasCompany(BusinessDomain model) 
        {
            int Number = db.td_Business.Where(x => x.BusinessNameAR == model.BusinessNameAR && x.BusinessNameEn == model.BusinessNameEn).Count();
            if (Number > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        
        }

        public AboutAlwakeel GetAbout() 
        {
            td_Business _Business = db.td_Business.Find(1);

            AboutAlwakeel about = new AboutAlwakeel()
            {


                BusinessAboutAR = _Business.BusinessAboutAR,
                BusinessAboutEn = _Business.BusinessAboutEn,
                BusinessId = _Business.BusinessId,
                BusinessLogo = _Business.BusinessLogo,
                BusinessNameAR = _Business.BusinessNameAR,
                BusinessNameEn = _Business.BusinessNameEn,
                BusinessLocationAr = _Business.BusinessLocationAr,
                BusinessLocationEn = _Business.BusinessLocationEn,
                BusinessPanelImage=_Business.BusinessPanelImage,
                Contact = _Business.td_Contact_Media.Select(x => new Contact
                {
                    contact = x.contact,
                    Id = x.Id,
                    Type = x.Type,
                    Icons = GetIconClass(x.Type)

                }).ToList(),

            };
            return about;

        }
        public string GetIconClass(string type)
        {
            if (type == "facebook")
            {
                return "fa fa-facebook";
            }
            else
            if (type == "Phone Number")
            {
                return "fa fa-phone";
            }
            else
            if (type == "Whatsapp Number")
            {
                return "fa fa-whatsapp";
            }
            else
            if (type == "Twitter")
            {
                return "fa fa-twitter";
            }
            else
            if (type == "Web Site")
            {
                return "fa fa-website";
            }
            else
            {
                return "fa fa-website";
            }
        }

        /// <summary>
        /// API
        /// </summary>
        /// <param name="CatId"></param>
        /// <returns></returns>
        public List<CompanyDetails> GetAPICompanies(int CatId) 
        {
            int OrderNumber = _random.Next(1, 8);
            List<CompanyDetails> model = new List<CompanyDetails>();
            List<CompanyDetails> CompanyList = db.td_Business.Where(c => c.BusinessType.Contains("Company"))
                .Select(x => new CompanyDetails 
                {

                    BusinessId = x.BusinessId,
                    BusinessLogo = x.BusinessLogo,
                    BusinessNameAR = x.BusinessNameAR,
                    BusinessNameEn = x.BusinessNameEn,
                    companyWithBrand =x.td_BusinessBrands.Where(e=> e.td_Brand.CatId==CatId).OrderBy(o=> o.BrandId)
                    .Select(b=> new CompanyWithBrand 
                    {
                        BrandId = (int)b.BrandId,
                        BrandNameAr = b.td_Brand.BrandNameAr,
                        BrandNameEn = b.td_Brand.BrandNameEn,
                        BrandsLogo = b.td_Brand.BrandsLogo

                    }).Take(3).ToList()
                }).ToList();

            model = CompanyList.Where(x => x.companyWithBrand.Count > 0).ToList();

            if (OrderNumber == 1)
            {
                model.OrderBy(o=> o.BusinessId);
                return model;
            }
            if (OrderNumber == 2)
            {
                model.OrderByDescending(o => o.BusinessId);
                return model;
            }

            if (OrderNumber == 3)
            {
                model.OrderByDescending(o => o.BusinessNameAR);
                return model;
            }

            if (OrderNumber == 4)
            {
                model.OrderByDescending(o => o.BusinessNameEn);
                return model;
            }

            if (OrderNumber == 5)
            {
                model.OrderBy(o => o.BusinessNameAR);
                return model;
            }

            if (OrderNumber == 6)
            {
                model.OrderBy(o => o.BusinessNameEn);
                return model;
            }

            if (OrderNumber == 7)
            {
                model.OrderBy(o => o.companyWithBrand.Count);
                return model;
            }

            if (OrderNumber == 8)
            {
                model.OrderByDescending(o => o.companyWithBrand.Count);
                return model;
            }


            return model;

        }
    }
}
