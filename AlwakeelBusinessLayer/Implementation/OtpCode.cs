﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer.APIView;

namespace AlwakeelBusinessLayer.Implementation
{
    public class OtpCode : Base, IOtpCode
    {
        Random _random = new Random();
        public string Add(UserLoginDomain model)
        {
            string code = GCode(model.UCode);
            UserLogin userLogin = new UserLogin()
            {
                FullName = model.FullName,
                isVerificat = model.Utype =="e" ?true:false,
                UCode = code,
                UEmail = model.UEmail,
                UPhone = model.UPhone,
                Utype = model.Utype,
                Id = model.Id
            };
            db.UserLogin.Add(userLogin);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return userLogin.UCode;
            }
            else
            {
                return "0";
            }
        }

        public bool ifCodeTure(string otpCod)
        {
            List<td_CommercialAds> optList = db.td_CommercialAds.Where(x => x.TitleEn == otpCod).ToList();
            if (optList.Count > 0)
            {
                foreach (var item in optList)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsAlreadyExists(string phone)
        {
            int Number = db.AspNetUsers.Where(x => x.PhoneNumber == phone).Count();
            if (Number == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public string FindEmail(string email)
        {
            try
            {
                UserLogin userLogin = db.UserLogin.Where(x => x.UEmail == email).SingleOrDefault();
                if (userLogin.UEmail != null)
                {
                    return userLogin.UEmail;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
           
        }

        public string GCode(string Code)
        {
            string newCode;
            List<UserLogin> users = db.UserLogin.Where(x => x.UCode == Code).ToList();
            if (users.Count > 0)
            {
                newCode = _random.Next(1000, 100000).ToString();
                GCode(newCode);
            }

            return Code;

        }

        public UserLoginDomain GetByPoneNubmer(string PoneNubmer)
        {
            UserLoginDomain model = db.UserLogin.Where(x => x.UPhone == PoneNubmer && x.isVerificat == true).Select(e => new UserLoginDomain
            {
                Id = e.Id,
                FullName = e.FullName,
                UEmail=e.UEmail

            }).SingleOrDefault();
            return model;
        }

        public UserLoginDomain Verificat(VerificationModel verification) 
        {
            UserLoginDomain model = new UserLoginDomain();
            UserLogin user;
            if (verification.Email==null)
            {
                user = db.UserLogin.Where(x => x.UCode == verification.code && x.UEmail == verification.Email).SingleOrDefault();
            }
            else
            {
                user = db.UserLogin.Where(x => x.UCode == verification.code && x.UPhone == verification.PhoneNumber).SingleOrDefault();
            }
          
            if (user != null)
            {
                model.Id = user.Id;
                model.FullName = user.FullName;

                user.isVerificat = true;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();

                return model;
            }
            else
            {
                return model;
            }
           
           
        }

        public AspNetUsers GetUser(string Id) 
        {
            AspNetUsers aspNetUsers = db.AspNetUsers.Find(Id);
            return aspNetUsers;
        
        }


        public string UpdateCode(string phone,string code) 
        {
            string Newcode = GCode(code);
            UserLogin userLogin = db.UserLogin.Where(x => x.UPhone == phone).SingleOrDefault();
            if (userLogin !=null)
            {
                userLogin.UCode = Newcode;
                userLogin.isVerificat = false;
                db.Entry(userLogin).State = EntityState.Modified;
                db.SaveChanges();
                return userLogin.UCode;
            }
            else
            {
                return "0";
            }


        }

        public string FindByEmail(string email)
        {

            try
            {
                UserLogin userLogin = db.UserLogin.Where(x => x.UEmail == email).SingleOrDefault();
                if (userLogin != null)
                {
                    return userLogin.UCode;
                }
                else
                {
                    return "no";
                }
            }
            catch (Exception)
            {

                return "no";
            }
          
        }

        public string FindByPhoneNumber(string PhoneNumber)
        {
            try
            {
                UserLogin userLogin = db.UserLogin.Where(x => x.UPhone == PhoneNumber).SingleOrDefault();
                if (userLogin != null)
                {
                    return userLogin.UCode;
                }
                else
                {
                    return "no";
                }
            }
            catch (Exception)
            {

                return "no";
            }
          
        }

        public string FindByCodeEmail(string code, string email)
        {
            try
            {
                UserLogin userLogin = db.UserLogin.Where(x => x.UEmail == email && x.UCode == code).SingleOrDefault();
                if (userLogin != null)
                {
                    return userLogin.Id;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
           
        }

        public string FindByCodePhoneNumber(string code, string phoneNumber)
        {
            try
            {
                UserLogin userLogin = db.UserLogin.Where(x => x.UPhone == phoneNumber && x.UCode == code).SingleOrDefault();
                if (userLogin != null)
                {
                    return userLogin.Id;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
            
        }


        public Userinfo UserInfo(string userId)
        {
            Userinfo model = db.UserLogin.Where(x => x.Id == userId).Select(e => new Userinfo
            {
              
                FullName = e.FullName,
                Mycontact = e.Utype == "e"? e.UEmail: "+249"+e.UPhone

            }).SingleOrDefault();
            return model;
        }
    }
}
