﻿using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Common;
using AlwakeelRepositoryLayer;

namespace AlwakeelBusinessLayer.Implementation
{

    public class BusinessUsers : Base, IBusinessUsers
    {
        public int Add(BusinessUserDomain bud)
        {
            BusinessUser model = new BusinessUser()
            {
                BusinessId = bud.BusinessId,
                UserId = bud.UserId
            };

            db.BusinessUser.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
        public BusinessUserDomain Get(string UserId)
        {
            BusinessUser bu = db.BusinessUser.Find(UserId);
            BusinessUserDomain model = new BusinessUserDomain();
            model.UserId = bu.UserId;
            model.typeOrRole = bu.td_Business.BusinessType;
            model.BusinessId = bu.BusinessId;
            return model;
        }

        public List<BusinessUserDomain> GeUsers()
        {
            List<BusinessUserDomain> modelList = new List<BusinessUserDomain>();
            List<BusinessUser> List = db.BusinessUser.ToList();

            foreach (var item in List)
            {
                BusinessUserDomain model = new BusinessUserDomain();
                model.BusinessId = item.BusinessId;
                model.BusinessName = item.td_Business.BusinessNameEn;
                model.UserName = db.AspNetUsers.Find(item.UserId).UserName;
                model.UserId = item.UserId;
                modelList.Add(model);
            }

            return modelList;
        }

        public List<BusinessUserDomain> GeUsersByBusiness(int BId)
        {
            List<BusinessUserDomain> modelList = new List<BusinessUserDomain>();
            List<BusinessUser> List = db.BusinessUser.Where(e=> e.BusinessId==BId).ToList();

            foreach (var item in List)
            {
                BusinessUserDomain model = new BusinessUserDomain();
                model.BusinessId = item.BusinessId;
                model.BusinessName = item.td_Business.BusinessNameEn;
                model.UserName = db.AspNetUsers.Find(item.UserId).UserName;
                model.UserId = item.UserId;
                modelList.Add(model);
            }

            return modelList;
        }


    }
}
