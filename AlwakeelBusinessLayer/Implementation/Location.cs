﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Location : Base, ILocation
    {
        public int Add(LocationDomain model)
        {
            int number = (int)DateTime.Now.Month+DateTime.Now.Hour+DateTime.Now.Millisecond;
            td_PosLocations pos = new td_PosLocations()
            {
                BusinessId = model.BusinessId,
                posEmail = model.posEmail,
                posPhoneNumber = model.posPhoneNumber.ToString(),
                posRegion = model.posRegion,
                posId = number + DateTime.Now.Millisecond,
                POSName=model.POSName,
                POStype=model.POStype
            };
            db.td_PosLocations.Add(pos);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_PosLocations pos = db.td_PosLocations.Find(Id);
            if (pos !=null)
            {
                db.Entry(pos).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<LocationDomainView> Get()
        {
            List<LocationDomainView> model = db.td_PosLocations.Select(x => new LocationDomainView
            {
                BusinessId=x.BusinessId,
                BusinessNameAR=x.td_Business.BusinessNameAR,
                BusinessNameEn=x.td_Business.BusinessNameEn,
                CityNameAr=x.td_Regions.td_City.CityNameAr,
                CityNameEn=x.td_Regions.td_City.CityNameEn,
                posEmail=x.posEmail,
                posId=x.posId,
                posPhoneNumber=x.posPhoneNumber,
                posRegion=x.posRegion,
                StateNameAr=x.td_Regions.td_City.td_State.StateNameAr,
                StateNameEn=x.td_Regions.td_City.td_State.StateNameEn,
                RegionNameAr=x.td_Regions.RegionNameAr,
                RegionNameEn=x.td_Regions.RegionNameEn,
                POSName=x.POSName,
                POStype=x.POStype

            }).ToList();
            return model;
        }

        public List<LocationDomainView> GetByBId(int BId)
        {
            List<LocationDomainView> model = db.td_PosLocations.Where(b=> b.BusinessId==BId).Select(x => new LocationDomainView
            {
                BusinessId = x.BusinessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                CityNameAr = x.td_Regions.td_City.CityNameAr,
                CityNameEn = x.td_Regions.td_City.CityNameEn,
                posEmail = x.posEmail,
                posId = x.posId,
                posPhoneNumber = x.posPhoneNumber,
                posRegion = x.posRegion,
                StateNameAr = x.td_Regions.td_City.td_State.StateNameAr,
                StateNameEn = x.td_Regions.td_City.td_State.StateNameEn,
                POSName = x.POSName,
                POStype = x.POStype

            }).ToList();
            return model;
        }

        public LocationDomain GetById(int Id)
        {
            td_PosLocations pos = db.td_PosLocations.Find(Id);
            LocationDomain model = new LocationDomain()
            {
                BusinessId=pos.BusinessId,
                CityId=pos.td_Regions.CityId,
                posEmail=pos.posEmail,
                posId=pos.posId,
                posPhoneNumber=pos.posPhoneNumber,
                posRegion=pos.posRegion,
                StateId=(int)pos.td_Regions.td_City.StateId,
                POSName = pos.POSName,
                POStype = pos.POStype
            };

            return model;
        }

        public int Update(LocationDomain model)
        {
            td_PosLocations pos = db.td_PosLocations.Find(model.posId);
            if (pos != null)
            {
                pos.posPhoneNumber = model.posPhoneNumber;
                pos.posRegion = model.posRegion;
                pos.posEmail = model.posEmail;
                pos.BusinessId = model.BusinessId;
                pos.POSName = model.POSName;
                pos.POStype = model.POStype;
                db.Entry(pos).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
