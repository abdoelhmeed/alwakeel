﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class City : Base, ICity
    {
        public int Add(CityDomain city)
        {
            td_City model = new td_City();
            model.CityNameAr = city.CityNameAr;
            model.CityNameEn = city.CityNameEn;
            model.StateId = city.StateId;
            db.td_City.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
           
        }

        public int Delete(int Id)
        {
            try
            {
                td_City model = db.td_City.Find(Id);
                if (model != null)
                {
                    db.Entry(model).State = EntityState.Deleted;
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {

                return -1;
            }
        
        }

        public List<CityDomain> Get()
        {
            List<CityDomain> model = db.td_City.Select(x => new CityDomain {
                CityId = x.CityId,
                CityNameAr = x.CityNameAr,
                CityNameEn = x.CityNameEn,
                StateId = x.StateId,
                StateNameAr=x.td_State.StateNameAr,
               StateNameEn=x.td_State.StateNameEn 
            }).ToList();
            return model;
        }

        public CityDomain GetById(int Id)
        {
            CityDomain modle = new CityDomain();
            td_City x = db.td_City.Find(Id);

            modle.CityId = x.CityId;
            modle.CityNameAr = x.CityNameAr;
            modle.CityNameEn = x.CityNameEn;
            modle.StateId = x.StateId;
            return modle;
        }

        public List<CityDomain> GetByStateId(int StateId)
        {
            List<CityDomain> model = db.td_City.Where(u=> u.StateId== StateId).Select(x => new CityDomain
            {
                CityId = x.CityId,
                CityNameAr = x.CityNameAr,
                CityNameEn = x.CityNameEn,
                StateId = x.StateId,
                StateNameAr = x.td_State.StateNameAr,
                StateNameEn = x.td_State.StateNameEn
            }).ToList();

            return model;
        }

        public int Update(CityDomain city)
        {
            td_City model =db.td_City.Find(city.CityId);
            if (model !=null)
            {
                model.CityNameAr = city.CityNameAr;
                model.CityNameEn = city.CityNameEn;
                model.StateId = city.StateId;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            } else
            {
                return 0;
            }
        }

       
    }
}
