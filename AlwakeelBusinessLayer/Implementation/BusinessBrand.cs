﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class BusinessBrand : Base, IBusinessBrand
    {
        public int Add(BusinessBrandDoamin model)
        {
            td_BusinessBrands bbrand = new td_BusinessBrands()
            {
                BrandId = model.BrandId,
                BusinessId = model.BusinessId
            };
            db.td_BusinessBrands.Add(bbrand);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_BusinessBrands bbrand = db.td_BusinessBrands.Find(Id);
            if (bbrand != null)
            {
                db.Entry(bbrand).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<BusinessBrandDoamin> Get()
        {
            List<BusinessBrandDoamin> model = db.td_BusinessBrands.Select(x =>
            new BusinessBrandDoamin
            {
                BrandId = x.BrandId,
                BrandNameAr = x.td_Brand.BrandNameAr,
                BrandNameEn = x.td_Brand.BrandNameEn,
                BusinessId = x.BusinessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                BrandImage=x.td_Brand.BrandsLogo,
                BusinessImage=x.td_Business.BusinessLogo,
                Id = x.Id
            }).ToList();

            return model;
        }

        public List<BusinessBrandDoamin> GetByBIdCatId(int CatId, int businessId)
        {
            List<BusinessBrandDoamin> model = db.td_BusinessBrands.Where(x => x.BusinessId == businessId && x.td_Brand.CatId==CatId).Select(x =>
                 new BusinessBrandDoamin
                 {
                     BrandId = x.BrandId,
                     BrandNameAr = x.td_Brand.BrandNameAr,
                     BrandNameEn = x.td_Brand.BrandNameEn,
                     BusinessId = x.BusinessId,
                     BusinessNameAR = x.td_Business.BusinessNameAR,
                     BusinessNameEn = x.td_Business.BusinessNameEn,
                     BrandImage = x.td_Brand.BrandsLogo,
                     BusinessImage = x.td_Business.BusinessLogo,
                     Id = x.Id
                 }).ToList();

            return model;
        }

        public List<BusinessBrandDoamin> GetByBrand(int brandId)
        {
            List<BusinessBrandDoamin> model = db.td_BusinessBrands.Where(x => x.BrandId == brandId).Select(x =>
                new BusinessBrandDoamin
                {
                    BrandId = x.BrandId,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    BusinessId = x.BusinessId,
                    BusinessNameAR = x.td_Business.BusinessNameAR,
                    BusinessNameEn = x.td_Business.BusinessNameEn,
                    BrandImage = x.td_Brand.BrandsLogo,
                    BusinessImage = x.td_Business.BusinessLogo,
                    Id=x.Id
                   
                }).ToList();

            return model;
        }
        public List<BusinessBrandDoamin> GetByBusinessId(int businessId)
        {
            List<BusinessBrandDoamin> model = db.td_BusinessBrands.Where(x => x.BusinessId == businessId).Select(x =>
                 new BusinessBrandDoamin
                 {
                     BrandId = x.BrandId,
                     BrandNameAr = x.td_Brand.BrandNameAr,
                     BrandNameEn = x.td_Brand.BrandNameEn,
                     BusinessId = x.BusinessId,
                     BusinessNameAR = x.td_Business.BusinessNameAR,
                     BusinessNameEn = x.td_Business.BusinessNameEn,
                     BrandImage = x.td_Brand.BrandsLogo,
                     BusinessImage = x.td_Business.BusinessLogo,
                     Id = x.Id
                 }).ToList();

            return model;
        }

        public int Update(BusinessBrandDoamin model)
        {
            td_BusinessBrands bbrand = db.td_BusinessBrands.Find(model.Id);
            if (bbrand != null)
            {
                bbrand.BrandId = model.BrandId;
                bbrand.BusinessId = model.BusinessId;
                db.Entry(bbrand).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}