﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class VideoYouTube : Base, IVideoYouTube
    {
        public int Add(VideoDomain Vd)
        {
            td_Video model = new td_Video() {
                VideoDate=DateTime.Now,
                VideoNameEn=Vd.VideoNameEn,
                VideoNameAr=Vd.VideoNameAr,
                VideoUrl=Vd.VideoUrl

            };

            db.td_Video.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            try
            {
                td_Video model = db.td_Video.Find(Id);
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {

                return -1;
            }
           
        }

        public List<VideoDomain> Get()
        {
            List<VideoDomain> model = db.td_Video.Select(v => new VideoDomain
            {
                id=v.id,
                VideoDate=v.VideoDate,
                VideoNameAr=v.VideoNameAr,
                VideoNameEn=v.VideoNameEn,
                VideoUrl=v.VideoUrl
            }).ToList();


            return model;
        }
    }
}
