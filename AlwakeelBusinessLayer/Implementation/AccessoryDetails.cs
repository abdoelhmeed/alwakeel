﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class AccessoryDetails : Base, IAccessoryDetails
    {
        public int Add(AccessoryDetailsDomain model)
        {
            td_AccessoryDetails AccessoryDetails = new td_AccessoryDetails()
            {
                ModelId=model.ModelId,
                PId=model.PId,
            };
            db.td_AccessoryDetails.Add(AccessoryDetails);
            db.SaveChanges();
            return 1;
        }

        public int Delete(int Id)
        {
            td_AccessoryDetails model = db.td_AccessoryDetails.Find(Id);
            int PId = (int)model.PId;
            db.Entry(model).State = EntityState.Deleted;
            db.SaveChanges();
            return PId;
        }

        public List<AccessoryDetailsViewDomain> GetByPId(int PId)
        {
            List<AccessoryDetailsViewDomain> model = db.td_AccessoryDetails.Where(x => x.PId == PId)
                .Select(a => new AccessoryDetailsViewDomain
                {
                    PId=a.PId,
                    BrandNameAR=a.td_CarModel.td_CarClass.td_Brand.BrandNameAr,
                    BrandNameEN = a.td_CarModel.td_CarClass.td_Brand.BrandNameEn,
                    ClassNameAR=a.td_CarModel.td_CarClass.CarClassNameAr,
                    ClassNameEN = a.td_CarModel.td_CarClass.CarClassNameEn,
                    ModelYears =a.td_CarModel.ModelYears,
                    Id=a.Id,
                    ModelId=a.ModelId
                }).ToList();
            return model;
        }



        public int Update(AccessoryDetailsDomain model)
        {
            throw new NotImplementedException();
        }
    }
}
