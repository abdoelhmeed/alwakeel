﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Implementation
{
    public class DealOfday : Base, IDealOfday
    {
        public int Add(DealOfdayDomain model)
        {
            td_DealsOfDay dealsOfDay = new td_DealsOfDay()
            {
                BusinessId = model.BusinessId,
                DescriptionAr = model.DescriptionAr,
                DescriptionEn = model.DescriptionEn,
                EndDate = DateTime.Now.AddDays(model.NumberOfDays),
                ImageOne = model.ImageOne,
                ImageThree = model.ImageThree,
                ImageTow = model.ImageTow,
                Id = model.Id,
                NewPrice = model.NewPrice,
                OldPrice = model.OldPrice,
                StartDate = DateTime.Now,
                TitleAr = model.TitleAr,
                TitleEn = model.TitleEn,
               
            };
            db.td_DealsOfDay.Add(dealsOfDay);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return dealsOfDay.Id;
            }
            else
            {
                return 0;
            }

        }

        public List<DealOfdayViewDomain> GetAll()
        {
            List<DealOfdayViewDomain> model = new List<DealOfdayViewDomain>();

           List <td_DealsOfDay> dayList = db.td_DealsOfDay.OrderByDescending(o => o.Id).Take(50).ToList();
            foreach (var item in dayList)
            {
                DealOfdayViewDomain dealOfday = new DealOfdayViewDomain();

                dealOfday.BusinessId =(int)item.BusinessId;
                dealOfday.DescriptionEn = item.DescriptionEn;
                dealOfday.Id = item.Id;
                dealOfday.DescriptionAr = item.DescriptionAr;
                dealOfday.NumberOfDays = -1 * (int)(item.StartDate - item.EndDate).Value.Days;
                dealOfday.NewPrice = (decimal)item.NewPrice;
                dealOfday.TitleAr = item.TitleAr;
                dealOfday.OldPrice = (decimal)item.OldPrice;
                dealOfday.TitleEn = item.TitleEn;
                dealOfday.BusinessLogo = item.td_Business.BusinessLogo;
                dealOfday.BusinessNameAr = item.td_Business.BusinessNameAR;
                dealOfday.BusinessNameEn = item.td_Business.BusinessNameEn;
                model.Add(dealOfday);
            }
          
            return model;
        }

        public List<DealOfdayViewDomain> GetByBId(int BId)
        {
            List<DealOfdayViewDomain> model = new List<DealOfdayViewDomain>();

            List<td_DealsOfDay> dayList = db.td_DealsOfDay.Where(e=> e.BusinessId==BId).OrderByDescending(o => o.Id).Take(50).ToList();
            foreach (var item in dayList)
            {
                DealOfdayViewDomain dealOfday = new DealOfdayViewDomain();

                dealOfday.BusinessId = (int)item.BusinessId;
                dealOfday.DescriptionEn = item.DescriptionEn;
                dealOfday.Id = item.Id;
                dealOfday.DescriptionAr = item.DescriptionAr;
                dealOfday.NumberOfDays = -1 * (int)(item.StartDate - item.EndDate).Value.Days;
                dealOfday.NewPrice = (decimal)item.NewPrice;
                dealOfday.TitleAr = item.TitleAr;
                dealOfday.OldPrice = (decimal)item.OldPrice;
                dealOfday.TitleEn = item.TitleEn;
                dealOfday.BusinessLogo = item.td_Business.BusinessLogo;
                dealOfday.BusinessNameAr = item.td_Business.BusinessNameAR;
                dealOfday.BusinessNameEn = item.td_Business.BusinessNameEn;
                model.Add(dealOfday);
            }

            return model;
        }

        public DealOfdayDomain GetById(int Id)
        {
            DealOfdayDomain model = new DealOfdayDomain();
            var Deal = db.td_DealsOfDay.Find(Id);
            if (Deal != null)
            {
                model.BusinessId = Deal.BusinessId;
                model.DescriptionAr = Deal.DescriptionAr;
                model.DescriptionEn = Deal.DescriptionEn;
                model.NumberOfDays = -1 * (Deal.StartDate - Deal.EndDate).Value.Days;
                model.ImageOne = Deal.ImageOne;
                model.ImageThree = Deal.ImageThree;
                model.ImageTow = Deal.ImageTow;
                model.Id = Deal.Id;
                model.NewPrice = (decimal)Deal.NewPrice;
                model.OldPrice = (decimal)Deal.OldPrice;
                model.TitleAr = Deal.TitleAr;
                model.TitleEn = Deal.TitleEn;
            }
            return model;
        }

        public int Update(DealOfdayDomain model)
        {
            td_DealsOfDay dealsOfDay = db.td_DealsOfDay.Find(model.Id);
            if (dealsOfDay != null)
            {
                dealsOfDay.BusinessId= model.BusinessId;
                dealsOfDay.DescriptionAr = model.DescriptionAr;
                dealsOfDay.DescriptionEn = model.DescriptionEn;
                dealsOfDay.EndDate = DateTime.Now.AddDays(model.NumberOfDays);
                dealsOfDay.ImageOne = model.ImageOne;
                dealsOfDay.ImageThree = model.ImageThree;
                dealsOfDay.ImageTow = model.ImageTow;
                dealsOfDay.NewPrice = model.NewPrice;
                dealsOfDay.OldPrice = model.OldPrice;
                dealsOfDay.StartDate = DateTime.Now;
                dealsOfDay.TitleAr = model.TitleAr;
                dealsOfDay.TitleEn = model.TitleEn;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            return 0;

        }

        public void Delete(int id)
        {
            td_DealsOfDay td_DealsOfDay = db.td_DealsOfDay.Find(id);
            db.Entry(td_DealsOfDay).State = EntityState.Deleted;
            db.SaveChanges();
        }

        //API
        public List<DealOfdayViewDomain> GetToApi()
        {
            List<DealOfdayViewDomain> dealOfdayList = new List<DealOfdayViewDomain>();
            List<td_DealsOfDay> days = db.td_DealsOfDay.Where(x => x.EndDate >= DateTime.Now).OrderByDescending(o => o.Id).ToList();
          
            foreach (var item in days)
            {
                List<string> Images = new List<string>();
                DealOfdayViewDomain dealOfday = new DealOfdayViewDomain();

                dealOfday.BusinessId = (int)item.BusinessId;
                dealOfday.DescriptionEn = item.DescriptionEn;
                dealOfday.Id = item.Id;
                dealOfday.DescriptionAr = item.DescriptionAr;
                dealOfday.NumberOfDays = -1 * (int)(item.StartDate - item.EndDate).Value.Days;
                dealOfday.NewPrice = (decimal)item.NewPrice;
                dealOfday.TitleAr = item.TitleAr;
                dealOfday.OldPrice = (decimal)item.OldPrice;
                dealOfday.TitleEn = item.TitleEn;
                dealOfday.BusinessLogo = item.td_Business.BusinessLogo;
                dealOfday.BusinessNameAr = item.td_Business.BusinessNameAR;
                dealOfday.BusinessNameEn = item.td_Business.BusinessNameEn;
                if (item.ImageOne != null)
                {
                    Images.Add(item.ImageOne);
                }
                if (item.ImageTow != null)
                {
                    Images.Add(item.ImageTow);
                }
                if (item.ImageThree != null)
                {
                   Images.Add(item.ImageThree);
                }
                dealOfday.Images = Images;
                dealOfdayList.Add(dealOfday);
            }

            return dealOfdayList;
        }
    }
}
