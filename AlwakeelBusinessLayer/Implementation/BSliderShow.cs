﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlwakeelRepositoryLayer;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class BSliderShow : Base, IBSliderShow
    {
        public int Add(BusinessSliderDomain slider)
        {
            td_BusinessSlider businessSlider = new td_BusinessSlider()
            {
                BusinessId = slider.BusinessId,
                ImageUrl = slider.ImageUrl,
            };
            db.td_BusinessSlider.Add(businessSlider);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_BusinessSlider businessSlider = db.td_BusinessSlider.Find(Id);
            if (businessSlider != null)
            {
                db.Entry(businessSlider).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<BSliderViewDomain> Get()
        {
            List<BSliderViewDomain> model = db.td_BusinessSlider.Select(x => new BSliderViewDomain
            {
                BNameAR = x.td_Business.BusinessNameAR,
                BNameEn = x.td_Business.BusinessNameEn,
                BusinessId = x.BusinessId,
                ImageUrl = x.ImageUrl,
                id=x.id

            }).ToList();
            return model;
        }

        public List<BSliderViewDomain> GetByBId(int BId)
        {
            List<BSliderViewDomain> model = db.td_BusinessSlider.Where(b=>b.BusinessId==BId).Select(x => new BSliderViewDomain
            {
                BNameAR = x.td_Business.BusinessNameAR,
                BNameEn = x.td_Business.BusinessNameEn,
                BusinessId = x.BusinessId,
                ImageUrl = x.ImageUrl,
                id=x.id

            }).ToList();
            return model;
        }

        public List<string> GetImageListByBId(int BId)
        {
            List<string> model = db.td_BusinessSlider.Where(b => b.BusinessId == BId).Select(x => x.ImageUrl).ToList();
            return model;
        }
    }
}
