﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class UserRole : Base,IUserRole
    {
        public List<UserRolesDonmain> GetRole()
        {
            List<UserRolesDonmain> model = db.AspNetRoles.Where(u => u.Name != "Admin" && u.Name != "Employee").Select(x => new UserRolesDonmain
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return model;
        }
    }
}
