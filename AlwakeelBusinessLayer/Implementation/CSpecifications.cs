﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class CSpecifications : Base, ICSpecifications
    {
        public int Add(carSpecificationsDomain model)
        {
            td_carSpecifications cs = new td_carSpecifications()
            {
                BrandId=model.BrandId,
                CarClassId=model.CarClassId,
                ModelId=model.ModelId,
                SpeAr=model.SpeAr,
                SpeEn=model.SpeEn,
                SpeIcon=model.SpeIcon
            };
            db.td_carSpecifications.Add(cs);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            try
            {
                td_carSpecifications model = db.td_carSpecifications.Find(Id);
                if (model != null)
                {
                    db.Entry(model).State = EntityState.Deleted;
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {

                return -1;
            }
        }

        public carSpecificationsDomain GetById(int SpeId)
        {
            td_carSpecifications x = db.td_carSpecifications.Find(SpeId);
            carSpecificationsDomain model = new carSpecificationsDomain()
            {
                SpeId = x.SpeId,
                BrandId=x.BrandId,
                CarClassId=x.CarClassId,
                ModelId = x.ModelId,
                SpeAr=x.SpeAr,
                SpeEn=x.SpeEn,
                SpeIcon=x.SpeIcon
            };

            return model;

        }

        public List<carSpecificationsDomain> GetByModelId(int ModelId)
        {
            List<carSpecificationsDomain> model = db.td_carSpecifications.Where(u => u.ModelId == ModelId).Select(x=> new carSpecificationsDomain
            {
                ModelId=x.ModelId,
                BrandId=x.BrandId,
                CarClassId=x.CarClassId,
                SpeAr=x.SpeAr,
                SpeEn=x.SpeEn,
                SpeIcon=x.SpeIcon,
                SpeId=x.SpeId
            }).ToList();
            return model;
        }


        //API
        public List<FeaturesDomain> GetAPIFeatures(int ModelId) 
        {
            List<FeaturesDomain> model = db.td_carSpecifications.Where(e => e.ModelId == ModelId).Select(x=> new FeaturesDomain 
            { 
            FId=x.SpeId,
            FIcon=x.SpeIcon,
            FNameAR=x.SpeAr,
            FNameEn=x.SpeEn
            
            }).ToList();
            return model;
        }
    }
}
