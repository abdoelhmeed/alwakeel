﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer.APIView;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Color : Base, IColor
    {
        public int Add(ColorDomain model)
        {
            tdColors color = new tdColors()
            {
                colorIcon=model.colorIcon,
                colorIdNameAR=model.colorIdNameAR,
                colorIdNameEN=model.colorIdNameEN
            };
            db.tdColors.Add(color);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            try
            {
                tdColors model = db.tdColors.Find(Id);
                if (model != null)
                {
                    db.Entry(model).State = EntityState.Deleted;
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {

                return -1;
            }
           
        }

        public List<ColorDomain> Get()
        {
            List<ColorDomain> model = db.tdColors.Select(x => new ColorDomain
            {
                colorIcon=x.colorIcon,
                colorId=x.colorId,
                colorIdNameAR=x.colorIdNameAR,
                colorIdNameEN=x.colorIdNameEN

            }).ToList();
            return model;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Colors> GetApiColors()
        {
            List<Colors> model = db.tdColors.Select(x => new Colors
            {
                Icon = x.colorIcon,
                coloreId = x.colorId,
                NameAR = x.colorIdNameAR,
                NameEn = x.colorIdNameEN

            }).ToList();
            return model;
        }
    }
}
