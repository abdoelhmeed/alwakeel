﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer.APIView;
using AlwakeelDomainLayer.ViewVehicleAndProdct;

namespace AlwakeelBusinessLayer.Implementation
{
    public class ProductVehicle : Base, IProductVehicle
    {

        public int Add(PVehicleDomain vehicel, List<string> Images)
        {
            td_ProductsItem product = new td_ProductsItem()
            {
                CarClassId = vehicel.CarClassId,
                CatId = vehicel.CatId,
                ModelId = vehicel.ModelId,
                CColors = vehicel.CColors,
                AdPrice = vehicel.AdPrice,
                AdEntryDate = DateTime.Now,
                CEngineCapacityCC = vehicel.CEngineCapacityCC,
                AdStatus = vehicel.AdStatus,
                businessId = vehicel.businessId,
                CMileageKM = vehicel.CMileageKM,
                BrandId = vehicel.BrandId,
                DescribeAR = vehicel.DescribeAR,
                DescribeEN = vehicel.DescribeEN,
                isNew = vehicel.isNew,
                CTransmission = 0,
                AdendDate = vehicel.EndDate,
                dealOfday = false,
                Active = true,
                love = 0,
                Views = 0

            };
            db.td_ProductsItem.Add(product);
            int res = db.SaveChanges();
            if (res > 0)
            {
               
                foreach (var item in Images)
                {
                    td_PImage pI = new td_PImage()
                    {
                        ImageUrl = item,
                        PId = product.PId
                    };
                    db.td_PImage.Add(pI);
                    db.SaveChanges();
                }
               
                foreach (var item in vehicel.Features)
                {
                    td_Features feature = new td_Features()
                    {
                        PId = product.PId,
                        SpeId = item
                    };
                    db.td_Features.Add(feature);
                    db.SaveChanges();
                }

                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int delete(int PId)
        {


            td_ProductsItem ProductsItem = db.td_ProductsItem.Find(PId);
            if (ProductsItem != null)
            {
                List<td_PImage> images = db.td_PImage.Where(x => x.PId == PId).ToList();
                foreach (var item in images)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                List<td_Features> Features = db.td_Features.Where(x => x.PId == PId).ToList();
                foreach (var item in Features)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                List<Complain> complain = db.Complain.Where(x => x.PId == PId).ToList();
                foreach (var item in complain)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                db.Entry(ProductsItem).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public List<PVehicleViewDomain> Get()
        {
            List<PVehicleViewDomain> model = db.td_ProductsItem
                .Where(u => u.businessId != null && u.CatId == 1 || u.CatId == 2)
                .OrderByDescending(d => d.PId)
                .Select(x => new PVehicleViewDomain
                {

                    AdEntryDate = x.AdEntryDate,
                    AdPrice = (decimal)x.AdPrice,
                    AdStatus = x.AdStatus,
                    BrandId = (int)x.BrandId,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    businessId = (int)x.businessId,
                    BusinessNameAR = x.td_Business.BusinessNameAR,
                    BusinessNameEn = x.td_Business.BusinessNameEn,
                    CarClassId = (int)x.CarClassId,
                    CarClassNameAr = x.td_CarModel.td_CarClass.CarClassNameAr,
                    CarClassNameEn = x.td_CarModel.td_CarClass.CarClassNameEn,
                    CatId = (int)x.CatId,
                    CatNameAr = x.td_Category.NameAr,
                    CatNameEn = x.td_Category.NameEn,
                    CColors = (int)x.CColors,
                    CEngineCapacityCC = (int)x.CEngineCapacityCC,
                    CMileageKM = (int)x.CMileageKM,
                    CTransmission = x.CTransmission,
                    DescribeAR = x.DescribeAR,
                    DescribeEN = x.DescribeEN,
                    isNew = (bool)x.isNew,
                    ModelId = (int)x.ModelId,
                    ModelYears = x.td_CarModel.ModelYears,
                    PId = x.PId,
                    ColorNameAr = x.tdColors.colorIdNameAR,
                    ColorNameEn = x.tdColors.colorIdNameEN,
                    ColorImage = x.tdColors.colorIcon,
                    EndDate = x.AdendDate


                }).ToList();
            return model;
        }
        public PVehicleViewDomain GetById(int PId)
        {
            td_ProductsItem x = db.td_ProductsItem.Find(PId);
            PVehicleViewDomain model = new PVehicleViewDomain()
            {
                EndDate = x.AdendDate,
                AdEntryDate = x.AdEntryDate,
                AdPrice = (decimal)x.AdPrice,
                AdStatus = x.AdStatus,
                BrandId = (int)x.BrandId,
                BrandNameAr = x.td_Brand.BrandNameAr,
                BrandNameEn = x.td_Brand.BrandNameEn,
                businessId = (int)x.businessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                CarClassId = (int)x.CarClassId,
                CarClassNameAr = x.td_CarModel.td_CarClass.CarClassNameAr,
                CarClassNameEn = x.td_CarModel.td_CarClass.CarClassNameEn,
                CatId = (int)x.CatId,
                CatNameAr = x.td_Category.NameAr,
                CatNameEn = x.td_Category.NameEn,
                CColors = (int)x.CColors,
                CEngineCapacityCC = (int)x.CEngineCapacityCC,
                CMileageKM = (int)x.CMileageKM,
                CTransmission = x.CTransmission,
                DescribeAR = x.DescribeAR,
                DescribeEN = x.DescribeEN,
                isNew = (bool)x.isNew,
                ModelId = (int)x.ModelId,
                ModelYears = x.td_CarModel.ModelYears,
                PId = x.PId,
                ColorNameAr = x.tdColors.colorIdNameAR,
                ColorNameEn = x.tdColors.colorIdNameEN,
                ColorImage = x.tdColors.colorIcon
            };
            return model;
        }
        public List<PVehicleViewDomain> GetByBusiness(int busId)
        {
            List<PVehicleViewDomain> model = db.td_ProductsItem
                .Where(u => u.businessId == busId && u.CatId <= 2)
                .OrderByDescending(d => d.PId)
                .Select(x => new PVehicleViewDomain
                {
                    EndDate = x.AdendDate,
                    AdEntryDate = x.AdEntryDate,
                    AdPrice = (decimal)x.AdPrice,
                    AdStatus = x.AdStatus,
                    BrandId = (int)x.BrandId,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    businessId = (int)x.businessId,
                    BusinessNameAR = x.td_Business.BusinessNameAR,
                    BusinessNameEn = x.td_Business.BusinessNameEn,
                    CarClassId = (int)x.CarClassId,
                    CarClassNameAr = x.td_CarModel.td_CarClass.CarClassNameAr,
                    CarClassNameEn = x.td_CarModel.td_CarClass.CarClassNameEn,
                    CatId = (int)x.CatId,
                    CatNameAr = x.td_Category.NameAr,
                    CatNameEn = x.td_Category.NameEn,
                    CColors = (int)x.CColors,
                    CEngineCapacityCC = (int)x.CEngineCapacityCC,
                    CMileageKM = (int)x.CMileageKM,
                    CTransmission = x.CTransmission,
                    DescribeAR = x.DescribeAR,
                    DescribeEN = x.DescribeEN,
                    isNew = (bool)x.isNew,
                    ModelId = (int)x.ModelId,
                    ModelYears = x.td_CarModel.ModelYears,
                    PId = x.PId,
                    ColorNameAr = x.tdColors.colorIdNameAR,
                    ColorNameEn = x.tdColors.colorIdNameEN,
                    ColorImage = x.tdColors.colorIcon
                }).ToList();
            return model;
        }
        public List<PVehicleViewDomain> GetByUserId(string userId)
        {
            List<PVehicleViewDomain> model = db.td_ProductsItem
                .Where(u => u.userId == userId && u.CatId == 1 || u.CatId == 2)
                .OrderByDescending(d => d.PId)
                .Select(x => new PVehicleViewDomain
                {
                    EndDate = x.AdendDate,
                    AdEntryDate = x.AdEntryDate,
                    AdPrice = (decimal)x.AdPrice,
                    AdStatus = x.AdStatus,
                    BrandId = (int)x.BrandId,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    businessId = (int)x.businessId,
                    BusinessNameAR = x.td_Business.BusinessNameAR,
                    BusinessNameEn = x.td_Business.BusinessNameEn,
                    CarClassId = (int)x.CarClassId,
                    CarClassNameAr = x.td_CarModel.td_CarClass.CarClassNameAr,
                    CarClassNameEn = x.td_CarModel.td_CarClass.CarClassNameEn,
                    CatId = (int)x.CatId,
                    CatNameAr = x.td_Category.NameAr,
                    CatNameEn = x.td_Category.NameEn,
                    CColors = (int)x.CColors,
                    CEngineCapacityCC = (int)x.CEngineCapacityCC,
                    CMileageKM = (int)x.CMileageKM,
                    CTransmission = x.CTransmission,
                    DescribeAR = x.DescribeAR,
                    DescribeEN = x.DescribeEN,
                    isNew = (bool)x.isNew,
                    ModelId = (int)x.ModelId,
                    ModelYears = x.td_CarModel.ModelYears,
                    PId = x.PId,
                    ColorNameAr = x.tdColors.colorIdNameAR,
                    ColorNameEn = x.tdColors.colorIdNameEN,
                    ColorImage = x.tdColors.colorIcon
                }).ToList();
            return model;
        }
        public PVehicleDomain GetByIdToEdit(int PId)
        {
            td_ProductsItem x = db.td_ProductsItem.Find(PId);
            PVehicleDomain model = new PVehicleDomain()
            {
                CarClassId = (int)x.CarClassId,
                CatId = (int)x.CatId,
                ModelId = (int)x.ModelId,
                CColors = (int)x.CColors,
                AdPrice = (decimal)x.AdPrice,
                AdEntryDate = DateTime.Now,
                CEngineCapacityCC = (int)x.CEngineCapacityCC,
                AdStatus = x.AdStatus,
                businessId = (int)x.businessId,
                CMileageKM = (int)x.CMileageKM,
                BrandId = (int)x.BrandId,
                DescribeAR = x.DescribeAR,
                DescribeEN = x.DescribeEN,
                isNew = (bool)x.isNew,
                CTransmission = 0,
                EndDate = x.AdEntryDate
            };
            return model;
        }
        public int Update(PVehicleDomain vehicel)
        {

            td_ProductsItem product = db.td_ProductsItem.Find(vehicel.PId);
            if (product != null)
            {
                product.CarClassId = vehicel.CarClassId;
                product.CatId = vehicel.CatId;
                product.ModelId = vehicel.ModelId;
                product.CColors = vehicel.CColors;
                product.AdPrice = vehicel.AdPrice;
                product.CEngineCapacityCC = vehicel.CEngineCapacityCC;
                product.AdStatus = vehicel.AdStatus;
                product.businessId = vehicel.businessId;
                product.CMileageKM = vehicel.CMileageKM;
                product.BrandId = vehicel.BrandId;
                product.DescribeAR = vehicel.DescribeAR;
                product.DescribeEN = vehicel.DescribeEN;
                product.isNew = vehicel.isNew;
                product.AdendDate = vehicel.EndDate;
                db.Entry(product).State = EntityState.Modified;
                int res = db.SaveChanges();
                return 1;

            }
            else
            {
                return 0;
            }
        }
        /// API

        public List<VehicleHomePage> GetCertificates()
        {
            List<VehicleHomePage> model = db.td_ProductsItem.Where(w => w.CatId < 4 && w.td_Business.BusinessId == 1).OrderByDescending(o => o.PId).Take(20).Select(x => new VehicleHomePage
            {
                CatNameAR = x.td_Category.NameAr,
                CatNameEn = x.td_Category.NameEn,
                Image = x.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                NewAR = (bool)x.isNew ? "جديد" : "مستعمل",
                NewEn = (bool)x.isNew ? "New" : "Used",
                PId = x.PId,
                Price = (decimal)x.AdPrice,
                VehicleInfoEn = x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears,
                VehicleInfoAR = x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                love = x.love,
                Views = x.Views
            }).ToList();
            return model;

        }
        public List<VehicleHomePage> GetAPIFeatured(int CatId)
        {


            List<VehicleHomePage> modelUser = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == CatId && w.userId != null && w.AdendDate >= DateTime.Now).OrderByDescending(o => o.PId).Take(10).Select(x => new VehicleHomePage
            {
                CatNameAR = x.td_Category.NameAr,
                CatNameEn = x.td_Category.NameEn,
                Image = x.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                NewAR = (bool)x.isNew ? "جديد" : "مستعمل",
                NewEn = (bool)x.isNew ? "New" : "Used",
                PId = x.PId,
                Price = (decimal)x.AdPrice,
                VehicleInfoEn = x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears,
                VehicleInfoAR = x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                love = x.love,
                Views = x.Views
            }).ToList();

            List<VehicleHomePage> model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == CatId && w.td_Business.BusinessType.Contains("CarShowrooms") && w.AdendDate >= DateTime.Now).OrderByDescending(o => o.PId).Take(20).Select(x => new VehicleHomePage
            {
                CatNameAR = x.td_Category.NameAr,
                CatNameEn = x.td_Category.NameEn,
                Image = x.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                NewAR = (bool)x.isNew ? "جديد" : "مستعمل",
                NewEn = (bool)x.isNew ? "New" : "Used",
                PId = x.PId,
                Price = (decimal)x.AdPrice,
                VehicleInfoEn = x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears,
                VehicleInfoAR = x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                love = x.love,
                Views = x.Views
            }).ToList();

            model.AddRange(modelUser);

            return model;
        }
    
        public List<VehicleList> GetApiVehicleLists(int CatId, int take)
        {
            List<VehicleList> vehicles = db.td_ProductsItem.Where(x => x.CatId == CatId)
                .OrderBy(o => o.PId)
                .Take(take)
                .Select(v => new VehicleList
                {
                    CatNameAR = v.td_Category.NameAr,
                    CatNameEn = v.td_Category.NameEn,
                    Image = v.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                    NewAR = (bool)v.isNew ? "جديد" : "مستعمل",
                    NewEn = (bool)v.isNew ? "New" : "Used",
                    PId = v.PId,
                    Price = (decimal)v.AdPrice,
                    VehicleInfoEn = v.td_Brand.BrandNameEn + " " + v.td_CarModel.td_CarClass.CarClassNameEn + " " + v.td_CarModel.ModelYears,
                    VehicleInfoAR = v.td_Brand.BrandNameAr + " " + v.td_CarModel.td_CarClass.CarClassNameAr + " " + v.td_CarModel.ModelYears,

                }).ToList();
            return vehicles;
        }
    }
}
