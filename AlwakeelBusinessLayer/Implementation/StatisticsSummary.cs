﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Implementation
{
    public class StatisticsSummary : Base, IStatisticsSummary
    {
        public StatisticsSummaryDomain GetAll()
        {
            StatisticsSummaryDomain model = new StatisticsSummaryDomain();
            model.TatalPordect = db.td_ProductsItem.Count();
            model.Employee = db.BusinessUser.Count();
            model.MyBrand = db.td_Brand.Count();
            model.OurPartners = db.td_Business.Count();
            model.RentaCarsOrdes = db.td_OrderRentCar.Count();
            return model;

        }

        public StatisticsSummaryDomain GetByBrand(int BId)
        {
            StatisticsSummaryDomain model = new StatisticsSummaryDomain();
            model.TatalPordect = db.td_ProductsItem.Where(x=> x.businessId==BId).Count();
            model.Employee = db.BusinessUser.Count();
            model.MyBrand = db.td_Brand.Count();
            model.OurPartners = db.td_Business.Count();
            model.RentaCarsOrdes = db.td_OrderRentCar.Where(x => x.tdRentCar.BusinessId == BId).Count();
            return model;
        }
    }
}
