﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class CustomersList : Base, ICustomersList
    {
        public List<CustomerListDomain> Get()
        {
            List<CustomerListDomain> model = new List<CustomerListDomain>();
            List<CustomerListDomain> customerLists = db.UserLogin.Select(x => new CustomerListDomain 
            { 
             Email=x.UEmail,
             Name=x.FullName,
             Phone=x.UPhone
            }).ToList();


            List<CustomerListDomain> customerLists1 = db.td_NotifyMe.Select(x => new CustomerListDomain 
            {
            Email=x.Cemail,
            Name="No",
            Phone="No"
            }).ToList();

            customerLists.AddRange(customerLists1);
            return customerLists;

        }
    }
}
