﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class State : Base, IState
    {
        public int Add(StateDomain state)
        {
            td_State model = new td_State();
            model.StateId = state.StateId;
            model.StateNameAr = state.StateNameAr;
            model.StateNameEn = state.StateNameEn;
            db.td_State.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Delete(int Id)
        {
            try
            {
                td_State model = db.td_State.Find(Id);
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
          
        }
        
        public List<StateDomain> Get()
        {
            List<StateDomain> model = db.td_State.Select(x => new StateDomain {
                StateId=x.StateId,
                StateNameAr=x.StateNameAr,
                StateNameEn=x.StateNameEn

            }).ToList();
            return model;
        }

        public StateDomain GetById(int Id)
        {
            StateDomain model = new StateDomain();
            td_State x = db.td_State.Find(Id);
            model.StateId = x.StateId;
            model.StateNameAr = x.StateNameAr;
            model.StateNameEn = x.StateNameEn;
            return model;

        }

        public int Update(StateDomain state)
        {
          
            td_State model = db.td_State.Find(state.StateId);
            if (model !=null)
            {
                model.StateNameAr = state.StateNameAr;
                model.StateNameEn = state.StateNameEn;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
          
            
        }
    }
}
