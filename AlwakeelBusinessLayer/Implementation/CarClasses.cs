﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelRepositoryLayer;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Interfaces;
using System.Data.Entity;
namespace AlwakeelBusinessLayer.Implementation
{
    public class CarClasses : Base, ICarClass
    {
        public int Add(CarClassDomain carClass)
        {
            td_CarClass classes = new td_CarClass()
            {
                BrandId=carClass.BrandId,
                CarClassNameAr=carClass.CarClassNameAr,
                CarClassNameEn=carClass.CarClassNameEn
            };
            db.td_CarClass.Add(classes);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }else
            {
                return 0;
            }
        }

       

        public int Delete(int Id)
        {
            try
            {
                td_CarClass model = db.td_CarClass.Find(Id);
              
                    db.Entry(model).State = EntityState.Deleted;
                   int res= db.SaveChanges();
                if (res > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
                   
              
            }
            catch (Exception)
            {

                return -1;
            }
           
        }

        public List<CarClassDomainViews> Get()
        {
            List<CarClassDomainViews> model = db.td_CarClass.Select(x => new CarClassDomainViews {
                BrandId = x.BrandId,
                BrandNameAr=x.td_Brand.BrandNameAr,
                BrandNameEn=x.td_Brand.BrandNameEn,
                CarClassId=x.CarClassId,
                CarClassNameAr=x.CarClassNameAr,
                CarClassNameEn=x.CarClassNameEn
            }).ToList();

            return model;
        }

        public List<CarClassDomainViews> GetByBrandId(int BrandId)
        {
            List<CarClassDomainViews> model = db.td_CarClass.Where(u=> u.BrandId==BrandId).Select(x => new CarClassDomainViews
            {
                BrandId = x.BrandId,
                BrandNameAr = x.td_Brand.BrandNameAr,
                BrandNameEn = x.td_Brand.BrandNameEn,
                CarClassId = x.CarClassId,
                CarClassNameAr = x.CarClassNameAr,
                CarClassNameEn = x.CarClassNameEn
            }).ToList();

            return model;
        }

        public CarClassDomain GetById(int id)
        {
            td_CarClass x = db.td_CarClass.Find(id);
            CarClassDomain model = new CarClassDomain()
            {
                BrandId=x.BrandId,
                CarClassId=x.CarClassId,
                CarClassNameAr=x.CarClassNameAr,
                CarClassNameEn=x.CarClassNameEn
            };

            return model;
        }

        public int Update(CarClassDomain carClass)
        {
            td_CarClass model = db.td_CarClass.Find(carClass.CarClassId);
            if (model !=null)
            {
                model.BrandId = carClass.BrandId;
                model.CarClassId = carClass.CarClassId;
                model.CarClassNameAr = carClass.CarClassNameAr;
                model.CarClassNameEn = carClass.CarClassNameEn;

                db.Entry(model).State = EntityState.Modified;
                int res = db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public bool IsHasClass(CarClassDomain model) 
        {
            int Number = db.td_CarClass.Where(x=> x.CarClassNameAr==model.CarClassNameAr && x.CarClassNameEn==model.CarClassNameEn && x.BrandId==model.BrandId ).Count();
            if (Number > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
