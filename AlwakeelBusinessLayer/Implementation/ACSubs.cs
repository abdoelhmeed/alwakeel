﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer.APIView;

namespace AlwakeelBusinessLayer.Implementation
{
    public class ACSubs : Base, IACSub
    {
        public int Add(ACSubDomain model)
        {
            tdAccessorySubCategory SCat = new tdAccessorySubCategory()
            {
                AcSNameAr=model.AcSNameAr,
                AcSNameEn=model.AcSNameEn,
                ACatId=model.ACatId
            };
            db.tdAccessorySubCategory.Add(SCat);
            int res=  db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            tdAccessorySubCategory sub = db.tdAccessorySubCategory.Find(Id);
            if (sub != null)
            {
                db.Entry(sub).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return -1;
            }
           
        }

        public List<ACSubDomainView> Get()
        {
            List<ACSubDomainView> model = db.tdAccessorySubCategory.Select(x => new ACSubDomainView
            {
                ACatId=x.ACatId,
                AcSNameAr = x.AcSNameAr,
                AcSNameEn = x.AcSNameEn,
                ACatNameAr=x.tdAccessoryMainCategory.ACatNameAr,
                ACatNameEn=x.tdAccessoryMainCategory.ACatNameEn,
                AcSubId=x.AcSubId

            }).ToList();
            return model;
        }


        public List<ACSubDomainView> GetBYCatId(int ACatId)
        {
            List<ACSubDomainView> model = db.tdAccessorySubCategory.Where(u=> u.ACatId==ACatId).Select(x => new ACSubDomainView
            {
                ACatId = x.ACatId,
                AcSNameAr = x.AcSNameAr,
                AcSNameEn = x.AcSNameEn,
                ACatNameAr = x.tdAccessoryMainCategory.ACatNameAr,
                ACatNameEn = x.tdAccessoryMainCategory.ACatNameEn,
                AcSubId = x.AcSubId

            }).ToList();
            return model;
        }

        public ACSubDomain GetById(int Id)
        {
            ACSubDomain model = new ACSubDomain();
            tdAccessorySubCategory cat = db.tdAccessorySubCategory.Find(Id);
            model.ACatId = cat.ACatId;
            model.AcSubId = cat.AcSubId;
            model.AcSNameAr = cat.AcSNameAr;
            model.AcSNameEn = cat.AcSNameEn;
            return model;
        }

        public int Update(ACSubDomain model)
        {
            tdAccessorySubCategory cat = db.tdAccessorySubCategory.Find(model.AcSubId);
            if (cat !=null)
            {
                cat.ACatId = model.ACatId;
                cat.AcSNameEn = model.AcSNameEn;
                cat.AcSNameAr = model.AcSNameAr;
                db.Entry(cat).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return -1;
            }
          
        }


        //api
        public List<SubAccessories> GetAPISubAccessories(int ACatId)
        {
            List<SubAccessories> model = db.tdAccessorySubCategory.Where(u => u.ACatId == ACatId).Select(x => new SubAccessories
            {
             
               NameAR=x.AcSNameAr,
               NameEn=x.AcSNameEn,
               SubAccessoriesId=x.AcSubId

            }).ToList();
            return model;
        }


    }
}
