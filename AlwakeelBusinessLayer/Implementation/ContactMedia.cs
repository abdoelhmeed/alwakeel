﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using System.Data.Entity;
using AlwakeelRepositoryLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class ContactMedia : Base, IContactMedia
    {
        public int Add(ContactMediaDomain cm)
        {
            td_Contact_Media model = new td_Contact_Media()
            {
                BusinessId= cm.BusinessId,
                contact=cm.contact,
                Type=cm.Type
            };
            db.td_Contact_Media.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Delete(int Id)
        {
            td_Contact_Media model = db.td_Contact_Media.Find(Id);
            if (model != null)
            {
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<ContactMediaDomain> Get()
        {
            List<ContactMediaDomain> model = db.td_Contact_Media.Select(x => new ContactMediaDomain
            {
                BusinessId=x.BusinessId,
                BusinessNameAR=x.td_Business.BusinessNameAR,
                BusinessNameEn=x.td_Business.BusinessNameEn,
                contact=x.contact,
                Id=x.Id,
                Type=x.Type

            }).ToList();
            return model;
        }

        public List<ContactMediaDomain> GetBusinessId(int businessId)
        {
            List<ContactMediaDomain> model = db.td_Contact_Media.Where(u=> u.BusinessId== businessId).Select(x => new ContactMediaDomain
            {
                BusinessId = x.BusinessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                contact = x.contact,
                Id = x.Id,
                Type = x.Type

            }).ToList();
            return model;
        }

        public List<ContactMediaDomain> GetByType(string type)
        {
            List<ContactMediaDomain> model = db.td_Contact_Media.Where(u => u.Type == type).Select(x => new ContactMediaDomain
            {
                BusinessId = x.BusinessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                contact = x.contact,
                Id = x.Id,
                Type = x.Type

            }).ToList();
            return model;
        }

        public int Update(ContactMediaDomain cm)
        {
            td_Contact_Media model = db.td_Contact_Media.Find();
            if (model != null)
            {
                model.BusinessId = cm.BusinessId;
                model.contact = cm.contact;
                model.Type = cm.Type;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
