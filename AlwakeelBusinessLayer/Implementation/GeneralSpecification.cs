﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class GeneralSpecification : Base, IGeneralSpecification
    {
        public int Add(SpecificationDomain model)
        {
            AllModelSpecification specification = new AllModelSpecification()
            {
                Icons = model.Icons,
                NameAR = model.NameAR,
                NameEn = model.NameEn
            };
            db.AllModelSpecification.Add(specification);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            AllModelSpecification specification = db.AllModelSpecification.Find(Id);
            if (specification !=null)
            {
                db.Entry(specification).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<SpecificationDomain> Get()
        {
            List<SpecificationDomain> model = db.AllModelSpecification.Select(x => new SpecificationDomain 
            { 
            Icons=x.Icons,
            NameAR=x.NameAR,
            NameEn=x.NameEn,
            Id=x.Id
            
            }).ToList();
            return model;
        }

        public int Update(SpecificationDomain model)
        {
            AllModelSpecification specification = db.AllModelSpecification.Find(model.Id);
            if (specification != null)
            {
                specification.NameEn = model.NameEn;
                specification.NameAR = model.NameAR;
                specification.Icons = model.Icons;
                db.Entry(specification).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
