﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class ProductImage : Base, IProductImage
    {
        public int Add(ProductImageDomain model)
        {
            td_PImage image = new td_PImage()
            {
                ImageUrl=model.ImageUrl,
                PId=model.PId
            };
            db.td_PImage.Add(image);
            db.SaveChanges();
            return 1;
        }

        public int Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public ProductImageDomain GetById(int Id)
        {
            ProductImageDomain model = new ProductImageDomain();
            td_PImage pImage = db.td_PImage.Find(Id);
            model.ImageUrl = pImage.ImageUrl;
            model.Id = pImage.Id;
            model.PId =(int) pImage.PId;
            return model;
            
        }

        public List<ProductImageDomain> GetByPId(int PId)
        {
            List<ProductImageDomain> model = db.td_PImage.Where(p=> p.PId==PId).Select(x => new ProductImageDomain
            {
                ImageUrl=x.ImageUrl,
                Id=x.Id,
                PId=(int)x.PId
            }).ToList();
            return model;
        }

        public string getProductSingleImage(int PId)
        {
          List<td_PImage> Url=  db.td_PImage.Where(x => x.PId == PId).ToList();
            foreach (var item in Url)
            {
                return item.ImageUrl;
            }
            return "";
        }

        public int Update(ProductImageDomain model)
        {
            td_PImage pImage = db.td_PImage.Find(model.Id); 
            if(pImage !=null)
            {
                pImage.ImageUrl = model.ImageUrl;
                db.Entry(pImage).State = EntityState.Modified;
                db.SaveChanges();
                return 1;

            }
            else
            {
                return 0;
            }
        }
    }
}
