﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class HomePage :Base,IHomePage
    {
        public HomePageStatisticDomain Statistics()
        {
            HomePageStatisticDomain model = new HomePageStatisticDomain();
            model.Products = db.td_ProductsItem.Count();
            model.OurPartners = db.td_Business.Count();
            return model;
        }
    }
}
