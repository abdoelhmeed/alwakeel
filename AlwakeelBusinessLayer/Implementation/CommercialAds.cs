﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class CommercialAds : Base, ICommercialAds
    {
        public int Add(CommercialAdsDomain model)
        {
            td_CommercialAds ads = new td_CommercialAds()
            {
                adsImage=model.adsImage,
                AdsType=model.AdsType,
                EntryData=DateTime.Now,
                TitleAr=model.TitleAr,
                TitleEn=model.TitleEn,
                UrlAds=model.UrlAds,

            };
            db.td_CommercialAds.Add(ads);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_CommercialAds model = db.td_CommercialAds.Find(Id);
            if (model != null)
            {
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<CommercialAdsDomain> Get()
        {
            List<CommercialAdsDomain> model = db.td_CommercialAds.Select(x => new CommercialAdsDomain
            {
                adsImage=x.adsImage,
                AdsType=x.AdsType,
                EntryData=(DateTime)x.EntryData,
                Id=x.Id,
                TitleAr=x.TitleAr,
                TitleEn=x.TitleEn,
                UrlAds=x.UrlAds
            }).ToList();
            return model;
        }

        public List<CommercialAdsDomain> GetByType( string Type)
        {
            List<CommercialAdsDomain> model = db.td_CommercialAds.Where(u=> u.AdsType==Type).Select(x => new CommercialAdsDomain
            {
                adsImage = x.adsImage,
                AdsType = x.AdsType,
                EntryData = (DateTime)x.EntryData,
                Id = x.Id,
                TitleAr = x.TitleAr,
                TitleEn = x.TitleEn,
                UrlAds=x.UrlAds
            }).ToList();
            return model;
        }


    }
}
