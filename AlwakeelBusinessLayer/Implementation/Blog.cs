﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Blog : Base, IBlog
    {
        public int Add(BlogDomain blog)
        {
            td_Blog model = new td_Blog()
            {
                blogBodyAr=blog.blogBodyAr,
                blogBodyEn=blog.blogBodyEn,
                blogDate=DateTime.Now,
                blogIamgeUrl=blog.blogIamgeUrl,
                blogTitleAr=blog.blogTitleAr,
                blogTitleEn=blog.blogTitleEn,
                PublishBy=blog.PublishBy,
                ViewsNuber=0
            };
            db.td_Blog.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_Blog blog = db.td_Blog.Find(Id);
            if (blog != null)
            {
                db.Entry(blog).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<BlogDomain> Get()
        {
            List<BlogDomain> model = db.td_Blog.Select(x=> new BlogDomain
            {
                blogBodyAr=x.blogBodyAr,
                blogBodyEn=x.blogBodyEn,
                blogDate=x.blogDate,
                blogIamgeUrl=x.blogIamgeUrl,
                blogId=x.blogId,
                blogTitleAr=x.blogTitleAr,
                blogTitleEn=x.blogTitleEn,
                PublishBy=x.PublishBy,
                ViewsNuber=x.ViewsNuber
            }).ToList();
            return model;
        }

        public BlogDomain GetById(int Id)
        {
            BlogDomain model = new BlogDomain();
            td_Blog x = db.td_Blog.Find(Id);
            if (x !=null)
            {
                model.blogBodyAr = x.blogBodyAr;
                model.blogBodyEn = x.blogBodyEn;
                model.blogDate = x.blogDate;
                model.blogIamgeUrl = x.blogIamgeUrl;
                model.blogId = x.blogId;
                model.blogTitleAr = x.blogTitleAr;
                model.blogTitleEn = x.blogTitleEn;
                model.PublishBy = x.PublishBy;
                model.ViewsNuber = x.ViewsNuber;
                return model;
            }
            else
            {
                return model;
            }

        }

        public List<BlogDomain> GetByPublish(string Publish)
        {
            List<BlogDomain> model = db.td_Blog.Where(u=> u.PublishBy==Publish).Select(x => new BlogDomain
            {
                blogBodyAr = x.blogBodyAr,
                blogBodyEn = x.blogBodyEn,
                blogDate = x.blogDate,
                blogIamgeUrl = x.blogIamgeUrl,
                blogId = x.blogId,
                blogTitleAr = x.blogTitleAr,
                blogTitleEn = x.blogTitleEn,
                PublishBy = x.PublishBy,
                ViewsNuber = x.ViewsNuber
            }).ToList();
            return model;
        }

        public int Update(BlogDomain blog)
        {
            td_Blog model = db.td_Blog.Find(blog.blogId);
            if (model != null)
            {
                model.blogBodyAr = blog.blogBodyAr;
                model.blogBodyEn = blog.blogBodyEn;
                model.blogDate = blog.blogDate;
                model.blogIamgeUrl = blog.blogIamgeUrl;
                model.blogId = blog.blogId;
                model.blogTitleAr = blog.blogTitleAr;
                model.blogTitleEn = blog.blogTitleEn;
                model.PublishBy = blog.PublishBy;
                model.ViewsNuber = blog.ViewsNuber;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
            
        }

        public BlogDomain ViewByBlpgById(int Id)
        {
            BlogDomain model = new BlogDomain();
            td_Blog x = db.td_Blog.Find(Id);
            if (x != null)
            {
                x.ViewsNuber = x.ViewsNuber + 1;
                db.Entry(x).State = EntityState.Modified;
                db.SaveChanges();
                model.blogBodyAr = x.blogBodyAr;
                model.blogBodyEn = x.blogBodyEn;
                model.blogDate = x.blogDate;
                model.blogIamgeUrl = x.blogIamgeUrl;
                model.blogId = x.blogId;
                model.blogTitleAr = x.blogTitleAr;
                model.blogTitleEn = x.blogTitleEn;
                model.PublishBy = x.PublishBy;
                model.ViewsNuber = x.ViewsNuber;
                return model;
            }
            else
            {
                return model;
            }
        }
    }
}
