﻿using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Category :Base,ICategory
    {
        public int Add(CategoryDomain model)
        {
            td_Category cat = new td_Category() {
                CatId=model.CatId,
                NameAr=model.NameAr,
                NameEn=model.NameEn
            };

            db.td_Category.Add(cat);
           int res= db.SaveChanges();
            return res;
        }

        public List<CategoryDomain> Get()
        {
            List<CategoryDomain> model = db.td_Category.Select(x => new CategoryDomain {
                CatId=x.CatId,
                NameAr=x.NameAr,
                NameEn=x.NameEn

            }).ToList();

            return model;
        }

        public CategoryDomain GetById(int Id)
        {
            td_Category x = db.td_Category.Find(Id);
            CategoryDomain model = new CategoryDomain()
            {
                CatId=x.CatId,
                NameAr=x.NameAr,
                NameEn=x.NameEn

            };
            return model;
        }

        public List<CategoryDomain> GetByIds(int OneId, int TowId)
        {
            List<CategoryDomain> model = db.td_Category.Where(u=> u.CatId>= OneId && u.CatId<= TowId).Select(x => new CategoryDomain
            {
                CatId = x.CatId,
                NameAr = x.NameAr,
                NameEn = x.NameEn

            }).ToList();

            return model;
        }

        public List<CategoryDomain> GetByAll()
        {
            List<CategoryDomain> model = db.td_Category.Select(x => new CategoryDomain
            {
                CatId = x.CatId,
                NameAr = x.NameAr,
                NameEn = x.NameEn

            }).ToList();

            return model;
        }
    }
}
