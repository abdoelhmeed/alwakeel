﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class OrderRentCar : Base, IOrderRentCar
    {
        public int NotAvailableOrder(int Id)
        {
            td_OrderRentCar orderRentCar = db.td_OrderRentCar.Find(Id);
            if (orderRentCar !=null)
            {
                orderRentCar.Status = "Not Available";
                db.Entry(orderRentCar).State = EntityState.Modified;
                db.SaveChanges();
                return (int)orderRentCar.RId;
            }
            else
            {
                return 0;
            }
        }

        public int DoneOrder(int Id)
        {
            td_OrderRentCar orderRentCar = db.td_OrderRentCar.Find(Id);
            if (orderRentCar != null)
            {
                orderRentCar.Status = "Done";
                db.Entry(orderRentCar).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<OrderRentCarDomain> GetNotAvailableOrder(int BId)
        {
            List<OrderRentCarDomain> model = db.td_OrderRentCar.Where(x => x.Status == "Not Available").Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image = c.tdRentCar.ImageURl,
                carInfo = c.tdRentCar.td_Brand.BrandNameEn + " " + c.tdRentCar.td_CarClass.CarClassNameEn + " " + c.tdRentCar.td_CarModel.ModelYears

            }).ToList();
            return model;
            
        }

        public List<OrderRentCarDomain> GetDoneOrder(int BId)
        {

            List<OrderRentCarDomain> model = db.td_OrderRentCar.Where(x => x.Status == "Done" && x.tdRentCar.BusinessId==BId).Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image = c.tdRentCar.ImageURl,
                carInfo = c.tdRentCar.td_Brand.BrandNameEn + " " + c.tdRentCar.td_CarClass.CarClassNameEn + " " + c.tdRentCar.td_CarModel.ModelYears

            }).ToList();
            return model;
        }

        public List<OrderRentCarDomain> GetNewsOrders(int BId)
        {
            List<OrderRentCarDomain> model = db.td_OrderRentCar.Where(x => x.Status == "New" && x.tdRentCar.BusinessId==BId).Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image=c.tdRentCar.ImageURl,
                carInfo=c.tdRentCar.td_Brand.BrandNameEn+" "+ c.tdRentCar.td_CarClass.CarClassNameEn+" "+ c.tdRentCar.td_CarModel.ModelYears


            }).ToList();
            return model;
        }

       
        public List<OrderRentCarDomain> GetOrder() 
        {
            List<OrderRentCarDomain> model = db.td_OrderRentCar.Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image = c.tdRentCar.ImageURl,
                carInfo = c.tdRentCar.td_Brand.BrandNameEn + " " + c.tdRentCar.td_CarClass.CarClassNameEn + " " + c.tdRentCar.td_CarModel.ModelYears


            }).ToList();
            return model;
        }

        public List<OrderRentCarDomain> GetOrderByBId(int BId)
        {
            List<OrderRentCarDomain> model = db.td_OrderRentCar.Where(x=> x.tdRentCar.BusinessId==BId).Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image = c.tdRentCar.ImageURl,
                carInfo = c.tdRentCar.td_Brand.BrandNameEn + " " + c.tdRentCar.td_CarClass.CarClassNameEn + " " + c.tdRentCar.td_CarModel.ModelYears

            }).ToList();
            return model;
        }

        public List<OrderRentCarDomain> GetAllNewsOrders()
        {
            List<OrderRentCarDomain> model = db.td_OrderRentCar.Where(x => x.Status == "New").Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image = c.tdRentCar.ImageURl,
                carInfo = c.tdRentCar.td_Brand.BrandNameEn + " " + c.tdRentCar.td_CarClass.CarClassNameEn + " " + c.tdRentCar.td_CarModel.ModelYears


            }).ToList();
            return model;
        }

        public int NewOrder(OrdesDmonain model)
        {
            td_OrderRentCar order = new td_OrderRentCar()
            {
                EndDate = model.EndDate,
                Phone = model.Phone,
                Name = model.Name,
                StartDate = model.StartDate,
                RId = model.RId,
                Status="New"
            };
            db.td_OrderRentCar.Add(order);
           int res= db.SaveChanges();
            if (res > 0)
            {
                return (int)order.RId;
            }
            else
            {
                return 0;
            }
        }

        //API
        public List<OrderRentCarDomain> GetAPNewsOrders(string UserId)
        {
            List<td_OrderRentCar> get = db.td_OrderRentCar.Where(x=> x.tdRentCar.UserId==UserId).ToList();
            List<OrderRentCarDomain> model = db.td_OrderRentCar.Where(x => x.Status == "New" && x.tdRentCar.UserId == UserId).Select(c => new OrderRentCarDomain
            {
                EndDate = c.EndDate,
                Status = c.Status,
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                RId = c.RId,
                StartDate = c.StartDate,
                Image = c.tdRentCar.ImageURl,
                carInfo = c.tdRentCar.td_Brand.BrandNameEn + " " + c.tdRentCar.td_CarClass.CarClassNameEn + " " + c.tdRentCar.td_CarModel.ModelYears
            }).ToList();
            return model;
        }

    }
}
