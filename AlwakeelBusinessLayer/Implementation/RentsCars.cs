﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using AlwakeelDomainLayer.APIView;

namespace AlwakeelBusinessLayer.Implementation
{
    public class RentsCars : Base, IRentsCars
    {
        public int Add(RentCarDomain model)
        {
            tdRentCar car = new tdRentCar()
            {
                BrandId = model.BrandId,
                Available = true,
                ImageURl = model.ImageURl,
                CarClassId = model.CarClassId,
                BusinessId = model.BusinessId,
                ModelId = model.ModelId

            };
            db.tdRentCar.Add(car);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        public int AddCustomer(RentCarDomain model)
        {
            tdRentCar car = new tdRentCar()
            {
                BrandId = model.BrandId,
                Available = true,
                ImageURl = model.ImageURl,
                CarClassId = model.CarClassId,
                UserId = model.UserId,
                ModelId = model.ModelId
            };
            db.tdRentCar.Add(car);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<RentCarViweDomain> GetBusiness()
        {
            List<RentCarViweDomain> model = db.tdRentCar.Where(x => x.BusinessId != null).OrderByDescending(o => o.Id).Select(x => new RentCarViweDomain
            {
                BusinessId = x.BusinessId,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                Id = x.Id,
                Available = x.Available,
                BrandId = x.BrandId,
                BrandNameAr = x.td_Brand.BrandNameAr,
                BrandNameEn = x.td_Brand.BrandNameEn,
                BrandsLogo = x.td_Brand.BrandsLogo,
                BusinessLocationAr = x.td_Business.BusinessLocationAr,
                BusinessLocationEn = x.td_Business.BusinessLocationEn,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                CarClassNameAr = x.td_CarClass.CarClassNameAr,
                CarClassNameEn = x.td_CarClass.CarClassNameEn,
                ImageURl = x.ImageURl,
                CarClassId = x.CarClassId,
                ModelId = x.ModelId,
                ModelYears = x.td_CarModel.ModelYears,
                UserId = x.UserId
            }).ToList();
            return model;
        }

        public List<RentCarViweCustomerDomain> GetCustomer()
        {
            List<RentCarViweCustomerDomain> model = new List<RentCarViweCustomerDomain>();
            List<tdRentCar> RentCar = db.tdRentCar.Where(x => x.UserId != null).ToList();
            foreach (var x in RentCar)
            {
                RentCarViweCustomerDomain rentCar = new RentCarViweCustomerDomain()
                {

                    Available = x.Available,
                    BrandId = x.BrandId,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    BrandsLogo = x.td_Brand.BrandsLogo,
                    Contact = GetUserPhone(x.UserId),
                    CarClassNameAr = x.td_CarClass.CarClassNameAr,
                    CarClassNameEn = x.td_CarClass.CarClassNameEn,
                    ImageURl = x.ImageURl,
                    CarClassId = x.CarClassId,
                    ModelId = x.ModelId,
                    ModelYears = x.td_CarModel.ModelYears,
                    Name = GetUserPhone(x.UserId),
                    Id = x.Id

                };
                model.Add(rentCar);
            }
            return model;

        }

        public List<RentCarViweDomain> GetByBId(int BId)
        {
            List<RentCarViweDomain> model = db.tdRentCar.Where(b => b.BusinessId == BId).OrderByDescending(o => o.Id).Select(x => new RentCarViweDomain
            {
                BusinessId = x.BusinessId,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                Id = x.Id,
                Available = x.Available,
                BrandId = x.BrandId,
                BrandNameAr = x.td_Brand.BrandNameAr,
                BrandNameEn = x.td_Brand.BrandNameEn,
                BrandsLogo = x.td_Brand.BrandsLogo,
                BusinessLocationAr = x.td_Business.BusinessLocationAr,
                BusinessLocationEn = x.td_Business.BusinessLocationEn,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                CarClassNameAr = x.td_CarClass.CarClassNameAr,
                CarClassNameEn = x.td_CarClass.CarClassNameEn,
                ImageURl = x.ImageURl,
                CarClassId = x.CarClassId,
                ModelId = x.ModelId,
                ModelYears = x.ModelId,
                UserId = x.UserId
            }).ToList();
            return model;
        }

        public RentCarDomain GetByEditId(int Id)
        {
            RentCarDomain car = new RentCarDomain();
            tdRentCar model = db.tdRentCar.Find(Id);
            if (car != null)
            {
                car.BrandId = model.BrandId;
                car.Available = model.Available;
                car.ImageURl = model.ImageURl;
                car.CarClassId = model.CarClassId;
                car.BusinessId = model.BusinessId;
                car.ModelId = model.ModelId;
                car.Id = model.Id;
            }
            return car;
        }

        public int isAvailable(int Id)
        {
            tdRentCar car = db.tdRentCar.Find(Id);
            car.Available = !car.Available;
            db.Entry(car).State = EntityState.Modified;
            db.SaveChanges();
            return 1;
        }

        public int Delete(int Id)
        {
            List<td_OrderRentCar> OList = db.td_OrderRentCar.Where(x => x.RId == Id).ToList();
            foreach (var item in OList)
            {
                db.Entry(item).State = EntityState.Deleted;
                db.SaveChanges();

            }
            tdRentCar car = db.tdRentCar.Find(Id);
            db.Entry(car).State = EntityState.Deleted;
            db.SaveChanges();
            return 1;
        }

        public int Update(RentCarDomain model)
        {
            tdRentCar car = db.tdRentCar.Find(model.Id);
            if (car != null)
            {
                car.BrandId = model.BrandId;

                car.ImageURl = model.ImageURl;
                car.CarClassId = model.CarClassId;
                car.BusinessId = model.BusinessId;
                car.ModelId = model.ModelId;
                car.Id = model.Id;
            }
            db.Entry(car).State = EntityState.Modified;
            db.SaveChanges();
            return 1;
        }

        //API

        public List<RentCarViweApiDomain> GetAPIShearhCars(SearchRentsCars model)
        {
            List<RentCarViweApiDomain> ListCars = new List<RentCarViweApiDomain>();
            List<tdRentCar> RentList = db.tdRentCar.
                Where(o => o.ModelId == model.ModelId && o.Available == true)
                .OrderByDescending(e => e.Id)
                .Skip(model.Skip).Take(model.Take).ToList();

            foreach (var item in RentList)
            {
                if (item.BusinessId != null)
                {
                    RentCarViweApiDomain carsmodel = new RentCarViweApiDomain()
                    {
                        CarInfoAR = item.td_Brand.BrandNameAr + " " + item.td_CarClass.CarClassNameAr + " " + item.td_CarModel.ModelYears,
                        CarInfoEn = item.td_Brand.BrandNameEn + " " + item.td_CarClass.CarClassNameEn + " " + item.td_CarModel.ModelYears,
                        phone = item.td_Business.td_Contact_Media.Where(x => x.Type.Contains("Phohe")).Select(x => x.contact).FirstOrDefault(),
                        ImageURl = item.ImageURl,
                        Logo = item.td_Business.BusinessLogo,
                        OwnerNameAR = item.td_Business.BusinessNameAR,
                        OwnerNameEn = item.td_Business.BusinessNameEn,
                        RId = item.Id
                    };
                    ListCars.Add(carsmodel);
                }
                else
                {
                    RentCarViweApiDomain carsmodel = new RentCarViweApiDomain()
                    {
                        CarInfoAR = item.td_Brand.BrandNameAr + " " + item.td_CarClass.CarClassNameAr + " " + item.td_CarModel.ModelYears,
                        CarInfoEn = item.td_Brand.BrandNameEn + " " + item.td_CarClass.CarClassNameEn + " " + item.td_CarModel.ModelYears,
                        phone = GetUserPhone(item.UserId),
                        ImageURl = item.ImageURl,
                        OwnerNameAR = GetUserName(item.UserId),
                        OwnerNameEn = GetUserName(item.UserId),
                        RId = item.Id
                    };
                    ListCars.Add(carsmodel);
                }
            }
            return ListCars;

        }

        public List<RentCarViweApiDomain> GetRentAPICars(SkipTakeDomain model)
        {
            List<RentCarViweApiDomain> ListCars = new List<RentCarViweApiDomain>();
            List<tdRentCar> RentList = db.tdRentCar.
                Where(o => o.Available == true).OrderByDescending(x => x.Id).Skip(model.Skep).Take(model.Take).ToList();

            foreach (var item in RentList)
            {
                if (item.BusinessId != null)
                {
                    RentCarViweApiDomain carsmodel = new RentCarViweApiDomain()
                    {
                        CarInfoAR = item.td_Brand.BrandNameAr + " " + item.td_CarClass.CarClassNameAr + " " + item.td_CarModel.ModelYears,
                        CarInfoEn = item.td_Brand.BrandNameEn + " " + item.td_CarClass.CarClassNameEn + " " + item.td_CarModel.ModelYears,
                        phone = item.td_Business.td_Contact_Media.Where(x => x.Type.Contains("Phohe")).Select(x => x.contact).FirstOrDefault(),
                        ImageURl = item.ImageURl,
                        Logo = item.td_Business.BusinessLogo,
                        OwnerNameAR = item.td_Business.BusinessNameAR,
                        OwnerNameEn = item.td_Business.BusinessNameEn,
                        RId = item.Id
                    };
                    ListCars.Add(carsmodel);
                }
                else
                {
                    RentCarViweApiDomain carsmodel = new RentCarViweApiDomain()
                    {
                        CarInfoAR = item.td_Brand.BrandNameAr + " " + item.td_CarClass.CarClassNameAr + " " + item.td_CarModel.ModelYears,
                        CarInfoEn = item.td_Brand.BrandNameEn + " " + item.td_CarClass.CarClassNameEn + " " + item.td_CarModel.ModelYears,
                        phone = GetUserPhone(item.UserId),
                        ImageURl = item.ImageURl,
                        OwnerNameAR = GetUserName(item.UserId),
                        OwnerNameEn = GetUserName(item.UserId),
                        RId = item.Id
                    };
                    ListCars.Add(carsmodel);
                }
            }
            return ListCars;

        }



        public List<RentCarViweApiDomain> GetRentCarsCustomerById(string UserId)
        {
            List<RentCarViweApiDomain> model = new List<RentCarViweApiDomain>();
            List<tdRentCar> rentCars = db.tdRentCar.Where(o => o.UserId == UserId).ToList();


            foreach (var item in rentCars)
            {
                RentCarViweApiDomain modelitem =new RentCarViweApiDomain();

                modelitem.CarInfoAR = item.td_Brand.BrandNameAr + " " + item.td_CarClass.CarClassNameAr + " " + item.td_CarModel.ModelYears;
                modelitem.CarInfoEn = item.td_Brand.BrandNameEn + " " + item.td_CarClass.CarClassNameEn + " " + item.td_CarModel.ModelYears;
                modelitem.phone = GetUserPhone(UserId);
                modelitem.ImageURl = item.ImageURl;
                modelitem.OwnerNameAR = GetUserName(UserId);
                modelitem.OwnerNameEn = GetUserName(UserId);
                modelitem.RId = item.Id;
                model.Add(modelitem);
            }

            return model;
        }

        public string GetUserPhone(string userId)
        {
            UserLogin userInfo = db.UserLogin.Find(userId);
            if (userInfo.Utype == "e")
            {
                return userInfo.UEmail;
            }
            else
            {
                return userInfo.UPhone;
            }
        }

        public string GetUserName(string userId)
        {
            UserLogin userInfo = db.UserLogin.Find(userId);
            return userInfo.FullName;
        }


    }
}
