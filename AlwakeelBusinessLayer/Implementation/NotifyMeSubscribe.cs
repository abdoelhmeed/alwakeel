﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;

namespace AlwakeelBusinessLayer.Implementation
{
    public class NotifyMeSubscribe : Base, INotifyMeSubscribe
    {
        public int AddNotifyMe(NotifyMeDomain model)
        {
            throw new NotImplementedException();
        }

        public int AddSubscribe(SubscribeDomain model)
        {
            td_NotifyMe _NotifyMe = new td_NotifyMe()
            {
                Cemail = model.Email,
                EntryDate = DateTime.Now,
                PTitle = "title",
                PTitleOne = "title",
                Stutus = "Sub"
            };
            db.td_NotifyMe.Add(_NotifyMe);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
