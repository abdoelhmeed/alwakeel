﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer.APIView;

namespace AlwakeelBusinessLayer.Implementation
{
    public class ACMains : Base, IACMain
    {
        public int Add(ACMainDomain model)
        {
            tdAccessoryMainCategory Mcat = new tdAccessoryMainCategory()
            {
                ACatIcon=model.ACatIcon,
                ACatNameAr=model.ACatNameAr,
                ACatNameEn=model.ACatNameEn

            };
            db.tdAccessoryMainCategory.Add(Mcat);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            tdAccessoryMainCategory Mcat = db.tdAccessoryMainCategory.Find(Id);
            if (Mcat != null)
            {
                db.Entry(Mcat).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public List<ACMainDomain> Get()
        {
            List<ACMainDomain> model = db.tdAccessoryMainCategory.Select(x => new ACMainDomain
            {
                ACatIcon=x.ACatIcon,
                ACatId=x.ACatId,
                ACatNameAr=x.ACatNameAr,
                ACatNameEn=x.ACatNameEn
            }).ToList();
            return model;
        }

        public ACMainDomain GetById(int Id)
        {
            throw new NotImplementedException();
        }

        public int Update(ACMainDomain model)
        {
            throw new NotImplementedException();
        }

        public List<MainAccessories> GetAPIMainAccessories()
        {
            List<MainAccessories> model = db.tdAccessoryMainCategory.Select(x => new MainAccessories
            {
                Icos = x.ACatIcon,
                NameAR = x.ACatNameAr,
                NameEn = x.ACatNameEn,
                MainAccessoriesId=x.ACatId
            }).ToList();
            return model;
        }

        public List<ManiWithSubAccessories> GetManiWithSubAccessories()
        {
            List<ManiWithSubAccessories> model = db.tdAccessoryMainCategory
                .Select(x => new ManiWithSubAccessories
                {
                    Icos = x.ACatIcon,
                    NameAR = x.ACatNameAr,
                    NameEn = x.ACatNameEn,
                    MainAccessoriesId = x.ACatId,
                    SubList = x.tdAccessorySubCategory.Select(s => new SubAccessories {
                    NameAR=s.AcSNameAr,
                    NameEn=s.AcSNameEn,
                    SubAccessoriesId=s.AcSubId
                    
                    }).ToList()

                }).ToList();

            return model;
        }
    }
}
