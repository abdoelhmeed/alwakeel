﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer.Accessory;
using AlwakeelDomainLayer.APIView;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Accessory : Base, IAccessory
    {
        AccessoryDetails accessoryDetails = new AccessoryDetails();
        ProductImage productImage = new ProductImage();
        public int Add(AccessoryDomain model)
        {
            td_ProductsItem accessory = new td_ProductsItem()
            {
                businessId = model.businessId,
                CatId = model.CatId,
                AccessoryTitleEN = model.AccessoryTitleEN,
                AccessoryTitleAR = model.AccessoryTitleAR,
                AcSubId = model.AcSubId,
                AccessoryForAll = model.AccessoryForAll,
                DescribeAR = model.DescribeAR,
                DescribeEN = model.DescribeEN,
                AdPrice = model.AdPrice,
                AdStatus = "New",
                isNew = model.isNew,
                AdEntryDate = DateTime.Now,
                AdendDate =model.EndDate,
                Active = true,
                love = 0,
                Views = 0

            };
            db.td_ProductsItem.Add(accessory);
            int res = db.SaveChanges();
            if (res > 0)
            {
                AddAccessoryImages(model, accessory);
                return accessory.PId;
            }
            else
            {
                return 0;
            }

        }
        public int Delete(int Id)
        {
            td_ProductsItem _ProductsItem = db.td_ProductsItem.Find(Id);
            if (_ProductsItem != null)
            {
                List<td_AccessoryDetails> AccessoryList = db.td_AccessoryDetails.Where(x => x.PId == Id).ToList();
                List<td_PImage> Images = db.td_PImage.Where(x => x.PId == Id).ToList();
                List<Complain> complains = db.Complain.Where(x => x.PId == Id).ToList();
                List<td_Features> Features = db.td_Features.Where(x => x.PId == Id).ToList();
                foreach (var item in AccessoryList)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }
                foreach (var item in Features)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }
                foreach (var item in Images)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                foreach (var item in complains)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }
                db.Entry(_ProductsItem).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }

        }
        public List<AccessoryViewDomain> Get()
        {
            List<AccessoryViewDomain> model = db.td_ProductsItem
                .Where(u => u.CatId == 4 || u.CatId == 5 && u.businessId != null)
                .Select(x => new AccessoryViewDomain
                {
                    AMCatIcon = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatIcon,
                    businessId = x.businessId,
                    AcSubId = x.AcSubId,
                    AcSNameAr = x.tdAccessorySubCategory.AcSNameAr,
                    AccessoryForAll = x.AccessoryForAll,
                    AccessoryTitleAR = x.AccessoryTitleAR,
                    AccessoryTitleEN = x.AccessoryTitleEN,
                    AdPrice = x.AdPrice,
                    AMCatNameAr = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameAr,
                    AMCatNameEn = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn,
                    ASNameEn = x.tdAccessorySubCategory.AcSNameAr,
                    AdStatus = x.AdStatus,
                    CatId = x.CatId,
                    CatNameAr = x.td_Category.NameAr,
                    CatNameEn = x.td_Category.NameEn,
                    DescribeEN = x.DescribeEN,
                    DescribeAR = x.DescribeAR,
                    PId = x.PId,
                    Images = x.td_PImage.Select(i => i.ImageUrl).ToList(),
                    isNew = x.isNew,
                    accessoryDetail = x.td_AccessoryDetails.Select(a => new AccessoryDetailsViewDomain
                    {
                        ModelYears = a.td_CarModel.ModelYears,
                        BrandNameAR = a.td_CarModel.td_CarClass.td_Brand.BrandNameAr,
                        BrandNameEN = a.td_CarModel.td_CarClass.td_Brand.BrandNameEn,
                        PId = a.PId,
                        ClassNameAR = a.td_CarModel.td_CarClass.CarClassNameAr,
                        ClassNameEN = a.td_CarModel.td_CarClass.CarClassNameEn,
                        Id = a.Id,
                        ModelId = a.ModelId

                    }).ToList()
                })
                .ToList();
            return model;
        }
        public List<AccessoryViewDomain> GetByBId(int PId)
        {
            List<AccessoryViewDomain> model = db.td_ProductsItem
               .Where(u => u.CatId >= 4 && u.businessId == PId)
               .Select(x => new AccessoryViewDomain
               {
                   AMCatIcon = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatIcon,
                   businessId = x.businessId,
                   AcSubId = x.AcSubId,
                   AcSNameAr = x.tdAccessorySubCategory.AcSNameAr,
                   AccessoryForAll = x.AccessoryForAll,
                   AccessoryTitleAR = x.AccessoryTitleAR,
                   AccessoryTitleEN = x.AccessoryTitleEN,
                   AdPrice = x.AdPrice,
                   AMCatNameAr = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameAr,
                   AMCatNameEn = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn,
                   ASNameEn = x.tdAccessorySubCategory.AcSNameAr,
                   AdStatus = x.AdStatus,
                   CatId = x.CatId,
                   CatNameAr = x.td_Category.NameAr,
                   CatNameEn = x.td_Category.NameEn,
                   DescribeAR = x.DescribeAR,
                   DescribeEN = x.DescribeEN,
                   PId = x.PId,
                   Images = x.td_PImage.Select(i => i.ImageUrl).ToList(),
                   isNew = x.isNew,
                   EndDate=x.AdEntryDate,
                   accessoryDetail = x.td_AccessoryDetails.Select(a => new AccessoryDetailsViewDomain
                   {
                       ModelYears = a.td_CarModel.ModelYears,
                       BrandNameAR = a.td_CarModel.td_CarClass.td_Brand.BrandNameAr,
                       BrandNameEN = a.td_CarModel.td_CarClass.td_Brand.BrandNameEn,
                       PId = a.PId,
                       ClassNameAR = a.td_CarModel.td_CarClass.CarClassNameAr,
                       ClassNameEN = a.td_CarModel.td_CarClass.CarClassNameEn,
                       Id = a.Id,
                       ModelId = a.ModelId

                   }).ToList()
               }).OrderByDescending(x => x.PId).ToList();
 
            return model;
        }
        public AccessoryEditDomain GetByIdToEdit(int Id)
        {
            td_ProductsItem x = db.td_ProductsItem.Find(Id);
            AccessoryEditDomain model = new AccessoryEditDomain()
            {
                businessId = x.businessId,
                AcSubId = x.AcSubId,
                AccessoryForAll = x.AccessoryForAll,
                AccessoryTitleEN = x.AccessoryTitleEN,
                AccessoryTitleAR = x.AccessoryTitleAR,
                AdPrice = x.AdPrice,
                AdStatus = x.AdStatus,
                CatId = x.CatId,
                DescribeAR = x.DescribeAR,
                DescribeEN = x.DescribeEN,
                PId = x.PId,
                EndDate = x.AdEntryDate,
                isNew=x.isNew

            };
            return model;

        }
        public AccessoryViewDomain GetById(int Id)
        {
            td_ProductsItem x = db.td_ProductsItem.Find(Id);
            AccessoryViewDomain model = new AccessoryViewDomain()
            {
                AMCatIcon = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatIcon,
                businessId = x.businessId,
                AcSubId = x.AcSubId,
                AcSNameAr = x.tdAccessorySubCategory.AcSNameAr,
                AccessoryForAll = x.AccessoryForAll,
                AccessoryTitleAR = x.AccessoryTitleAR,
                AccessoryTitleEN = x.AccessoryTitleEN,
                AdPrice = x.AdPrice,
                AMCatNameAr = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameAr,
                AMCatNameEn = x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn,
                ASNameEn = x.tdAccessorySubCategory.AcSNameAr,
                AdStatus = x.AdStatus,
                CatId = x.CatId,
                CatNameAr = x.td_Category.NameAr,
                CatNameEn = x.td_Category.NameEn,
                DescribeAR = x.DescribeAR,
                DescribeEN = x.DescribeEN,
                PId = x.PId,
                Images = x.td_PImage.Select(i => i.ImageUrl).ToList(),
                isNew = x.isNew,
                EndDate = x.AdEntryDate,
                accessoryDetail = x.td_AccessoryDetails.Select(a => new AccessoryDetailsViewDomain
                {
                    ModelYears = a.td_CarModel.ModelYears,
                    BrandNameAR = a.td_CarModel.td_CarClass.td_Brand.BrandNameAr,
                    BrandNameEN = a.td_CarModel.td_CarClass.td_Brand.BrandNameEn,
                    PId = a.PId,
                    ClassNameAR = a.td_CarModel.td_CarClass.CarClassNameAr,
                    ClassNameEN = a.td_CarModel.td_CarClass.CarClassNameEn,
                    Id = a.Id,
                    ModelId = a.ModelId

                }).ToList()

            };

            return model;

        }
        public int Update(AccessoryEditDomain model)
        {

            td_ProductsItem accessory = db.td_ProductsItem.Find(model.PId);
            if (accessory != null)
            {
                accessory.CatId = model.CatId;
                accessory.AccessoryTitleAR = model.AccessoryTitleAR;
                accessory.AccessoryTitleEN = model.AccessoryTitleEN;
                accessory.AcSubId = model.AcSubId;
                accessory.AccessoryForAll = model.AccessoryForAll;
                accessory.DescribeEN = model.DescribeEN;
                accessory.DescribeAR = model.DescribeAR;
                accessory.AdPrice = model.AdPrice;
                accessory.AdStatus = "1";
                accessory.isNew = model.isNew;
                db.Entry(accessory).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }

        }
        private void AddAccessoryImages(AccessoryDomain model, td_ProductsItem accessory)
        {
            foreach (var item in model.Images)
            {
                ProductImageDomain PImage = new ProductImageDomain() { Id = 0, PId = accessory.PId, ImageUrl = item };
                productImage.Add(PImage);
            }
        }
        /// API
        public List<AccessoryAutoPartList> GetAPIAccessory(int CatId, int take)
        {
            List<AccessoryAutoPartList> mode = db.td_ProductsItem.Where(x => x.CatId == CatId).OrderByDescending(o => o.PId).Take(take)
                .Select(a => new AccessoryAutoPartList
                {
                    AccessoryForAll = a.AccessoryForAll,
                    AccessoryTitleAR = a.AccessoryTitleAR,
                    AccessoryTitleEN = a.AccessoryTitleEN,
                    AdPrice = a.AdPrice,
                    Category = a.td_Category.NameEn,
                    Images = a.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                    NewAR = (bool)a.isNew ? "جديد" : "مستعمل",
                    NewEn = (bool)a.isNew ? "New" : "Used",
                    PId = a.PId,
                    love=a.love,
                    Views=a.Views
                }).ToList();

            return mode;
        }

    }
}
