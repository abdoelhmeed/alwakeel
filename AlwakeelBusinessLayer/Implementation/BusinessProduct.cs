﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class BusinessProduct : Base, IBusinessProduct
    {
        public int Add(BusinessProductDomain product)
        {
            td_BusinessProduct bp = new td_BusinessProduct()
            {
                BusinessId = product.BusinessId,
                PimageUrl2 = product.PimageUrl,
                PNameAR = product.PNameAR,
                PNameEN = product.PNameEN,
                ProductType = product.ProductType,
                Price = product.Price,
                DescribeEN = product.DescribeEN,
                DescribeAR = product.DescribeAR
            };
            db.td_BusinessProduct.Add(bp);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int pid)
        {
            try
            {
                td_BusinessProduct product = db.td_BusinessProduct.Find(pid);
                db.Entry(product).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {

                return -1;
            }

        }

        public List<BusinessProductDomain> Get()
        {
            List<BusinessProductDomain> model = db.td_BusinessProduct.Select(x => new BusinessProductDomain
            {
                BusinessId = x.BusinessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                id = x.id,
                PimageUrl = x.PimageUrl2,
                PNameAR = x.PNameAR,
                PNameEN = x.PNameEN,
                ProductType = x.ProductType,
                Price = x.Price,
                DescribeEN = x.DescribeEN,
                DescribeAR = x.DescribeAR

            }).ToList();

            return model;
        }

        public List<BusinessProductDomain> GetByBusinessId(int businessId)
        {

            List<BusinessProductDomain> model = db.td_BusinessProduct.Where(b => b.BusinessId == businessId).Select(x => new BusinessProductDomain
            {
                BusinessId = x.BusinessId,
                BusinessNameAR = x.td_Business.BusinessNameAR,
                BusinessNameEn = x.td_Business.BusinessNameEn,
                id = x.id,
                PimageUrl = x.PimageUrl2,
                PNameAR = x.PNameAR,
                PNameEN = x.PNameEN,
                ProductType = x.ProductType,
                Price = x.Price,
                DescribeEN = x.DescribeEN,
                DescribeAR = x.DescribeAR

            }).ToList();

            return model;
        }

        public BusinessProductDomain GetByPid(int pid)
        {
            BusinessProductDomain model = new BusinessProductDomain();
            td_BusinessProduct x = db.td_BusinessProduct.Find(pid);
            model.BusinessId = x.BusinessId;
            model.BusinessNameAR = x.td_Business.BusinessNameAR;
            model.BusinessNameEn = x.td_Business.BusinessNameEn;
            model.id = x.id;
            model.PimageUrl = x.PimageUrl2;
            model.PNameAR = x.PNameAR;
            model.PNameEN = x.PNameEN;
            model.Price = x.Price;
            model.DescribeEN = x.DescribeEN;
            model.DescribeAR = x.DescribeAR;
            model.DescribeAR = x.DescribeAR;
            model.ProductType = x.ProductType;
            return model;
        }

        public int Update(BusinessProductDomain product)
        {
            td_BusinessProduct model = db.td_BusinessProduct.Find(product.id);
            if (model != null)
            {
                model.PNameAR = product.PNameAR;
                model.PNameEN = product.PNameEN;
                model.Price = product.Price;
                model.DescribeEN = product.DescribeEN;
                model.DescribeAR = product.DescribeAR;
                model.DescribeAR = product.DescribeAR;
                model.ProductType = product.ProductType;
                model.PimageUrl2 = product.PimageUrl;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
