﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class BayNow : Base, IBayNow
    {
        public int Add(BayNowDomain model)
        {
            td_BayNow bay = new td_BayNow()
            {
                Available = true,
                BusinessId = model.BusinessId,
                EmpName = model.EmpName,
                ComName = model.DescriptionOrMote,
                PhoneNumber = model.contact,
                Product = model.Product,
                Quantity = model.Quantity,
            };

            db.td_BayNow.Add(bay);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }


        public int Done(int Id)
        {
            td_BayNow bay = db.td_BayNow.Find(Id);
            if (bay != null)
            {
                bay.Available = false;
                db.Entry(bay).State = EntityState.Modified;
                int res = db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }


        }

        public List<BayNowViewDomain> Get()
        {
            List<BayNowViewDomain> model = db.td_BayNow.Select(x => new BayNowViewDomain
            {
                BusinessId = (int)x.BusinessId,
                BusinessName = x.td_Business.BusinessNameEn,
                contact = x.PhoneNumber,
                DescriptionOrMote = x.ComName,
                Done = (bool)x.Available == true ? "Done" : "New",
                EmpName = x.EmpName,
                Id = x.Id,
                Product = x.Product,
                Quantity = (int)x.Quantity
            }).OrderByDescending(o => o.Id).ToList();
            return model;
        }

        public List<BayNowViewDomain> GetDone()
        {
            List<BayNowViewDomain> model = db.td_BayNow.Where(e=> e.Available == false).Select(x => new BayNowViewDomain
            {
                BusinessId = (int)x.BusinessId,
                BusinessName = x.td_Business.BusinessNameEn,
                contact = x.PhoneNumber,
                DescriptionOrMote = x.ComName,
                Done = (bool)x.Available == true ? "Done" : "New",
                EmpName = x.EmpName,
                Id = x.Id,
                Product = x.Product,
                Quantity = (int)x.Quantity
            }).OrderByDescending(o => o.Id).ToList();
            return model;
        }

        public List<BayNowViewDomain> GetNew()
        {
            List<BayNowViewDomain> model = db.td_BayNow.Where(e => e.Available == true).Select(x => new BayNowViewDomain
            {
                BusinessId = (int)x.BusinessId,
                BusinessName = x.td_Business.BusinessNameEn,
                contact = x.PhoneNumber,
                DescriptionOrMote = x.ComName,
                Done = (bool)x.Available == true ? "Done" : "New",
                EmpName = x.EmpName,
                Id = x.Id,
                Product = x.Product,
                Quantity = (int)x.Quantity
            }).OrderByDescending(o => o.Id).ToList();
            return model;
        }

        public List<BayNowViewDomain> GetByBuId(int BId)
        {
            List<BayNowViewDomain> model = db.td_BayNow.Where(e=> e.BusinessId==BId).Select(x => new BayNowViewDomain
            {
                BusinessId = (int)x.BusinessId,
                BusinessName = x.td_Business.BusinessNameEn,
                contact = x.PhoneNumber,
                DescriptionOrMote = x.ComName,
                Done = (bool)x.Available == true ? "Done" : "New",
                EmpName = x.EmpName,
                Id = x.Id,
                Product = x.Product,
                Quantity = (int)x.Quantity
            }).OrderByDescending(o=> o.Id).ToList();
            return model;
        }

        public List<BayNowViewDomain> GetDoneByBuId(int BId)
        {
            List<BayNowViewDomain> model = db.td_BayNow.Where(e => e.BusinessId == BId && e.Available==false).Select(x => new BayNowViewDomain
            {
                BusinessId = (int)x.BusinessId,
                BusinessName = x.td_Business.BusinessNameEn,
                contact = x.PhoneNumber,
                DescriptionOrMote = x.ComName,
                Done = (bool)x.Available == true ? "Done" : "New",
                EmpName = x.EmpName,
                Id = x.Id,
                Product = x.Product,
                Quantity = (int)x.Quantity
            }).OrderByDescending(o => o.Id).ToList();
            return model;
        }

        public List<BayNowViewDomain> GetNewByBuId(int BId)
        {
            List<BayNowViewDomain> model = db.td_BayNow.Where(e => e.BusinessId == BId && e.Available == true).Select(x => new BayNowViewDomain
            {
                BusinessId = (int)x.BusinessId,
                BusinessName = x.td_Business.BusinessNameEn,
                contact = x.PhoneNumber,
                DescriptionOrMote = x.ComName,
                Done = (bool)x.Available == true ? "Done" : "New",
                EmpName = x.EmpName,
                Id = x.Id,
                Product = x.Product,
                Quantity = (int)x.Quantity
            }).OrderByDescending(o => o.Id).ToList();
            return model;
        }
    }
}
