﻿using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
   public class Region :Base,IRegion
    {
        public int Add(RegionDomain model)
        {
            td_Regions region = new td_Regions();
            region.CityId = model.CityId;
            region.RegionNameAr = model.RegionNameAr;
            region.RegionNameEn = model.RegionNameEn;
            db.td_Regions.Add(region);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Delete(int Id)
        {
            try
            {
                td_Regions model = db.td_Regions.Find(Id);
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }

        public List<RegionDomain> Get()
        {
            List<RegionDomain> model = db.td_Regions.Select(x => new RegionDomain {
                CityId = x.CityId,
                RegionId = x.RegionId,
                RegionNameAr = x.RegionNameAr,
                RegionNameEn = x.RegionNameEn,
                CityNameAr=x.td_City.CityNameAr,
                CityNameEn=x.td_City.CityNameEn,
                StateId=x.td_City.td_State.StateId,
                StateNameAr=x.td_City.td_State.StateNameAr,
                StateNameEn=x.td_City.td_State.StateNameEn

               
            }).ToList();
            return model;
        }

        public List<RegionDomain> GetByCityId(int CityId)
        {
            List<RegionDomain> model = db.td_Regions.Where(u=> u.CityId==CityId).Select(x => new RegionDomain
            {
                CityId = x.CityId,
                RegionId = x.RegionId,
                RegionNameAr = x.RegionNameAr,
                RegionNameEn = x.RegionNameEn,
                CityNameAr = x.td_City.CityNameAr,
                CityNameEn = x.td_City.CityNameEn,
                StateId = x.td_City.td_State.StateId,
                StateNameAr = x.td_City.td_State.StateNameAr,
                StateNameEn = x.td_City.td_State.StateNameEn
            }).ToList();
            return model;
        }

        public RegionDomain GetById(int Id)
        {
            RegionDomain model = new RegionDomain();
            td_Regions x = db.td_Regions.Find(Id);
            if (model != null)
            {
                model.CityId = x.CityId;
                model.RegionId = x.RegionId;
                model.RegionNameAr = x.RegionNameAr;
                model.RegionNameEn = x.RegionNameEn;
                return model;
            }
            else
            {
                return model;
            }
        }

        public int Update(RegionDomain model)
        {

            td_Regions region =db.td_Regions.Find(model.RegionId);
            if (region  != null)
            {
                region.CityId = model.CityId;
                region.RegionNameAr = model.RegionNameAr;
                region.RegionNameEn = model.RegionNameEn;
                db.Entry(region).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            } else
            {
                return 0;
            }

        }
    }
}
