﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class FeaturesVehicle : Base, IFeaturesVehicle
    {
        public int Delete(int FId)
        {
            td_Features frature = db.td_Features.Find(FId);
            if (frature != null)
            {
                db.Entry(frature).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public PFVVeiwDomain GetById(int Id)
        {
            throw new NotImplementedException();
        }

        public List<PFVVeiwDomain> GetByPId(int PId)
        {
            List<PFVVeiwDomain> model = db.td_Features.Where(u => u.PId == PId).Select(x => new PFVVeiwDomain
            {
                PId=x.PId,
                FId=x.FId,
                SpeAr=x.td_carSpecifications.SpeAr,
                SpeEn=x.td_carSpecifications.SpeEn,
                SpeId=x.SpeId,
                Icons=x.td_carSpecifications.SpeIcon
                
            }).ToList();
            return model;
        }
    }
}
