﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelDomainLayer;
using AlwakeelRepositoryLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Motorcycle : Base, IMotorcycle
    {
        public int Add(MotorcycleDomain motorcycle,List<string> Images)
        {
            td_ProductsItem product = new td_ProductsItem()
            {
                CarClassId = motorcycle.CarClassId,
                CatId = 3,
                ModelId = motorcycle.ModelId,
                CColors = motorcycle.CColors,
                AdPrice = motorcycle.AdPrice,
                AdEntryDate = DateTime.Now,
                CEngineCapacityCC = motorcycle.CEngineCapacityCC,
                businessId = motorcycle.businessId,
                CMileageKM = motorcycle.CMileageKM,
                BrandId = motorcycle.BrandId,
                DescribeAR = motorcycle.DescribeAR,
                DescribeEN = motorcycle.DescribeEN,
                isNew = motorcycle.isNew,
                CTransmission = 0,
                AdendDate = DateTime.Now.AddYears(1),
                Active = true,
                love = 0,
                Views = 0

            };
            db.td_ProductsItem.Add(product);
            int res = db.SaveChanges();
            if (res > 0)
            {
                List<td_PImage> pIList = new List<td_PImage>();
                List<td_Features> FList = new List<td_Features>();
                foreach (var item in Images)
                {
                    td_PImage pI = new td_PImage()
                    {
                        ImageUrl = item,
                        PId = product.PId
                    };
                    pIList.Add(pI);
                }
                db.td_PImage.AddRange(pIList);
                db.SaveChanges();

                foreach (var item in motorcycle.Features)
                {
                    td_Features feature = new td_Features()
                    {
                        PId = product.PId,
                        SpeId = item
                    };
                    FList.Add(feature);
                }

                db.td_Features.AddRange(FList);
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public List<MotorcycleViweDomain> Get()
        {
            List<MotorcycleViweDomain> model = db.td_ProductsItem.Where(b => b.CatId == 3 && b.businessId != null && b.Active == true).OrderByDescending(o => o.PId)
                .Select(x => new MotorcycleViweDomain
                {
                    PId = x.PId,
                    BusinessName = x.td_Business.BusinessNameAR + " " + x.td_Business.BusinessNameEn,
                    Category = x.td_Category.NameEn,
                    CategoryAr = x.td_Category.NameAr,
                    ColorId = (int)x.CColors,
                    ColorImage = x.tdColors.colorIcon,
                    ColorNameAr = x.tdColors.colorIdNameAR,
                    ColorNameEn = x.tdColors.colorIdNameEN,
                    EngineCapacityCC = (int)x.CEngineCapacityCC,
                    MileageKM = (int)x.CMileageKM,
                    New = x.isNew,
                    Price = (int)x.AdPrice,
                    VehicleInfoAR = x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                    VehicleInfoEn = x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears
                }).ToList();
            return model;
        }
        public List<MotorcycleViweDomain> GetByBId(int BId)
        {
            List<MotorcycleViweDomain> model = db.td_ProductsItem.Where(b => b.CatId == 3 && b.businessId == BId && b.Active == true).OrderByDescending(o => o.PId)
               .Select(x => new MotorcycleViweDomain
               {
                   PId = x.PId,
                   BusinessName = x.td_Business.BusinessNameAR + " " + x.td_Business.BusinessNameEn,
                   Category = x.td_Category.NameEn,
                   CategoryAr = x.td_Category.NameAr,
                   ColorId = (int)x.CColors,
                   ColorImage = x.tdColors.colorIcon,
                   ColorNameAr = x.tdColors.colorIdNameAR,
                   ColorNameEn = x.tdColors.colorIdNameEN,
                   EngineCapacityCC = (int)x.CEngineCapacityCC,
                   MileageKM = (int)x.CMileageKM,
                   New = x.isNew,
                   Price = (int)x.AdPrice,
                   VehicleInfoAR = x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                   VehicleInfoEn = x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears
               }).ToList();
            return model;
        }
        public MotorcycleDomain GetByPId(int PId)
        {
            MotorcycleDomain model = new MotorcycleDomain();
            td_ProductsItem product = db.td_ProductsItem.Find(PId);
            if (product !=null)
            {
                model.CarClassId = (int)product.CarClassId;
                model.CatId = 3;
                model.ModelId = (int)product.ModelId;
                model.CColors = (int)product.CColors;
                model.AdPrice = (decimal)product.AdPrice;
                model.EndDate = product.AdEntryDate;
                model.CEngineCapacityCC = (int)product.CEngineCapacityCC;
                model.businessId = (int)product.businessId;
                model.CMileageKM = (int)product.CMileageKM;
                model.BrandId = (int)product.BrandId;
                model.DescribeAR = product.DescribeAR;
                model.DescribeEN = product.DescribeEN;
                model.isNew = (bool)product.isNew;
            }

            return model;
        }
        public int Update(MotorcycleDomain motorcycle)
        {

            td_ProductsItem product = db.td_ProductsItem.Find(motorcycle.PId);
            if(product !=null)
            {
                product.CarClassId = motorcycle.CarClassId;
                product.ModelId = motorcycle.ModelId;
                product.CColors = motorcycle.CColors;
                product.AdPrice = motorcycle.AdPrice;
                product.CEngineCapacityCC = motorcycle.CEngineCapacityCC;
                product.businessId = motorcycle.businessId;
                product.CMileageKM = motorcycle.CMileageKM;
                product.BrandId = motorcycle.BrandId;
                product.DescribeAR = motorcycle.DescribeAR;
                product.DescribeEN = motorcycle.DescribeEN;
                product.isNew = motorcycle.isNew;
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int delete(int PId)
        {


            td_ProductsItem ProductsItem = db.td_ProductsItem.Find(PId);
            if (ProductsItem != null)
            {
                List<td_PImage> images = db.td_PImage.Where(x => x.PId == PId).ToList();
                foreach (var item in images)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                List<td_Features> Features = db.td_Features.Where(x => x.PId == PId).ToList();
                foreach (var item in Features)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                List<Complain> complain = db.Complain.Where(x => x.PId == PId).ToList();
                foreach (var item in complain)
                {
                    db.Entry(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                db.Entry(ProductsItem).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public MotorcycleViweDomain GetDetailsByPId(int PId)
        {
            MotorcycleViweDomain model = db.td_ProductsItem.Where(w=> w.PId==PId).OrderByDescending(o => o.PId)
               .Select(x => new MotorcycleViweDomain
               {
                   PId = x.PId,
                   BusinessName = x.td_Business.BusinessNameAR + " " + x.td_Business.BusinessNameEn,
                   Category = x.td_Category.NameEn,
                   CategoryAr = x.td_Category.NameAr,
                   ColorId = (int)x.CColors,
                   ColorImage = x.tdColors.colorIcon,
                   ColorNameAr = x.tdColors.colorIdNameAR,
                   ColorNameEn = x.tdColors.colorIdNameEN,
                   EngineCapacityCC = (int)x.CEngineCapacityCC,
                   MileageKM = (int)x.CMileageKM,
                   New = x.isNew,
                   Price = (int)x.AdPrice,
                   VehicleInfoAR = x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                   VehicleInfoEn = x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears
               }).SingleOrDefault();
            return model;
        }
    }
}
