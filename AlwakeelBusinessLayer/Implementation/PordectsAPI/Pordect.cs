﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces.PordectsAPI;
using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelRepositoryLayer;
using System.Data.Entity;
using AlwakeelDomainLayer;

namespace AlwakeelBusinessLayer.Implementation.PordectsAPI
{
    public class Pordect : Base, IPordect
    {
        public List<PordectList> pordectLists(Filter filter)
        {
            List<PordectList> model;

            if (filter == null)
            {
                model = db.td_ProductsItem.Where(w => w.Active == true && w.AdEntryDate >=DateTime.Now)
                       .OrderByDescending(x => x.PId).Skip(0).Take(12)
                      .Select(p => new PordectList
                      {
                          Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                          Category = p.td_Category.NameEn,
                          CatId = (int)p.CatId,
                          NameAR = p.CatId > 3 ? p.AccessoryTitleAR : p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                          NameEN = p.CatId > 3 ? p.AccessoryTitleEN : p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                          NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                          NewEn = (bool)p.isNew ? "New" : "Used",
                          PId = p.PId,
                          Price = p.AdPrice,
                          love = p.love,
                          Views = p.Views
                      }).ToList();
                return model;
            }

            // Get Cat By Brand
            if (filter.BrandId > 0 && filter.CatId == 0 && filter.SubAccessoriesId == 0 && filter.MainAccessoriesId == 0)
            {
                filter.CatId = GetCatByBrandId(filter.BrandId);
            }
            // Get Cat By Sub Accessories Type
            if (filter.BrandId == 0 && filter.CatId == 0 && filter.SubAccessoriesId > 0 && filter.MainAccessoriesId == 0)
            {
                filter.CatId = GetSubAccessories(filter.SubAccessoriesId);
            }
            // Get Cat By Main Accessories Type
            if (filter.BrandId == 0 && filter.CatId == 0 && filter.SubAccessoriesId == 0 && filter.MainAccessoriesId > 0)
            {
                filter.CatId = GetMInAccessories(filter.MainAccessoriesId);
            }
            if (filter.CatId == 1 || filter.CatId == 2 || filter.CatId == 3)
            {
                //Company
                if (filter.BId > 0 && filter.BrandId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.businessId == filter.BId && w.BrandId == filter.BrandId && w.Active== true &&   w.AdendDate >= DateTime.Now)
                        .OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.BId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.businessId == filter.BId && w.Active == true &&   w.AdendDate >= DateTime.Now)
                        .OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                //colors
                if (filter.coloreId > 0 && filter.ModelId <= 0 && filter.ClassId <= 0 && filter.BrandId <= 0 )
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.CColors == filter.coloreId && w.Active == true &&   w.AdendDate >= DateTime.Now)
                        .OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.coloreId > 0 && filter.ModelId > 0 && filter.ClassId <= 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.ModelId == filter.ModelId && w.CColors == filter.coloreId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.coloreId > 0 && filter.ModelId <= 0 && filter.ClassId > 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.CarClassId == filter.ClassId && w.CColors == filter.coloreId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.coloreId > 0 && filter.ModelId <= 0 && filter.ClassId <= 0 && filter.BrandId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.BrandId == filter.BrandId && w.CColors == filter.coloreId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.coloreId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.CColors == filter.coloreId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                //Pries
                if (filter.price > 0 && filter.ModelId <= 0 && filter.ClassId <= 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.AdPrice >= filter.price &&   w.AdendDate >= DateTime.Now)
                        .OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.price > 0 && filter.ModelId > 0 && filter.ClassId <= 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.ModelId == filter.ModelId && w.AdPrice <= filter.price &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.price > 0 && filter.ModelId <= 0 && filter.ClassId > 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.CarClassId == filter.ClassId && w.AdPrice <= filter.price &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.price > 0 && filter.ModelId <= 0 && filter.ClassId <= 0 && filter.BrandId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.BrandId == filter.BrandId && w.AdPrice <= filter.price &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                // عادي
                if (filter.ModelId <= 0 && filter.ClassId <= 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId &&  w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.ModelId > 0 && filter.ClassId <= 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.ModelId == filter.ModelId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.ModelId <= 0 && filter.ClassId > 0 && filter.BrandId <= 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.CarClassId == filter.ClassId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.ModelId > 0 && filter.ClassId > 0 && filter.BrandId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.BrandId == filter.BrandId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                //las
                if (filter.ModelId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.ModelId == filter.ModelId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.ClassId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CarClassId == filter.ClassId).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.BrandId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.BrandId == filter.BrandId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.CatId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                           NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }

            }
            if (filter.CatId == 4 || filter.CatId == 5)
            {
                if (filter.SubAccessoriesId == 0 && filter.MainAccessoriesId == 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                      .Select(p => new PordectList
                      {
                          Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                          Category = p.td_Category.NameEn,
                          CatId = (int)p.CatId,
                          NameAR = p.AccessoryTitleAR,
                          NameEN = p.AccessoryTitleEN,
                          NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                          NewEn = (bool)p.isNew ? "New" : "Used",
                          PId = p.PId,
                          Price = p.AdPrice,
                          love = p.love,
                          Views = p.Views
                      }).ToList();
                    return model;
                }
                if (filter.SubAccessoriesId > 0 && filter.MainAccessoriesId == 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.tdAccessorySubCategory.AcSubId == filter.SubAccessoriesId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.AccessoryTitleAR,
                           NameEN = p.AccessoryTitleEN,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.SubAccessoriesId == 0 && filter.MainAccessoriesId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.tdAccessorySubCategory.ACatId == filter.MainAccessoriesId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.AccessoryTitleAR,
                           NameEN = p.AccessoryTitleEN,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
                if (filter.SubAccessoriesId > 0 && filter.MainAccessoriesId > 0)
                {
                    model = db.td_ProductsItem.Where(w => w.Active == true && w.CatId == filter.CatId && w.tdAccessorySubCategory.AcSubId == filter.SubAccessoriesId &&   w.AdendDate >= DateTime.Now).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                       .Select(p => new PordectList
                       {
                           Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                           Category = p.td_Category.NameEn,
                           CatId = (int)p.CatId,
                           NameAR = p.AccessoryTitleAR,
                           NameEN = p.AccessoryTitleEN,
                           NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                           NewEn = (bool)p.isNew ? "New" : "Used",
                           PId = p.PId,
                           Price = p.AdPrice,
                           love = p.love,
                           Views = p.Views
                       }).ToList();
                    return model;
                }
            }

            if (filter.SubAccessoriesId > 0 && filter.ISNew == true)
            {
                model = db.td_ProductsItem.Where(w => w.Active == true && w.tdAccessorySubCategory.AcSubId == filter.SubAccessoriesId && w.isNew == true).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                      .Select(p => new PordectList
                      {
                          Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                          Category = p.td_Category.NameEn,
                          CatId = (int)p.CatId,
                          NameAR = p.AccessoryTitleAR,
                          NameEN = p.AccessoryTitleEN,
                          NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                          NewEn = (bool)p.isNew ? "New" : "Used",
                          PId = p.PId,
                          Price = p.AdPrice,
                          love = p.love,
                          Views = p.Views
                      }).ToList();
                return model;
            }

            if (filter.SubAccessoriesId > 0)
            {
                model = db.td_ProductsItem.Where(w => w.Active == true && w.tdAccessorySubCategory.AcSubId == filter.SubAccessoriesId).OrderByDescending(x => x.PId).Skip(filter.Skep).Take(filter.Take)
                      .Select(p => new PordectList
                      {
                          Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                          Category = p.td_Category.NameEn,
                          CatId = (int)p.CatId,
                          NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                          NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                          NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                          NewEn = (bool)p.isNew ? "New" : "Used",
                          PId = p.PId,
                          Price = p.AdPrice,
                          love = p.love,
                          Views = p.Views
                      }).ToList();
                return model;
            }
          

            return model = null;
        }
        public PorductDetailModel PorductDetail(int PId)
        {
            PorductDetailModel model = db.td_ProductsItem.Where(e => e.PId == PId)
                .Select(x => new PorductDetailModel
                {
                    Views = x.Views,
                    PId = x.PId,
                    AccessoryForAll = x.AccessoryForAll,
                    AccessoryTypeAR = x.tdAccessorySubCategory.AcSNameAr + " " + x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameAr,
                    AccessoryTypeEn = x.tdAccessorySubCategory.AcSNameEn + " " + x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn,
                    businessId = x.businessId,
                    CarsFeature = x.td_Features.Select(f => new CarFeature
                    {
                        FeatureAr = f.td_carSpecifications.SpeAr,
                        FeatureEn = f.td_carSpecifications.SpeEn,
                        Icons = f.td_carSpecifications.SpeIcon,
                    }).ToList(),
                    Category = x.td_Category.NameEn,
                    CatId = (int)x.CatId,
                    ColorAR = x.CatId < 4 ? x.tdColors.colorIdNameAR : "",
                    ColorEn = x.CatId < 4 ? x.tdColors.colorIdNameEN : "",
                    ColorIcon = x.CatId < 4 ? x.tdColors.colorIcon : "",
                    DescribeAR = x.DescribeAR,
                    DescribeEN = x.DescribeEN,
                    Logo= x.businessId != null && x.businessId > 0 ? x.td_Business.BusinessLogo : "",
                    EngineCapacityCC = x.CEngineCapacityCC,
                    Images = x.td_PImage.Select(i => i.ImageUrl).ToList(),
                    love = x.love,
                    MileageKM = x.CMileageKM,
                    NewAR = (bool)x.isNew ? "جديد" : "مستعمل",
                    NewEn = (bool)x.isNew ? "New" : "Used",
                    NameAR = x.CatId > 3 ? x.AccessoryTitleAR : x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                    NameEN = x.CatId > 3 ? x.AccessoryTitleEN : x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears,
                    Price = x.AdPrice,
                    userId = x.userId,
                    ownerNameAR = x.businessId != null && x.businessId > 0 ? x.td_Business.BusinessNameAR : "",
                    ownerNameEn = x.businessId != null && x.businessId > 0 ? x.td_Business.BusinessNameEn : "",
                    AccessorySubTypeId = x.CatId > 3 ? x.AcSubId : 0,
                    CarClassId = x.CatId > 3 ? x.CarClassId : 0,
                    AccessoryModels = x.td_AccessoryDetails.Select(a => new AccessoryDetailsViewDomain
                    {
                        BrandNameAR = a.td_CarModel.td_CarClass.td_Brand.BrandNameAr,
                        BrandNameEN = a.td_CarModel.td_CarClass.td_Brand.BrandNameEn,
                        ClassNameAR = a.td_CarModel.td_CarClass.CarClassNameAr,
                        ClassNameEN = a.td_CarModel.td_CarClass.CarClassNameEn,
                        Id = a.Id,
                        ModelId = a.ModelId,
                        ModelYears = a.td_CarModel.ModelYears,
                        PId = a.PId
                    }).ToList()
                }).SingleOrDefault();
            if (model.ownerNameEn == "" && model.ownerNameAR == "" && model.userId != null && model.userId != "")
            {
                string name = db.UserLogin.Where(u => u.Id == model.userId).SingleOrDefault().FullName;
                model.ownerNameEn = name;
                model.ownerNameAR = name;
            }

            model.Views = model.Views + 1;

            model.SimilarAds = GetSimilarAds(PId).SimilarAds;
            model.contacts = GetSimilarAds(PId).contacts;
            return model;
        }
        public int Setlove(int PId)
        {
            td_ProductsItem Product = db.td_ProductsItem.Find(PId);
            if (Product != null)
            {
                Product.love = Product.love + 1;
                db.Entry(Product).State = EntityState.Modified;
                int res = db.SaveChanges();
                if (res > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
            else
            {
                return -1;
            }
        }
        private int GetCatByBrandId(int brandId)
        {
            int CatId = (int)db.td_ProductsItem.Where(x => x.BrandId == brandId).FirstOrDefault().CatId;
            return CatId;
        }
        private int GetSubAccessories(int SubAccessories)
        {
            int CatId = (int)db.td_ProductsItem.Where(x => x.AcSubId == SubAccessories).FirstOrDefault().CatId;
            return CatId;
        }
        private int GetMInAccessories(int MInAccessories)
        {
            int CatId = (int)db.td_ProductsItem.Where(x => x.tdAccessorySubCategory.ACatId == MInAccessories).FirstOrDefault().CatId;
            return CatId;
        }
        private SimilarContact GetSimilarAds(int PId)
        {
            SimilarContact similar = new SimilarContact();
            List<Contact> contact = new List<Contact>();
            td_ProductsItem model = db.td_ProductsItem.Find(PId);
            if (model.CatId > 3)
            {
                List<PordectList> PordectList = db.td_ProductsItem.Where(s => s.AcSubId == model.AcSubId && s.Active == true).OrderByDescending(e => e.PId).Take(20).Select(p => new PordectList
                {
                    Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                    Category = p.td_Category.NameEn,
                    CatId = (int)p.CatId,
                    NameAR = p.AccessoryTitleAR,
                    NameEN = p.AccessoryTitleEN,
                    NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                    NewEn = (bool)p.isNew ? "New" : "Used",
                    PId = p.PId,
                    Price = p.AdPrice,
                    love = p.love,
                    Views = p.Views
                }).ToList();
                if (model.userId == null && model.businessId != null)
                {
                    Contact contact1 = new Contact();
                    List<td_Contact_Media> _Contact_Medias = db.td_Contact_Media.Where(x => x.BusinessId == model.businessId && x.Type == "Phone Number").ToList();
                    foreach (var item in _Contact_Medias)
                    {
                        contact1.contact = item.contact;
                        contact1.Type = item.Type;
                        contact1.Icons = GetIconClass(item.Type);
                        contact.Add(contact1);
                    }
                }
                else
                {
                    UserLogin userLogin = db.UserLogin.Find(model.userId);

                    Contact contact1 = new Contact()
                    {
                        contact = userLogin.Utype == "e" ? userLogin.UPhone : userLogin.UEmail,
                        Icons = userLogin.Utype == "e" ? "fa fa-phone" : "fa fa-phone"
                    };

                    contact.Add(contact1);
                }
                similar.contacts = contact;
                similar.SimilarAds = PordectList;

                model.Views = model.Views + 1;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return similar;
            }
            else
            {
                List<PordectList> PordectList = db.td_ProductsItem.Where(s => s.CarClassId == model.CarClassId && s.Active == true).OrderByDescending(e => e.PId).Take(20).Select(p => new PordectList
                {
                    Images = p.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                    Category = p.td_Category.NameEn,
                    CatId = (int)p.CatId,
                    NameAR = p.td_Brand.BrandNameAr + " " + p.td_CarModel.td_CarClass.CarClassNameAr + " " + p.td_CarModel.ModelYears,
                    NameEN = p.td_Brand.BrandNameEn + " " + p.td_CarModel.td_CarClass.CarClassNameEn + " " + p.td_CarModel.ModelYears,
                    NewAR = (bool)p.isNew ? "جديد" : "مستعمل",
                    NewEn = (bool)p.isNew ? "New" : "Used",
                    PId = p.PId,
                    Price = p.AdPrice,
                    love = p.love,
                    Views = p.Views
                }).ToList();

                if (model.userId == null && model.businessId != null)
                {
                    Contact contact1 = new Contact();
                    List<td_Contact_Media> _Contact_Medias = db.td_Contact_Media.Where(x => x.BusinessId == model.businessId && x.Type == "Phone Number").ToList();
                    foreach (var item in _Contact_Medias)
                    {
                        contact1.contact = item.contact;
                        contact1.Type = item.Type;
                        contact1.Icons = GetIconClass(item.Type);
                        contact.Add(contact1);
                    }

                }
                else
                {
                    UserLogin userLogin = db.UserLogin.Find(model.userId);

                    Contact contact1 = new Contact()
                    {
                        contact = userLogin.Utype == "e" ? userLogin.UPhone : userLogin.UEmail,
                        Icons = userLogin.Utype == "e" ? "fa fa-phone" : "fa fa-phone"
                    };

                    contact.Add(contact1);
                }
                similar.contacts = contact;
                similar.SimilarAds = PordectList;
                model.Views = model.Views + 1;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return similar;


            }

        }
        public string GetIconClass(string type)
        {
            if (type == "facebook")
            {
                return "fa fa-facebook";
            }
            else
            if (type == "Phone Number")
            {
                return "fa fa-phone";
            }
            else
            if (type == "Whatsapp Number")
            {
                return "fa fa-whatsapp";
            }
            else
            if (type == "Twitter")
            {
                return "fa fa-twitter";
            }
            else
            if (type == "Web Site")
            {
                return "fa fa-website";
            }
            else
            {
                return "fa fa-website";
            }
        }
        public List<PordectMailDomain> MailForCompanyPordect() 
        {

            List<PordectMailDomain> List = db.td_ProductsItem.Where(w => w.Active == true && w.businessId != null && w.businessId != 0)
                .OrderByDescending(o => o.PId).Skip(50).OrderByDescending(o => o.PId).Take(50)
                .Select(x => new PordectMailDomain
                {
                    PId = x.PId,
                    Image = x.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                    NameAR = x.CatId > 3 ? x.AccessoryTitleAR : x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                    NameEN = x.CatId > 3 ? x.AccessoryTitleEN : x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears,

                }).ToList();

            return List;

        }
        public List<PordectMailDomain> MailForUsersPordect()
        {

            List<PordectMailDomain> List = db.td_ProductsItem.Where(w => w.Active == true && w.userId != null && w.userId != "" && w.AdendDate <= DateTime.Now)
                .OrderByDescending(o => o.PId).Take(50)
                .Select(x => new PordectMailDomain
                {
                    PId = x.PId,
                    Image = x.td_PImage.Select(i => i.ImageUrl).FirstOrDefault(),
                    NameAR = x.CatId > 3 ? x.AccessoryTitleAR : x.td_Brand.BrandNameAr + " " + x.td_CarModel.td_CarClass.CarClassNameAr + " " + x.td_CarModel.ModelYears,
                    NameEN = x.CatId > 3 ? x.AccessoryTitleEN : x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears,

                }).ToList();

            return List;

        }
    }
}
