﻿using AlwakeelBusinessLayer.Common;
using AlwakeelBusinessLayer.Interfaces.PordectsAPI;
using AlwakeelDomainLayer.APIView;
using AlwakeelRepositoryLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation.PordectsAPI
{
    public class Customerproducts : Base, ICustomerproducts
    {
        /// <summary>
        /// N = New 
        /// Activ = Activ
        /// S=Selld
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>


        public int AddCars(SellCarsDomain model)
        {
            td_ProductsItem product = new td_ProductsItem()
            {
                CarClassId = model.CarClassId,
                CatId = model.CatId,
                ModelId = model.ModelId,
                CColors = model.colorId,
                AdPrice = model.AdPrice,
                AdEntryDate = DateTime.Now,
                CEngineCapacityCC = model.CEngineCapacityCC,
                AdStatus = "N",
                CMileageKM = model.CMileageKM,
                BrandId = model.BrandId,
                DescribeAR = model.DescribeAR,
                DescribeEN = model.DescribeEN,
                isNew = model.isNew,
                CTransmission = 0,
                userId = model.UserId,
                AdendDate = DateTime.Now.AddDays(7),
                dealOfday = false,
                Active = false,
                love = 0,
                Views = 0

            };
            db.td_ProductsItem.Add(product);
            int res = db.SaveChanges();
            if (res > 0)
            {
                List<td_PImage> pIList = new List<td_PImage>();
                List<td_Features> FList = new List<td_Features>();
                foreach (var item in model.ImagePordect)
                {
                    td_PImage pI = new td_PImage()
                    {
                        ImageUrl = item,
                        PId = product.PId
                    };
                    pIList.Add(pI);
                }
                db.td_PImage.AddRange(pIList);
                db.SaveChanges();

                foreach (var item in model.Features)
                {
                    td_Features feature = new td_Features()
                    {
                        PId = product.PId,
                        SpeId = item
                    };
                    FList.Add(feature);
                }

                db.td_Features.AddRange(FList);
                db.SaveChanges();

                return product.PId;
            }
            else
            {
                return 0;
            }
        }

        public List<CustomerproductsDomain> GetSold()
        {
            List<CustomerproductsDomain> model = new List<CustomerproductsDomain>();
            List<td_ProductsItem> ProductsI = db.td_ProductsItem
                .Where(e => e.AdStatus == "s" && e.userId != null)
                .OrderByDescending(o => o.PId).ToList();

            foreach (var x in ProductsI)
            {
                CustomerproductsDomain CP = new CustomerproductsDomain()
                {
                    PId = x.PId,
                    AdendDate = x.AdendDate,
                    AdEntryDate = x.AdEntryDate,
                    Category = x.td_Category.NameEn,
                    CEngineCapacityCC = (int)x.CEngineCapacityCC,
                    CMileageKM = (int)x.CMileageKM,
                    Contact = GetContact(x.userId),
                    ColorName = x.tdColors.colorIdNameEN,
                    DescribeEN = x.DescribeEN,
                    DescribeAR = x.DescribeAR,
                    Info = x.CatId <= 3 ? x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears : x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn + " - " + x.tdAccessorySubCategory.AcSNameEn,
                    NewEn = (bool)x.isNew ? "New" : "Used",
                    Name = GetName(x.userId),
                    Price = (decimal)x.AdPrice,
                };

                model.Add(CP);
            }

            return model;
        }

        public int SetSold(int pId)
        {
            td_ProductsItem item = db.td_ProductsItem.Find(pId);
            if (item != null)
            {
                item.AdStatus = "S";
                item.Active = false;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {

                return 0;
            }
        }

        public int AddAccessory(SellAccessoryDomain model)
        {
            td_ProductsItem product = new td_ProductsItem()
            {

                CatId = model.CatId,
                AccessoryForAll = model.AccessoryForAll,
                AdEntryDate = DateTime.Now,
                AcSubId = model.SubAccessoriesType,
                AccessoryTitleAR = model.AccessoryTitleAR,
                AccessoryTitleEN = model.AccessoryTitleEN,
                AdStatus = "N",
                userId = model.UserId,
                DescribeAR = model.DescribeAR,
                DescribeEN = model.DescribeEN,
                isNew = model.isNew,
                CTransmission = 0,
                AdendDate = DateTime.Now.AddDays(7),
                dealOfday = false,
                Active = false,
                love = 0,
                Views = 0,
                CMileageKM=0,
                CEngineCapacityCC=0,
                AdPrice=model.Price
                
               


            };
            db.td_ProductsItem.Add(product);
            int res = db.SaveChanges();
            if (res > 0)
            {
                List<td_PImage> pIList = new List<td_PImage>();
                List<td_AccessoryDetails> accessoryDetails = new List<td_AccessoryDetails>();
                foreach (var item in model.ImagePordect.Take(4))
                {
                    td_PImage pI = new td_PImage()
                    {
                        ImageUrl = item,
                        PId = product.PId
                    };
                    pIList.Add(pI);
                }
                db.td_PImage.AddRange(pIList);
                db.SaveChanges();

                foreach (var item in model.SupportDetails)
                {
                    td_AccessoryDetails modelaccessory = new td_AccessoryDetails()
                    {

                        ModelId = item.ModelId,
                        PId = product.PId,
                        Id=product.PId
                    };
                    accessoryDetails.Add(modelaccessory);
                }
                db.td_AccessoryDetails.AddRange(accessoryDetails);
                db.SaveChanges();
                return product.PId;
            }
            else
            {
                return 0;
            }
        }

        public List<CustomerproductsDomain> GetNotActive()
        {
            List<CustomerproductsDomain> model = new List<CustomerproductsDomain>();
            List<td_ProductsItem> ProductsI = db.td_ProductsItem
                .Where(e => e.Active == false && e.AdStatus == "N" && e.userId != null)
                .OrderByDescending(o => o.PId).ToList();

            foreach (var x in ProductsI)
            {
                CustomerproductsDomain CP = new CustomerproductsDomain()
                {
                    PId = x.PId,
                    AdendDate = x.AdendDate,
                    AdEntryDate = x.AdEntryDate,
                    Category = x.td_Category.NameEn,
                    CEngineCapacityCC = x.CEngineCapacityCC,
                    CMileageKM = x.CMileageKM,
                    Contact = GetContact(x.userId),
                    ColorName = x.CatId <= 3 ? x.tdColors.colorIdNameEN : "Now Colors",
                    DescribeEN = x.DescribeEN,
                    DescribeAR = x.DescribeAR,
                    Info = x.CatId <= 3 ? x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears : x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn + " - " + x.tdAccessorySubCategory.AcSNameEn,
                    NewEn = (bool)x.isNew ? "New" : "Used",
                    Name = GetName(x.userId),
                    Price = (decimal)x.AdPrice,
                };

                model.Add(CP);
            }

            return model;
        }

        public CustomerproductsDomain GetDetails(int PId)
        {
            td_ProductsItem x = db.td_ProductsItem.Find(PId);
            CustomerproductsDomain model = new CustomerproductsDomain()
            {
                PId = x.PId,
                AdendDate = x.AdendDate,
                AdEntryDate = x.AdEntryDate,
                Category = x.td_Category.NameEn,
                CEngineCapacityCC = x.CEngineCapacityCC,
                CMileageKM = x.CMileageKM,
                Contact = GetContact(x.userId),
                ColorName = x.CatId <= 3 ? x.tdColors.colorIdNameEN : "Now Colors",
                DescribeEN = x.DescribeEN,
                DescribeAR = x.DescribeAR,
                Info = x.CatId <= 3 ? x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears : x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn + " - " + x.tdAccessorySubCategory.AcSNameEn,
                NewEn = (bool)x.isNew ? "New" : "Used",
                Name = GetName(x.userId),
                Price = (decimal)x.AdPrice,
                CatId = x.CatId
            };

            return model;
        }

        public List<CustomerproductsDomain> GetActivted()
        {

            List<CustomerproductsDomain> model = new List<CustomerproductsDomain>();
            List<td_ProductsItem> ProductsI = db.td_ProductsItem
                .Where(e => e.Active == true && e.AdStatus == "A" && e.userId != null)
                .OrderByDescending(o => o.PId).ToList();

            foreach (var x in ProductsI)
            {
                CustomerproductsDomain CP = new CustomerproductsDomain()
                {
                    PId = x.PId,
                    AdendDate = x.AdendDate,
                    AdEntryDate = x.AdEntryDate,
                    Category = x.td_Category.NameEn,
                    CEngineCapacityCC = x.CEngineCapacityCC,
                    CMileageKM = x.CMileageKM,
                    Contact = GetContact(x.userId),
                    ColorName = x.CatId <= 3 ? x.tdColors.colorIdNameEN : "Now Colors",
                    DescribeEN = x.DescribeEN,
                    DescribeAR = x.DescribeAR,
                    Info = x.CatId <= 3 ? x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears : x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn + " - " + x.tdAccessorySubCategory.AcSNameEn,
                    NewEn = (bool)x.isNew ? "New" : "Used",
                    Name = GetName(x.userId),
                    Price = (decimal)x.AdPrice,
                };

                model.Add(CP);
            }

            return model;

        }



        public int SetActivat(int PId)
        {
            td_ProductsItem item = db.td_ProductsItem.Find(PId);
            if (item != null)
            {
                item.AdStatus = "A";
                item.Active = true;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {

                return 0;
            }

        }
        public string GetContact(string UserId)
        {
            UserLogin userLogin = db.UserLogin.Find(UserId);
            if (userLogin.UPhone == "e")
            {
                return userLogin.UEmail;
            }
            else
            {
                return userLogin.UPhone;
            }
        }
        public string GetName(string UserId)
        {
            UserLogin userLogin = db.UserLogin.Find(UserId);
            return userLogin.FullName;
        }
        public List<Colors> GetApiColors()
        {
            List<Colors> model = db.tdColors.Select(x => new Colors
            {
                Icon = x.colorIcon,
                coloreId = x.colorId,
                NameAR = x.colorIdNameAR,
                NameEn = x.colorIdNameEN

            }).ToList();
            return model;
        }



        //API

        public List<CustomerproductsDomain> GetAPICustomerProducts(string UserId)
        {
            List<CustomerproductsDomain> model = new List<CustomerproductsDomain>();
            List<td_ProductsItem> ProductsI = db.td_ProductsItem
                .Where(e => e.userId == UserId)
                .OrderByDescending(o => o.PId).ToList();

            foreach (var x in ProductsI)
            {
                CustomerproductsDomain CP = new CustomerproductsDomain()
                {
                    PId = x.PId,
                    AdendDate = x.AdendDate,
                    AdEntryDate = x.AdEntryDate,
                    Category = x.td_Category.NameEn,
                    CEngineCapacityCC = x.CEngineCapacityCC,
                    CMileageKM = x.CMileageKM,
                    Contact = GetContact(x.userId),
                    ColorName = x.CatId <= 3 ? x.tdColors.colorIdNameEN : "Now Colors",
                    DescribeEN = x.DescribeEN,
                    DescribeAR = x.DescribeAR,
                    Info = x.CatId <= 3 ? x.td_Brand.BrandNameEn + " " + x.td_CarModel.td_CarClass.CarClassNameEn + " " + x.td_CarModel.ModelYears : x.tdAccessorySubCategory.tdAccessoryMainCategory.ACatNameEn + " - " + x.tdAccessorySubCategory.AcSNameEn,
                    NewEn = (bool)x.isNew ? "New" : "Used",
                    Name = GetName(x.userId),
                    Price = (decimal)x.AdPrice,
                };

                model.Add(CP);
            }

            return model;
        }

    }
}
