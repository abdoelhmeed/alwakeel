﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;
using AlwakeelBusinessLayer.Interfaces;
using AlwakeelBusinessLayer.Common;
using AlwakeelRepositoryLayer;
using System.Data.Entity;

namespace AlwakeelBusinessLayer.Implementation
{
    public class Brand : Base,IBrand
    {
        public int Add(BrandDomain brand)
        {
            td_Brand model = new td_Brand()
            {
                BramdCountry = brand.BramdCountry,
                BrandNameAr = brand.BrandNameAr,
                BrandNameEn = brand.BrandNameEn,
                CatId = brand.CatId,
                BrandsLogo = brand.BrandsLogo
            };

            db.td_Brand.Add(model);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            try
            {
                td_Brand brn = db.td_Brand.Find(Id);
                if (brn !=null)
                {
                    db.Entry(brn).State = EntityState.Deleted;
                    int res =db.SaveChanges();
                    if (res > 0)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }

                }
                else
                {
                    return -1;
                }
            }
            catch (Exception)
            {

                return -1;
            }
        }

        public List<BrandDomainViews> Get()
        {
            List<BrandDomainViews> model = db.td_Brand.Select(x => new BrandDomainViews
            {
                BramdCountry = x.BramdCountry,
                BrandNameAr = x.BrandNameAr,
                BrandNameEn = x.BrandNameEn,
                CatId = x.CatId,
                BrandsLogo = x.BrandsLogo,
                BrandId=x.BrandId,
                CatNameAr=x.td_Category.NameAr,
                CatNameEn=x.td_Category.NameEn

            }).OrderBy(o=> o.CatId).ToList();
            return model;
        }

        public List<BrandDomainViews> GetByCatId(int CatId)
        {
            List<BrandDomainViews> model = db.td_Brand.Where(u=> u.CatId==CatId).Select(x => new BrandDomainViews
            {
                BramdCountry = x.BramdCountry,
                BrandNameAr = x.BrandNameAr,
                BrandNameEn = x.BrandNameEn,
                CatId = x.CatId,
                BrandsLogo = x.BrandsLogo,
                BrandId = x.BrandId,
                CatNameAr = x.td_Category.NameAr,
                CatNameEn = x.td_Category.NameEn

            }).ToList();
            return model;
        }

        public BrandDomain GetById(int Id)
        {
            td_Brand x = db.td_Brand.Find(Id);
            BrandDomain model = new BrandDomain()
            {
                BramdCountry = x.BramdCountry,
                BrandNameAr = x.BrandNameAr,
                BrandNameEn = x.BrandNameEn,
                CatId = x.CatId,
                BrandsLogo = x.BrandsLogo,
                BrandId = x.BrandId,
               
            };

            return model;

        }

        public int Update(BrandDomain brand)
        {
            td_Brand model = db.td_Brand.Find(brand.BrandId);
            if (model !=null)
            {
                model.BramdCountry = brand.BramdCountry;
                model.BrandNameAr = brand.BrandNameAr;
                model.BrandNameEn = brand.BrandNameEn;
                model.CatId = brand.CatId;
                model.BrandsLogo = brand.BrandsLogo;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public bool isHasBrand(BrandDomain brand) 
        {
            int  Number= db.td_Brand.Where(x => x.BrandNameAr == brand.BrandNameAr && x.BrandNameEn == brand.BrandNameEn && x.CatId==brand.CatId).Count();
            if (Number > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<BrandDomainViews> GetBrandPyBusiness(int BId,int CatId) 
        {

            List<BrandDomainViews> model = db.td_BusinessBrands.Where(e => e.BusinessId == BId && e.td_Brand.CatId == CatId)
                .Select(x => new BrandDomainViews {

                    BramdCountry = x.td_Brand.BramdCountry,
                    BrandNameAr = x.td_Brand.BrandNameAr,
                    BrandNameEn = x.td_Brand.BrandNameEn,
                    CatId = x.td_Brand.CatId,
                    BrandsLogo = x.td_Brand.BrandsLogo,
                    BrandId = x.td_Brand.BrandId,
                    CatNameAr = x.td_Brand.td_Category.NameAr,
                    CatNameEn = x.td_Brand.td_Category.NameEn

                }).ToList();

            return model;

        }


    }
}
