﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IOrderRentCar
    {
        int NewOrder(OrdesDmonain model);
        int NotAvailableOrder(int Id);
     
        int DoneOrder(int Id);

        List<OrderRentCarDomain> GetNewsOrders(int BId);
        List<OrderRentCarDomain> GetNotAvailableOrder(int BId);
       
        List<OrderRentCarDomain> GetDoneOrder(int BId);


    }
}
