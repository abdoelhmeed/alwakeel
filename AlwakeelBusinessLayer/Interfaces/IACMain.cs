﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    /// <summary>
    /// Accessory Main Category
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public interface IACMain
    { 
        int Add(ACMainDomain model);
        int Update(ACMainDomain model);
        int Delete(int Id);
        List<ACMainDomain> Get();
        ACMainDomain GetById(int Id);
    }
}
