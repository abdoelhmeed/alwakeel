﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public  interface ICategory
    {
        int Add(CategoryDomain model);
        List<CategoryDomain> Get();
       CategoryDomain GetById(int Id);
        List<CategoryDomain> GetByIds(int OneId,int TowId);
    }
}
