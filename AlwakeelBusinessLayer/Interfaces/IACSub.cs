﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IACSub
    {
        int Add(ACSubDomain model);
        int Update(ACSubDomain model);
        int Delete(int Id);
        List<ACSubDomainView> Get();
        ACSubDomain GetById(int Id);
        List<ACSubDomainView> GetBYCatId(int ACatId);
    }
}
