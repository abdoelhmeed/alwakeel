﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBusinessBrand
    {
        int Add(BusinessBrandDoamin model);
        int Update(BusinessBrandDoamin model);
        int Delete(int Id);
         List<BusinessBrandDoamin>  Get();
        List<BusinessBrandDoamin> GetByBrand(int brandId);
        List<BusinessBrandDoamin> GetByBusinessId(int businessId);
        List<BusinessBrandDoamin> GetByBIdCatId(int CatId,int businessId);
    }
}
