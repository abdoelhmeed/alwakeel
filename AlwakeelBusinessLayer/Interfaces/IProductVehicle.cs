﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IProductVehicle
    {
        int Add(PVehicleDomain vehicel,List<string> Images);

        int delete(int PId);
        int Update(PVehicleDomain vehicel);
        List<PVehicleViewDomain> Get();

        List<PVehicleViewDomain> GetByBusiness(int busId);

        List<PVehicleViewDomain> GetByUserId(string userId);
        PVehicleViewDomain GetById(int PId);

        PVehicleDomain GetByIdToEdit(int PId);

    }
}
