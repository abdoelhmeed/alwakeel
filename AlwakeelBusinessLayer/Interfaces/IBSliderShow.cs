﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBSliderShow
    {
        int Add(BusinessSliderDomain slider);
        int Delete(int Id);
        List<BSliderViewDomain> Get();
        List<BSliderViewDomain> GetByBId(int BId);
        List<string> GetImageListByBId(int BId);
    }
}
