﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface ICarsModel
    {
        int Add(CarModelDomain model);
        int Update(CarModelDomain model);
        int Delete(int Id);
        CarModelDomain GetById(int Id);
        List<CarModelDomainViews> Get();
        List<CarModelDomainViews> GetByCarClassId(int CarClassId);



    }
}
