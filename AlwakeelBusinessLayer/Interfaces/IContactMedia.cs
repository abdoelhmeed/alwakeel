﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IContactMedia
    {
        int Add(ContactMediaDomain cm);
        int Update(ContactMediaDomain cm);
        int Delete(int Id);
        List<ContactMediaDomain> Get();
        List<ContactMediaDomain> GetByType(string type);
        List<ContactMediaDomain> GetBusinessId(int businessId);

    }
}
