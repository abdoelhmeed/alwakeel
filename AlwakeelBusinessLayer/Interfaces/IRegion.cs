﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelBusinessLayer.Common;
using AlwakeelDomainLayer;


namespace AlwakeelBusinessLayer.Interfaces
{
    public  interface IRegion
    {
        int Add(RegionDomain model);
        int Update(RegionDomain model);
        List<RegionDomain> Get();
        List<RegionDomain> GetByCityId(int CityId);
        RegionDomain GetById(int Id);
        int Delete(int Id);

    }
}
