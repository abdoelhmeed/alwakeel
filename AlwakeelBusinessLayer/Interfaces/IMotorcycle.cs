﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IMotorcycle
    {
        int Add(MotorcycleDomain motorcycle, List<string> Images);
        int Update(MotorcycleDomain motorcycle);
        MotorcycleDomain GetByPId(int PId);
        List<MotorcycleViweDomain> Get();
        List<MotorcycleViweDomain> GetByBId(int BId);
        int delete(int PId);
        MotorcycleViweDomain GetDetailsByPId(int PId);

    }
}
