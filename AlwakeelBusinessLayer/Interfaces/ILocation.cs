﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface ILocation
    {
        int Add(LocationDomain model);
        int Update(LocationDomain model);
        int Delete(int Id);
        List<LocationDomainView> Get();
        List<LocationDomainView> GetByBId(int BId);
        LocationDomain GetById(int Id);
    }
}
