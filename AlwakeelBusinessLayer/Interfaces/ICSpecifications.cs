﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface ICSpecifications
    {
        int Add(carSpecificationsDomain model);
        int Delete(int Id);
        List<carSpecificationsDomain> GetByModelId(int ModelId);
        carSpecificationsDomain GetById(int SpeId);

    }
}
