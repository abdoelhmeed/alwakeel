﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IFeaturesVehicle
    {
        List<PFVVeiwDomain> GetByPId(int PId);
        PFVVeiwDomain GetById(int Id);

        int Delete(int FId);
    }
}
