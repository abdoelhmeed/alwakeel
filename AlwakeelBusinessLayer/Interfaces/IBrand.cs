﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBrand
    {
        int Add(BrandDomain brand);
        int Update(BrandDomain brand);
        int Delete(int Id);
        List<BrandDomainViews> Get();
        List<BrandDomainViews> GetByCatId(int CatId);
        BrandDomain GetById(int Id);

    }
}
