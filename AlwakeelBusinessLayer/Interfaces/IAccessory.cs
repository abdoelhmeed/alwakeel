﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IAccessory
    {
        int Add(AccessoryDomain model);
        int Update(AccessoryEditDomain model);
        int Delete(int Id);
        List<AccessoryViewDomain> Get();
        AccessoryViewDomain GetById(int Id);
        List<AccessoryViewDomain> GetByBId(int BId);

    }
}
