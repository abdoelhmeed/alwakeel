﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface ICarClass
    {
        int Add(CarClassDomain carClass);
        int Update(CarClassDomain carClass);
        int Delete(int Id);
        CarClassDomain GetById(int id);
        List<CarClassDomainViews> Get();
        List<CarClassDomainViews> GetByBrandId(int BrandId);
    }
}
