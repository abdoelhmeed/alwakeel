﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBayNow
    {
        int Add(BayNowDomain model);

        List<BayNowViewDomain> Get();

        List<BayNowViewDomain> GetByBuId(int BId);


        List<BayNowViewDomain> GetNewByBuId(int BId);
        List<BayNowViewDomain> GetDoneByBuId(int BId);

        int Done(int Id);
    }
}
