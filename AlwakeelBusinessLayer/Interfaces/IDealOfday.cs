﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IDealOfday
    {
        int Add(DealOfdayDomain model);
        int Update(DealOfdayDomain model);
        DealOfdayDomain GetById(int Id);
        List<DealOfdayViewDomain> GetAll();
        List<DealOfdayViewDomain> GetByBId(int BId);
    }
}
