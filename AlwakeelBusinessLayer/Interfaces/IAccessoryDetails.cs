﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IAccessoryDetails
    {
        int Add(AccessoryDetailsDomain model);
        int Update(AccessoryDetailsDomain model);
        int Delete(int Id);
        List<AccessoryDetailsViewDomain> GetByPId(int PId);
       
    }
}
