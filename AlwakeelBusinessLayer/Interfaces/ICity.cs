﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface ICity
    {
        int Add(CityDomain city);
        int Update(CityDomain city);
        int Delete(int Id);
        List<CityDomain> Get();
        CityDomain GetById(int Id);
        List<CityDomain> GetByStateId(int StateId);
    }
}
