﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlwakeelDomainLayer;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBusiness
    {
        int AddReturnId(BusinessDomain model);
        int Delete(int Id);
        int Update(BusinessDomain model);
        List<BusinessDomain> Get();
        BusinessDomain GetById(int Id);
        List<BusinessDomain> GetType(string Type);
        int Activeion(int PId);
    }
}
