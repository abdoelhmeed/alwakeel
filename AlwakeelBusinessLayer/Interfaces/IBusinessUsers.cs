﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBusinessUsers
    {
        int Add(BusinessUserDomain bud);
        BusinessUserDomain Get(string UserId);
    }
}
