﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IColor
    {
       int Add(ColorDomain model);
       int Delete(int Id);
        List<ColorDomain> Get();
    }
}
