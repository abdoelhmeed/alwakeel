﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IBlog
    {
        int Add(BlogDomain blog);
        int Update(BlogDomain blog);
        int Delete(int Id);
        List<BlogDomain> Get();
        List<BlogDomain> GetByPublish(string Publish);
        BlogDomain GetById(int Id);
        BlogDomain ViewByBlpgById(int Id);
    }
}
