﻿using AlwakeelDomainLayer.APIView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces.PordectsAPI
{
    public interface ICustomerproducts
    {
        int AddCars(SellCarsDomain model);
        int AddAccessory(SellAccessoryDomain model);
        int SetActivat(int PId);
        List<CustomerproductsDomain> GetNotActive();
        CustomerproductsDomain GetDetails(int PId);
    }


}
