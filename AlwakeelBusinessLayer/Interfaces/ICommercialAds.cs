﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public  interface ICommercialAds
    {
        int Delete(int Id);
        int  Add(CommercialAdsDomain model);
        List<CommercialAdsDomain> Get();
        List<CommercialAdsDomain> GetByType(string Type);
    }
}
