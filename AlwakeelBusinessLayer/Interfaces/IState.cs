﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IState
    {
        int  Add(StateDomain state);
        int Update(StateDomain state);
        int Delete(int Id);
        List<StateDomain> Get();
        StateDomain GetById(int Id);
    }
}
