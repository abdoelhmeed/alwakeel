﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public  interface IBusinessProduct
    {
        int Add(BusinessProductDomain product);
        int Delete(int pid);
        List<BusinessProductDomain> Get();
        int Update(BusinessProductDomain product);
        BusinessProductDomain GetByPid(int pid);
        List<BusinessProductDomain> GetByBusinessId(int businessId);


    }
}
