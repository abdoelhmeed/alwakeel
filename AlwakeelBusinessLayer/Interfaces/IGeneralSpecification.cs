﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IGeneralSpecification
    {
        int Add(SpecificationDomain model);
        int Update(SpecificationDomain model);
        List<SpecificationDomain> Get( );
        int Delete(int Id);
    }
}
