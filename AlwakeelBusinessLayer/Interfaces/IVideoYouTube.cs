﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
  public  interface IVideoYouTube
  {
        int Add(VideoDomain Vd);
        int Delete(int Id);
        List<VideoDomain> Get ();
    }
}
