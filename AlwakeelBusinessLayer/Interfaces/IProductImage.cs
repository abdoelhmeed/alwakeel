﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IProductImage
    {
        int Add(ProductImageDomain model);
        int Update(ProductImageDomain model);
        int Delete(int Id);
        List<ProductImageDomain> GetByPId(int PId);
        ProductImageDomain GetById(int Id);

    }
}
