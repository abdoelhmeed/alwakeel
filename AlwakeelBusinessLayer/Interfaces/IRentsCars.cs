﻿using AlwakeelDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwakeelBusinessLayer.Interfaces
{
    public interface IRentsCars
    {
        int Add(RentCarDomain model);
        int Update(RentCarDomain model);
        RentCarDomain GetByEditId(int Id);
        List<RentCarViweDomain> GetBusiness();
        List<RentCarViweCustomerDomain> GetCustomer();
        List<RentCarViweDomain> GetByBId(int BId);
        int isAvailable(int Id);
        int Delete(int Id);

    }
}
